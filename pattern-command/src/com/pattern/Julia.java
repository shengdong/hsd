package com.pattern;

public class Julia {

	public static void main(String[] args) {
		AudioPlayer audioPlayer = new AudioPlayer();
		
		Command playCommand = new PlayCommand(audioPlayer);
		Command rewindCommand = new PlayCommand(audioPlayer);
		Command stopCommand = new PlayCommand(audioPlayer);
		
		Keypad keypad = new Keypad();
		keypad.setPlayCommand(playCommand);
		keypad.setRewindCommand(rewindCommand);
		keypad.setStopCommand(stopCommand);
		
		keypad.play();
		keypad.rewind();
		keypad.stop();
		keypad.play();
		keypad.stop();

	}

}
