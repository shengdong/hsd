package com.pattern;

public interface Command {

	public void execute();
}
