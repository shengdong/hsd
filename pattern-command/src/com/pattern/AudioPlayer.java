package com.pattern;

public class AudioPlayer {

	public void play() {
		System.out.println("playing.........");
	}
	
	public void rewind() {
		System.out.println("rewinding.........");
	}
	
	public void stop() {
		System.out.println("stoping..........");
	}
}
