#!/bin/bash

date1=`date -d last-day +%Y-%m-%d`
echo ${date1}

date0=`date +%Y-%m-%d`
echo ${date0}

date2=`date -d next-day +%Y-%m-%d`
echo ${date2}

date3=`date -d last-month +%Y-%m-%d`
echo ${date3}

date4=`date -d next-month +%Y-%m-%d`
echo ${date4}

a=10;
b=20;
echo `expr ${a} \* ${b}`

factorial=1
for a in `seq 1 10`
  do
  factorial=`expr ${factorial} \* ${a}`
  done
  echo "10!=${factorial}"

  for a in `seq 1 10`
  do
  factorial=`expr ${factorial} \* ${a}`
  done

  echo "10！=${factorial}"
