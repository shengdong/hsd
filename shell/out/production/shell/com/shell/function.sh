#!/usr/bash

function factorial() {
   factorial=1
   for a in `seq 1 $1`
   do
   factorial=`expr ${factorial} \* ${a}`
   done
   echo "$1 的阶乘是: ${factorial}"
}

factorial 10

function shift() {

    num=1
    for i in "$@";
    do
     num=` expr ${num} \* ${i}`
    done
    echo "所有乘积的和shift为: ${num}"
}
shift 10 80 90

function looPoint() {
    count=0
    while [ ${count} -lt $1 ]
    do
      echo ${count}
      let ++count
    done
    return 0
}

looPoint 10