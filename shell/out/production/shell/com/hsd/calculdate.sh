#!/usr/bin/env bash

#如果不定义变量类型，默认为string
#aa=11
#bb=22
#cc=${aa}+${bb}
#echo ${cc}
##declare -i 声明成为整型 1 第一种数字运算
#declare -i cc=${bb}+${aa}
#echo ${cc}
#
#declare -p cc


#数组movie[0]=pp
#movie[0]=pp
#movie[1]=tp
#declare -a movie[2]=live
#echo ${movie}
#echo ${movie[2]}
#echo ${movie[*]}

#环境变量有什么作用？
#declare -x pp=123

#declare -r p=234
#echo ${p}

aa=11
bb=22
dd=` expr ${aa} + 1`
dd=$[${aa}+${bb}]
let dd=${aa}+${bb}
echo ${dd}


