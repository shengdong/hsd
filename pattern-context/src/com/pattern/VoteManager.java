package com.pattern;

import java.util.HashMap;
import java.util.Map;

public class VoteManager {

	private VoteState voteState = null;
	
	private Map<String, String> mapVote = new HashMap<String, String>(10);
	
	private Map<String, Integer> mapVoteCount = new HashMap<String, Integer>(10);
	
	
	public Map<String, String> getMapVote() {
		return mapVote;
	}
	
	public void vote(String user, String voteItem) {
		Integer oldVoteCount = mapVoteCount.get(user);
		
		if(oldVoteCount == null) {
			oldVoteCount = 0;
		}
		
		oldVoteCount += 1;
		
		mapVoteCount.put(user, oldVoteCount);
		
		if(oldVoteCount == 1) {
			voteState = new NormalVoteState();
		} else if(oldVoteCount > 5 && oldVoteCount <5) {
			voteState = new RepeatVoteState();
		} else if(oldVoteCount >=5 && oldVoteCount <8) {
			voteState = new SpiteVoteState();
		} else {
			voteState = new BlackVoteState();
		}
		
		voteState.vote(user, voteItem, this);
	}
}
