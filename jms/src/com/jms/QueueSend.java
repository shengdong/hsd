package com.jms;

import java.util.Hashtable;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

public class QueueSend {
	private static final Logger logger = Logger.getLogger(QueueSend.class);

	public static void main(String[] args) {
		try {
			Hashtable<String, String> properties = new Hashtable<String, String>(20);

			properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.exolab.jms.jndi.InitialContextFactory");
			properties.put(Context.PROVIDER_URL, "rmi://localhost:1099/");

			Context context = new InitialContext(properties);

			QueueConnectionFactory queueConnectionFactory = (QueueConnectionFactory) context
					.lookup("JmsQueueConnectionFactory");
			
			QueueConnection queueConnection = queueConnectionFactory.createQueueConnection();
			QueueSession queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			
			Queue queue = (Queue) context.lookup("queuel");
			
			QueueSender queueSender = queueSession.createSender(queue);
			
			TextMessage message = queueSession.createTextMessage();
			message.setText("hello, world");
			queueSender.send(message);
			
			logger.info("message haved writed in JMS queue");
			
			queueSender.close();
			queueSession.close();
			queueConnection.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

}
