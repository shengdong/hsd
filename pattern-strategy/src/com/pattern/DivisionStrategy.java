package com.pattern;

public class DivisionStrategy implements Strategy {

	@Override
	public int calculate(int a, int b) {
		
		if( b!= 0) {
			return a/b;
		} else {
			throw new RuntimeException("divisor can't to be 0");
		}
	}

}
