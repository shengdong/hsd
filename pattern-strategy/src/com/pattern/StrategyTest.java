package com.pattern;

public class StrategyTest {

	public static void main(String[] args) {
		Context context = new Context(StrategyEnum.Add);
		System.out.println(context.calculate(10, 5));
		
		context = new Context(StrategyEnum.Multiply);
		System.out.println(context.calculate(10, 5));
		
		context = new Context(StrategyEnum.Substract);
		System.out.println(context.calculate(10, 5));
		
		context = new Context(StrategyEnum.Division);
		System.out.println(context.calculate(10, 5));
	}

}
