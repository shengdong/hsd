package com.pattern;

public class Context {
	private Strategy strategy;
	
	
	public Context(StrategyEnum strategyEnum) {
		switch (strategyEnum) {
		case Add:
			strategy = new AddStrategy();
			break;

		case Substract:
			strategy = new SubstractStrategy();
			break;
			
		case Multiply:
			strategy = new MultiplyStrategy();
			break;
			
		case Division:
			strategy = new DivisionStrategy();
			break;
		default:
			break;
		}
	}

	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}
	
	public int calculate(int a, int b) {
		return strategy.calculate(a, b);
	}

}
