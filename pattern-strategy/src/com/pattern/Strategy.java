package com.pattern;

public interface Strategy {
	
	public abstract int calculate(int a, int b);
}
