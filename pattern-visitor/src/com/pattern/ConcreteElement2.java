package com.pattern;

public class ConcreteElement2 extends Element {

	@Override
	public void accept(IVisitor iVisitor) {
		iVisitor.visit(this);
	}

	@Override
	public void doSomething() {
		System.out.println("this is element 2");

	}

}
