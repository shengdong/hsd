package com.pattern;

import java.util.List;

public class Client {

	public static void main(String[] args) {
		List<Element> list = ObjectStruture.getList();
		
		for(Element element : list) {
			element.accept(new Visitor());
		}
	}
}
