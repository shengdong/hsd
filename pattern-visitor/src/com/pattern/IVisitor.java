package com.pattern;

public interface IVisitor {

	public void visit(ConcreteElement1 element1);

	public void visit(ConcreteElement2 element2);
}
