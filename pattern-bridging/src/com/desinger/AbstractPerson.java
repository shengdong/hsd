package com.desinger;

public abstract class AbstractPerson {
	private WorkJob workJob;
	
	
	public abstract void workingJob();
	
	
	public AbstractPerson(WorkJob workJob) {
		this.workJob = workJob;
	}
	
	public WorkJob getWoJob() {
		return workJob;
	}
		
}
