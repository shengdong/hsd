package com.desinger;

public class Client {

	public static void main(String[] args) {
		System.out.println("**********************");
		System.out.println("*********wind********");
		System.out.println("**********************");
		
		WorkJob job = new WindJob();
		AbstractPerson person = new ZhangYujiePerson(job);
		person.workingJob();
		
		System.out.println("**********************");
		System.out.println("*********router********");
		System.out.println("**********************");
		
		job = new RouterJob();
		person = new YaoMingDongPerson(job);
		person.workingJob();
		
		System.out.println("**********************");
		System.out.println("*********web********");
		System.out.println("**********************");
		
		job = new WebJob();
		person = new FangZhiJianPerson(job);
		person.workingJob();
	}
}
