package com.desinger;

public class YaoMingDongPerson extends AbstractPerson {

	public YaoMingDongPerson(WorkJob workJob) {
		super(workJob);
	}

	@Override
	public void workingJob() {
		System.out.println("YaoMingDong is : " + super.getWoJob().job());
	}

}
