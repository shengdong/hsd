package com.stsing.handler;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.stsing.util.DBUtil;
import com.stsing.util.SocketConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class SHCHHandler extends MessageHandler {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SHCHHandler.class);
	
	

	@SuppressWarnings("unchecked")
	@Override
	public boolean saveToDB(String path) {
	    Gson gson = new Gson();
	    InputStream stream = null;
	    byte[] data = new byte[10240];
	    try {
	    	stream = new FileInputStream(path);
	    	int count = 0;
	    	StringBuffer buffer = new StringBuffer();
    	    while((count = stream.read(data, 0, data.length)) > 0) {
			   String str = new String(data, 0, count, "utf-8").trim();
			   buffer.append(str);
		    }
    	    stream.close();
    	    List<String> listsALl = gson.fromJson(buffer.toString(), List.class);
    	    logger.info(listsALl.size());
    	    List<Map<String, Object>> reslutLists = new ArrayList<Map<String,Object>>(5000);
    	    for(int j=0; j<listsALl.size(); j++) {
    	    	String key = listsALl.get(j);
    	    	Map<String, Object> result = new HashMap<String, Object>(20);
    	    	Map<String, Object> m = gson.fromJson(key, Map.class);
    	    	for(String k : m.keySet()) {
    	    		if(StringUtils.equals("securityId", k)) {
    	    			result.put("SECID", m.get(k));
    	    			String value = SocketConfigUtil.getshortNameMap().get((String) m.get(k));
    	    			result.put("SHORTNAME", value);
    	    		}
    	    		if(StringUtils.equals("prevClosePx", k)) {
    	    			result.put("LAST_CLOSING_PRICE", m.get(k));
    	    		}
    	    		if(StringUtils.equals("numTrades", k)) {
    	    			result.put("DEAL_COUNT", m.get(k));
    	    		}
    	    		if(StringUtils.equals("totalVolumeTrade", k)) {
    	    			result.put("DEAL_QUANTITY", m.get(k));
    	    		}
    	    		if(StringUtils.equals("totalValueTrade", k)) {
    	    			result.put("DEAL_AMOUNT", m.get(k));
    	    		} 
    	    		if(StringUtils.equals("NoMDEntries", k)) {
    	    			List<String> lists = (List<String>) m.get(k);
    	    			for(String s : lists) {
    	    				String[] datas = s.split(",");
    	    				if(StringUtils.equals(datas[0], "2")) {
    	    					result.put("LATEST_DEAL_PRICE", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "4")) {
    	    					result.put("OPENING_PRICE", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "7")) {
    	    					result.put("HIGHEST_PRICE", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "8")) {
    	    					result.put("LOWEST_PRICE", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "x1")) {
    	    					result.put("PRICE_FLOAT1", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "x2")) {
    	    					result.put("PRICE_FLOAT2", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "x5")) {
    	    					result.put("EARNINGS_MULTIPLE1", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "x6")) {
    	    					result.put("EARNINGS_MULTIPLE2", datas[1]);
    	    				}
    	    				if(StringUtils.equals(datas[0], "xg")) {
    	    					result.put("POSITION", datas[1]);
    	    				}
    	    				// sale in
    	    				if(StringUtils.equals(datas[0], "0")) {
    	    					if(!StringUtils.equals(datas[3], "0")) {
    	    						result.put("BUY_PRICE" + datas[3], datas[1]);
    	    						result.put("BUY_QUANTITY" + datas[3], datas[2]);
    	    					}
    	    				}
    	    				// sale out
    	    				if(StringUtils.equals(datas[0], "1")) {
    	    					if(!StringUtils.equals(datas[3], "0")) {
    	    						result.put("SOLE_PIRCE" + datas[3], datas[1]);
    	    						result.put("SOLE_QUANTITY" + datas[3], datas[2]);
    	    					}
    	    				}
    	    			}
    	    		}
    	    	}
    	    	reslutLists.add(result);
    	    	
    	    }
    	    logger.error("个数" + reslutLists.size());
    	    
    	   return insertOrUpdateDB(reslutLists);
    	   
	    } catch (Exception e) {
	    	logger.error(e.getMessage(), e);
	    	return false;
		} finally {
			if(stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		
	}
	
	/**
	 * if secid is exist, update it, or insert
	 * @param reslutLists 插入或者更新的列表
	 * @return 插入或者更新的结果
	 */
	private boolean insertOrUpdateDB(List<Map<String, Object>> reslutLists) {
		List<Map<String, Object>> insertLists = new LinkedList<Map<String,Object>>();
		List<Map<String, Object>> updateLists = new LinkedList<Map<String,Object>>();
		
		List<String> lists = SocketConfigUtil.getSJHQID();
		
		for(Map<String, Object> map : reslutLists) {
			String secid = (String)map.get("SECID");
			if(lists.contains(secid)) {
				updateLists.add(map);
			} else {
				insertLists.add(map);
			}
		}
		logger.error("插入的数据" + insertLists.size());
		logger.error("更新的数据" + updateLists.size());
		return insertIntoDB(insertLists) && updateIntoDB(updateLists);
	}

	/**
	 * insert into DB
	 * @param reslutLists 插入数据库的结果
	 * @return 返回插入数据库正确与否
	 */
	private boolean insertIntoDB(List<Map<String, Object>> reslutLists) {
		DBUtil dbUtil = new DBUtil();
		Connection connection = dbUtil.getConnection();
		PreparedStatement statement = null;
		try {
			connection.setAutoCommit(false);
			String deleteSql = deleteSql();
			statement = connection.prepareStatement(deleteSql);
			statement.execute();
			
			String insertSql = insertSql();
			statement = connection.prepareStatement(insertSql);
			for(Map<String, Object> map : reslutLists) {
				statement.setString(1, insertData(map.get("SECID")));
				statement.setString(2, insertData(map.get("SHORTNAME")));
				statement.setFloat(3, insertDataFloat(map.get("LAST_CLOSING_PRICE")));
				statement.setFloat(4, insertDataFloat(map.get("OPENING_PRICE")));
				statement.setFloat(5, insertDataFloat(map.get("LATEST_DEAL_PRICE")));
				statement.setFloat(6, insertDataFloat(map.get("DEAL_QUANTITY")));
				statement.setFloat(7, insertDataFloat(map.get("DEAL_AMOUNT")));
				statement.setFloat(8, insertDataFloat(map.get("DEAL_COUNT")));
				statement.setFloat(9, insertDataFloat(map.get("HIGHEST_PRICE")));
				statement.setFloat(10, insertDataFloat(map.get("LOWEST_PRICE")));
				statement.setFloat(11, insertDataFloat(map.get("EARNINGS_MULTIPLE1")));
				statement.setFloat(12, insertDataFloat(map.get("EARNINGS_MULTIPLE2")));
				statement.setFloat(13, insertDataFloat(map.get("PRICE_FLOAT1")));
				statement.setFloat(14, insertDataFloat(map.get("PRICE_FLOAT2")));
				statement.setFloat(15, insertDataFloat(map.get("POSITION")));
				statement.setFloat(16, insertDataFloat(map.get("SOLD_PRICE5")));
				statement.setFloat(17, insertDataFloat(map.get("SOLD_QUANTITY5")));
				statement.setFloat(18, insertDataFloat(map.get("SOLE_PIRCE4")));
				statement.setFloat(19, insertDataFloat(map.get("SOLE_QUANTITY4")));
				statement.setFloat(20, insertDataFloat(map.get("SOLE_PRICE3")));
				statement.setFloat(21, insertDataFloat(map.get("SOLD_QUANTITY3")));
				statement.setFloat(22, insertDataFloat(map.get("SOLD_PRICE2")));
				statement.setFloat(23, insertDataFloat(map.get("SOLD_QUANTITY2")));
				statement.setFloat(24, insertDataFloat(map.get("SOLD_PRICE1")));
				statement.setFloat(25, insertDataFloat(map.get("SOLD_QUANTITY1")));
				statement.setFloat(26, insertDataFloat(map.get("BUY_PRICE1")));
				statement.setFloat(27, insertDataFloat(map.get("BUY_QUANTITY1")));
				statement.setFloat(28, insertDataFloat(map.get("BUY_PRICE2")));
				statement.setFloat(29, insertDataFloat(map.get("BUY_QUANTITY2")));
				statement.setFloat(30, insertDataFloat(map.get("BUY_PRICE3")));
				statement.setFloat(31, insertDataFloat(map.get("BUY_QUANTITY3")));
				statement.setFloat(32, insertDataFloat(map.get("BUY_PRICE4")));
				statement.setFloat(33, insertDataFloat(map.get("BUY_QUANTITY4")));
				statement.setFloat(34, insertDataFloat(map.get("BUY_PRICE5")));
				statement.setFloat(35, insertDataFloat(map.get("BUY_QUANTITY5")));
				statement.addBatch();
			}
			statement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			try {
				connection.rollback();
				return false;
			} catch (SQLException e1) {
				logger.info(e1.getMessage(), e1);
				return false;
			}
		} finally {
			if(statement != null) {
				try {
					dbUtil.close(statement);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
				
				if(connection != null) {
					try {
						dbUtil.close(connection);
					} catch (SQLException e) {
						logger.error(e.getMessage(), e);
					}
				}
				
			}
		}
		
	}

	/**
	 * get insert SQL
	 * @return 插入数据库的sql
	 */
	private  String insertSql() {
		return "insert into CDMSZ_SJSHQ(SECID, SHORTNAME, LAST_CLOSING_PRICE, " +
				"OPENING_PRICE, LATEST_DEAL_PRICE, DEAL_QUANTITY, DEAL_AMOUNT, " +
				"DEAL_COUNT, HIGHEST_PRICE, LOWEST_PRICE, EARNINGS_MULTIPLE1, " +
				"EARNINGS_MULTIPLE2, PRICE_FLOAT1, PRICE_FLOAT2, POSITION," +
				" SOLD_PRICE5, SOLD_QUANTITY5, SOLE_PIRCE4, SOLE_QUANTITY4, " +
				"SOLE_PRICE3, SOLD_QUANTITY3, SOLD_PRICE2, SOLD_QUANTITY2," +
				" SOLD_PRICE1, SOLD_QUANTITY1, BUY_PRICE1, BUY_QUANTITY1, BUY_PRICE2," +
				" BUY_QUANTITY2, BUY_PRICE3, BUY_QUANTITY3, BUY_PRICE4, BUY_QUANTITY4, " +
				"BUY_PRICE5, BUY_QUANTITY5, POSTDATE) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate)";
	}
	
	/**
	 * get delete SQL
	 * @return 删除sql语句
	 */
	private String deleteSql() {
		return "delete from CDMSZ_SJSHQ";
	}
	
	/**
	 * @param data 数据
	 * @return if data is null, return ""
	 */
	private String insertData(Object data) {
		if(data == null) {
			return "";
		}
		return (String) data;
	}
	
	/**
	 * @param data 数据
	 * @return if data is null, return 0
	 */
	private float insertDataFloat(Object data) {
		if(data == null) {
			return 0;
		}
		return Float.parseFloat((String)data);
	}
	
	/**
	 * @param reslutLists 结果集
	 * @return update database if data is exists
	 */
	private boolean updateIntoDB(List<Map<String, Object>> reslutLists) {
		StringBuffer sql = new StringBuffer("");
		DBUtil dbUtil =  new DBUtil();
		Connection connection = dbUtil.getConnection();
		PreparedStatement statement = null;
		try {
			connection.setAutoCommit(false);
			for(Map<String, Object> map : reslutLists) {
				logger.info(map);
				sql.append("update CDMSZ_SJSHQ set ");
				for(String key : map.keySet()) {
					if(StringUtils.equals(key, "SHORTNAME")) {
						sql.append(key + "=" + insertData(map.get("SECID") + ","));
					} else if(!StringUtils.equals(key, "SECID")) {
						logger.info(key);
						logger.info(map.get(key));
						sql.append(key + "=" + insertDataFloat(map.get(key)) + ",");
					}
				}
				String updateSql = sql.append(" POSTDATE=sysdate where SECID=" + map.get("SECID")).toString();
				statement = connection.prepareStatement(updateSql);
				statement.execute();
			}
			
			connection.commit();
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			try {
				connection.rollback();
			} catch (SQLException e1) {
				logger.error(e1.getMessage(), e1);
			}
			return false;
		}  finally {
			if(connection != null) {
				try {
					dbUtil.close(connection);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(statement != null) {
				try {
					dbUtil.close(statement);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
		
		}
	}
}
