package com.stsing.handler;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * 
 * @author shengdong.he
 *
 */

public abstract class MessageHandler {
	/**
	 *  declare logger
	 */
	private static final Logger logger = Logger.getLogger(MessageHandler.class);
	
	/**
	 *  declare file name
	 */
	private String fileName = "";
	
	/**
	 *  declare path
	 */
	private String path = "";
	
	/**
	 *  declare path successPath
	 */
	private String successPath = "";
	
	/**
	 *  declare fileType
	 */
	private String fileType = "";
	
	
	/**
	 * abstract function save to database
	 * @param path ����ļ���·��
	 * @return ��������Ƿ�ɹ�
	 */
	public abstract boolean saveToDB(String path);

	/**
	 * deal message by message and flag
	 * @param message �������Ϣ
	 * @param flag ��ʶ�����ǵ�һ�����
	 * @return ���ش����Ƿ�ɹ�
	 */
	public synchronized boolean dealMessage(String message, int flag) {
	    logger.log(Level.INFO, "received message success");
	    logger.log(Level.INFO, flag);
	    boolean file_flag = true;
	    if(flag == 1) {
	    	file_flag = saveToFile(message);
	    	logger.log(Level.INFO, "fileName " + fileName);
	    } else {
	    	file_flag = true;
	    }
	    
	    if(!file_flag) {
	    	return false;
	    }
		logger.log(Level.INFO, "save to file success");
		boolean dbflag = saveToDB(path + File.separator + fileName + fileType);
		logger.info("svae to DB : " + dbflag);
		if(dbflag) {
			logger.info("svae to db success");
			moveFilePath();
			logger.info("end..........................");
			}
		return file_flag;
	}
	
	/**
	 * save to file when received message
	 * @param message ����ݱ��浽�ļ�
	 * @return ���ر����ļ��Ƿ�ɹ�
	 */
	public boolean saveToFile(String message) {
		setFileName();
		FileOutputStream fileOutputStream = null;
		ByteBuffer buffer = null;
		FileChannel channel = null;
		try {
			fileOutputStream = new FileOutputStream(path + File.separator + fileName + fileType);
			if(fileType.contains("xml")) {
				fileOutputStream.write(message.getBytes("utf-8"));
				fileOutputStream.flush();
			} else {
				fileOutputStream.write(message.getBytes());
				fileOutputStream.flush();
			}
			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		} finally {
			try {
				if(fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			try {
				if(channel != null) {
					channel.close();
				}
				
				if(buffer != null) {
					buffer.clear();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		
	}
	
	/**
	 * move file to success path
 	 */
	public void moveFilePath() {
		File file = null ;
		String changedPath = successPath + File.separator + getToday();
		File fileSuccess = new File(changedPath);
		if(!fileSuccess.exists()) {
			logger.info("mkdir : " + fileSuccess);
			fileSuccess.mkdir();
		}
		file = new File(path + File.separator + fileName + fileType);
		boolean move_flag = true;
//		boolean move_flag = file.renameTo(new File(changedPath + File.separator+ fileName + fileType));
		logger.log(Level.INFO, "path : " + path + File.separator + fileName + fileType);
		logger.log(Level.INFO, "success path : " + changedPath);
		if(move_flag) {
			file.delete();
			logger.log(Level.INFO, "File moved successfull");
			return;
		}
		logger.info("File moved faild");
	}
	
	/**
	 * set file name
	 */
	private void setFileName() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhh");
		Date date = new Date();
		String dateString = dateFormat.format(date);
		String uuid = UUID.randomUUID().toString().substring(0, 5);
		fileName = dateString + uuid;
	}
	
	/**
	 * set path
	 * @param path �ļ�·��
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * set success path
	 * @param successPath �ɹ��ļ�·��
	 */
	public void setSuccessPath(String successPath) {
		this.successPath = successPath;
	}

	/**
	 * set fileType
	 * @param fileType �ļ�����
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	/**
	 * get today by String
	 * @return ��������ڸ�ʽ
	 */
	private String getToday() {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormat.format(new Date());
	}
}
