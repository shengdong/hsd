package com.stsing.handler;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.util.DBUtil;
/**
 * 
 * @author k1193
 *
 */
public class DBHandler {
	/**
	 * declare logger
	 */
	private final static Logger logger = Logger.getLogger(DBHandler.class);
	

	/**
	 * save to DB from path
	 * @param path 入库的文件地址
 	 * @return  是否入库成功
	 */
	public static boolean saveToDB(String path) {
		logger.info("begin to save to oracle : " + path);
		Connection connection = null;
		PreparedStatement statement = null;
		BufferedReader reader = null;
		DBUtil dbUtil = new DBUtil();
		try {
			connection = dbUtil.getConnection();
			connection.setAutoCommit(false);
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(path)));

			String deleteSql = reader.readLine();
			String insertSql = reader.readLine();

			reader.readLine();

			statement = connection.prepareStatement(deleteSql);
			statement.execute();

			statement = connection.prepareStatement(insertSql);
			String line = "";
			int countLine = 0;
			while ((line = reader.readLine()) != null) {
				if(StringUtils.isBlank(line) || StringUtils.isEmpty(line)) {
					break;
				}
				countLine ++;
				line = line + " ";
				String[] data = line.split(";;;");
				for (int i = 0; i < data.length; i++) {
					if(i == data.length - 1) {
						data[i] = data[i].trim();
					}
					statement.setString(i + 1, data[i]);
				}
				statement.addBatch();
				if (countLine > 50000) {
					statement.executeBatch();
					connection.commit();
					countLine = 0;
				}
			}
			statement.executeBatch();
			connection.commit();
			return true;

		} catch (Exception e) {
			try {
				logger.error(e.getMessage(), e);
				connection.rollback();
				return false;
			} catch (SQLException e1) {
				logger.error(e1.getMessage(), e1);
				return false;
			}

		} finally {
			try {
				if (statement != null) {
					dbUtil.close(statement);
				}
			} catch (Exception e2) {
				logger.error(e2.getMessage(), e2);
			}
			try {
				if (connection != null) {
					dbUtil.close(connection);
				}
			} catch (Exception e2) {
				logger.error(e2.getMessage(), e2);
			}
			try {

				if (reader != null) {
					reader.close();
				}
			} catch (Exception e2) {
				logger.error(e2.getMessage(), e2);
			}
		}
	}

	/**
	 * save to DB from message
	 * @param msg 入库的数据
	 * @param insertSql 执行的sql语句
	 * @return 返回保存到数据库是否成功
	 */
	public static boolean saveToDBFromMessage(List<String> msg, String insertSql) {
		logger.info("insert sql : " + insertSql);
		Connection connection = null;
		PreparedStatement statement = null;
		DBUtil dbUtil = new DBUtil();
		try {
			connection = dbUtil.getConnection();
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(insertSql);
			for (String da : msg) {
				String[] das = da.split(",");
				for (int i = 0; i < das.length; i++) {
					if (StringUtils.isNotBlank(das[i])) {
						das[i] = das[i].trim();
					}
					statement.setString(i + 1, das[i]);
				}
				statement.addBatch();
			}
			statement.executeBatch();
			connection.commit();
			statement.close();
			connection.close();
			return true;

		} catch (Exception e) {
			try {
				logger.error(e.getMessage(), e);
				connection.rollback();
				return false;
			} catch (SQLException e1) {
				logger.error(e1.getMessage(), e1);
				return false;
			}

		} finally {
			try {
				if (statement != null) {
					dbUtil.close(statement);
				}
				if (connection != null) {
					dbUtil.close(connection);
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

}
