package com.stsing.handler;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.stsing.util.DBUtil;

/**
 * 
 * @author k1193
 *
 */
public class SHCHNameHandler extends MessageHandler {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SHCHNameHandler.class);

	
	@Override
	public boolean saveToDB(String path) {
		BufferedReader reader = null;
		Connection connection = null;
		PreparedStatement statement = null;
		DBUtil dbUtil = new DBUtil();
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(path)));
			String deleteSql = reader.readLine();
			String insertSql = reader.readLine();

			connection = dbUtil.getConnection();
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(deleteSql);
			statement.execute();

			statement = connection.prepareStatement(insertSql);
			String line = "";
			while ((line = reader.readLine()) != null) {
				String datas[] = line.split(",");
				for (int i = 0; i < datas.length; i++) {
					statement.setString(i + 1, datas[i].trim());
				}
				statement.addBatch();
			}

			statement.executeBatch();
			connection.commit();

			return true;

		} catch (Exception e) {
			try {
				logger.error(e.getMessage(), e);
				connection.rollback();
				return false;
			} catch (SQLException e1) {
				logger.error(e1.getMessage(), e1);
				return false;
			}
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
				
				try {
					statement.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
				
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}

		
	}

}
