package com.stsing.handler;

import org.apache.log4j.Logger;
/**
 * 
 * @author k1193
 *
 */
public class DSSHandler extends MessageHandler {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(DSSHandler.class);
	
    
	@Override
	public boolean saveToDB(String path) {
		logger.info("begin to save to DB");
		logger.info("path : " + path);
		boolean flag = DBHandler.saveToDB(path);
		return flag;
	}

}
