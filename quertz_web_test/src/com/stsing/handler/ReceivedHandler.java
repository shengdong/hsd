package com.stsing.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.stsing.dispatcher.JobDisPatcher;
import com.stsing.model.Info;
import com.stsing.util.ErrorDBHandler;
public class ReceivedHandler {
	private static final Logger logger = Logger.getLogger(ReceivedHandler.class);

	
	public static String sendToClient(String jobName) {
		Gson gson = new Gson();
		Info info = new Info();
		List<Info> result = new ArrayList<Info>();
	    if(StringUtils.isBlank(jobName)) {
	    	logger.info("job name is error: " + jobName);
	    	info.setSuccess(false);
            info.setErrorMsg("job name is blank " + jobName);
            info.setObject(jobName);
            result.add(info);
            ErrorDBHandler.insertError(result);
            return gson.toJson(result);
	     }
	    
	    logger.info("jobName : " + jobName);
	    
	    return JobDisPatcher.getIntance().executeJob(jobName);
	}
}
