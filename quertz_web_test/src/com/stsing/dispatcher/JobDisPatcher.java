package com.stsing.dispatcher;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.provide.job.ProvideAbstract;
import com.stsing.util.DBUtil;

public class JobDisPatcher {
	private static final Logger logger = Logger.getLogger(JobDisPatcher.class);

	private static JobDisPatcher jobDisPatcher = null;
	
	
	public synchronized static JobDisPatcher getIntance() {
		if(jobDisPatcher == null) {
			synchronized (JobDisPatcher.class) {
				jobDisPatcher = new JobDisPatcher();
			}
		}
		return jobDisPatcher;
	}
	
	public String executeJob(String jobName) {
		/**
		 * judge this jobName is send job or provide job
		 * 
		 */
		String sql = "select * from quertz t where t.jobname = ?";
		Map<String, Object> map = DBUtil.getIntance().getDataFromDB(sql).get(0);
		String serivce = (String)map.get("SERVICE");
		if(StringUtils.isBlank(serivce)) {
			String section = (String)map.get("FLAG");
			String executeClass = (String)map.get("EXECUTECLASS");
			return executeProvideJob(section, executeClass);
		} else {
			return executeSendJob();
		}
	}
	
	private String executeSendJob() {
		return "";
	}
	
	private String executeProvideJob(String section, String executeClass) {
		try {
			Class<ProvideAbstract> classJob = (Class<ProvideAbstract>) Class.forName(executeClass);
			ProvideAbstract provideAbstract = classJob.newInstance();
			provideAbstract.setSection(section);
			provideAbstract.beginToCreateFile();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return "";
	}
}
