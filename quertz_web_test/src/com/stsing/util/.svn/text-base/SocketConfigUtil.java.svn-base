package com.stsing.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 
 * @author k1193
 *
 */
public class SocketConfigUtil {

	/**
	 *  declare logger
	 */
	private static final  Logger logger = Logger.getLogger(QuertzConfigUtil.class.getName());
	
	/**
	 *  declare CHARSET key
	 */
	public static  String CHARSET = "gbk";
	
	/**
	 *  declare FILEPATH key
	 */
//    private static final String FILEPATH = "src/conf/Socket.ini";
    
    private static final String FILEPATH = "../conf/Socket.ini";
	
	/**
	 *  declare FLAG key
	 */
	private static final String FLAG = "flag";
	
	/**
	 *  declare PATH key
	 */
	private static final String PATH = "path";
	
	/**
	 *  declare CLASS key
	 */
	private static final String CLASS = "class";
	
	/**
	 *  declare resultMapOfFlag key
	 */
	private static Map<String, Integer> resultMapOfFlag = null;
	
	
	/**
	 * get value
	 * @param key 查找的key
	 * @return value 要查找的value
	 */
	public static String getValue(String key) {
		HierarchicalINIConfiguration configuration = null;
		try {
			configuration = new HierarchicalINIConfiguration(FILEPATH);
			String value = configuration.getString(key);
			return value;
		} catch (ConfigurationException e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}
	
	/**
	 * get value of int
	 * @param key 查找的key
	 * @return 查找的value
	 */
	public static int getValueOfInt(String key) {
		String value = getValue(key);
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return 0;
	}
	
	/**
	 * get object by flag
	 * @param flag 渠道标识
	 * @param key 这个渠道标识下面的key
	 * @return 反射类的对象
	 */
	@SuppressWarnings("unchecked")
	public static Object getObjectByFlag(String flag, String key) {
		HierarchicalINIConfiguration configuration = null;
		try {
			configuration = new HierarchicalINIConfiguration(FILEPATH);
			
			Set<String> setOfSections = configuration.getSections();
			Iterator<String> sectionNames = setOfSections.iterator();
			while(sectionNames.hasNext()) {
				 String seactionName = sectionNames.next();
				 SubnodeConfiguration sObj = configuration.getSection(seactionName);
				 //judge flag is exist
				 String String_flag = sObj.getString(FLAG);
				 if(StringUtils.isBlank(String_flag)) {
					 continue;
				 }
				 String path_flag = sObj.getString(FLAG);
				 if(StringUtils.equals(flag, path_flag)) {
					 if(StringUtils.equals(CLASS, key)) {
						Class result = Class.forName(sObj.getString(CLASS)); 
						return result.newInstance();
					 }
					 return sObj.getString(key);
				 } 
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * @return get path and flag into Map
	 */
	public static Map<String, Integer> getPathAndFlag() {
		if(resultMapOfFlag != null) {
			return resultMapOfFlag;
		}
		Map<String, Integer> result = new HashMap<String, Integer>();
		HierarchicalINIConfiguration configuration = null;
		try {
			configuration = new HierarchicalINIConfiguration(FILEPATH);
			
			Set<String> setOfSections = configuration.getSections();
			Iterator<String> sectionNames = setOfSections.iterator();
			while(sectionNames.hasNext()) {
				 String seactionName = sectionNames.next();
				 SubnodeConfiguration sObj = configuration.getSection(seactionName);
				 String class_path = sObj.getString(CLASS);
				 if(StringUtils.isNotBlank(class_path)) {
					 result.put(sObj.getString(PATH), sObj.getInt(FLAG));
				 }
			}
			resultMapOfFlag = result;
			return result;
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	/**
	 * GET VALUE BY SECTIONS AND KEY
 	 * @param sections 节点名称
	 * @param key 查找的key
	 * @return 查找的value
	 */
	public static String getValue(String sections, String key) {
		HierarchicalINIConfiguration configuration = null;
		try {
			configuration = new HierarchicalINIConfiguration(FILEPATH);
			Set<String> setOfSections = configuration.getSections();
			Iterator<String> sectionNames = setOfSections.iterator();
			while(sectionNames.hasNext()) {
				String seactionName = sectionNames.next();
				if(StringUtils.equals(sections, seactionName)) {
					String result = configuration.getSection(seactionName).getString(key);
					return result;
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}
	
	
	/**
	 * @return get today by String
	 */
	public static String getToday() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(date);
	}
	
	/**
	 * @return get id of cdmsz_sjshq
	 */
	public static List<String> getSJHQID() {
		List<String> idList = new LinkedList<String>();
		DBUtil dbUtil = new DBUtil();
		String sql = "select secid from cdmsz_sjshq";
		Connection connection =  dbUtil.getConnection();
		PreparedStatement statement = null;
		ResultSet set = null;
		try {
			statement = connection.prepareStatement(sql);
			set = statement.executeQuery();
			while(set.next()) {
				idList.add(set.getString("secid"));
			}
			return idList;
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return idList;
		} finally {
			if(set != null) {
				try {
					set.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
		}
		
	}
	
	/**
	 * @return get short name and id
	 */
	public static Map<String, String> getshortNameMap() {
		Map<String, String> shortNameMap = new HashMap<String, String>(5000);
		DBUtil dbUtil = new DBUtil();
		String sql = "select * from cdmsz_security t";
		Connection connection =  dbUtil.getConnection();
		PreparedStatement statement = null;
		ResultSet set = null;
		try {
			statement = connection.prepareStatement(sql);
			set = statement.executeQuery();
			while(set.next()) {
				shortNameMap.put(set.getString("SECURITYID"), set.getString("SYMBOL"));
			}
			return shortNameMap;
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return shortNameMap;
		} finally {
			if(set != null) {
				try {
					set.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
		}
	}
}
