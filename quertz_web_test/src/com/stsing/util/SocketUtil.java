package com.stsing.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.stsing.model.Info;

/**
 * 
 * @author k1193
 *
 */
public class SocketUtil {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SocketUtil.class);
	
	/**
	 * declare gson
	 */
	private static Gson gson = null;
	
	/**
	 * 初始化gson
	 */
	static {
		if(gson == null) {
			gson = new Gson();
		}
	}
	
	/**
	 * client send middle ware to server
	 * @param ip 发送的ip
	 * @param port 发送的端口
	 * @param msg 发送的数据
	 * @param flag 发送的标识
	 * @param timeout 超时时间
	 * @return 服务器返回的信息
	 */
	public static String sendMessageToMW(String ip, int port, String msg, String flag, int timeout) {
		logger.info("begin to send to middleWare");
		String header = getHeader(msg, flag);
		String footer = getFooter();
		String sendMessage = header + msg + footer;
		logger.info("sendMessage : " + sendMessage);
		return socketSend(ip, port, sendMessage, timeout);
	}
	
	/**
	 * return  
	 * @param msg 发送的数据
	 * @param flag 发送的标识
	 * @return send message header
	 */
	private static String getHeader(String msg, String flag) {
		int length = msg.getBytes().length;
		return "HANGQING.0000000000ZZZZZ" + getLengthOfString(length) + flag;
	}
	
	/**
	 * @return return send message footer
	 */
	private static String getFooter() {
		return "HANGQING.0000000000YYYYY";
	}
	
	/**
	 * get like 0000002510 string by length
	 * @param length 长度
	 * @return 定位长度头
	 */
	private static String getLengthOfString(int length) {
		int stringLength = (length + "").length();
		int needLength = 10 - stringLength;
		StringBuilder builder = new StringBuilder("");
		for(int i=0; i<needLength; i++) {
			builder = builder.append("0");
		}
		builder = builder.append(length + "");
		return builder.toString();
	}
	
	/**
	 * 
	 * @param ip 发送的ip
	 * @param port 发送的端口
	 * @param msg 发送的信息
	 * @param timeout 超时时间
	 * @return 服务器收到的信息
	 */
	private static String socketSend(String ip, int port, String msg, int timeout) {
		Socket socket = null;
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			socket = new Socket(ip, port);
			socket.setSoTimeout(timeout);
			inputStream = socket.getInputStream();
			outputStream = socket.getOutputStream();
			outputStream.write(msg.getBytes(), 0, msg.getBytes().length);
			outputStream.flush();
			byte[] receivedMsg = new byte[1024];
			inputStream.read(receivedMsg);
			outputStream.close();
			inputStream.close();
			socket.close();
			return new String(receivedMsg).trim();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return "";
			
		} finally {
			try {
				outputStream.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
			try {
				inputStream.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
			try {
				socket.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * 
	 * @param message json的字符串
	 * @return 字符串转换成对象
	 */
	public static List<Info> getLists(String message) {
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(message);
		
		JsonArray array = element.getAsJsonArray();
		List<Info> result = new ArrayList<Info>();
		Iterator<JsonElement> it = array.iterator();
		while(it.hasNext()) {
			Info info = gson.fromJson(it.next(), Info.class);
			result.add(info);
		}
		return result;
	}
}
