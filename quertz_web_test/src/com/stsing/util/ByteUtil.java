package com.stsing.util;

/**
 * 
 * @author k1193
 *
 */
public class ByteUtil {

	/**
	 * merge byte ....
	 * @param bytes 合并的byte数据
	 * @return 合并后的byte数据
	 */
	public static byte[] merge(byte[] ...bytes) {
		int amount = 0;
		for (byte[] by : bytes) {
			amount = amount + by.length;
		}
		byte[] result = new byte[amount];
		int count = 0;
		for (byte[] by : bytes) {
			System.arraycopy(by, 0, result, count, by.length);
			count = count + by.length;
		}
		return result;
	}

	/**
	 * extend byte
	 * @param messageData 要扩展的byte组
	 * @return 扩大一倍后byte数组
	 */
	public static byte[] extendBytes(byte[] messageData) {
		byte[] result = new byte[messageData.length];
		return merge(messageData, result);
	}
}
