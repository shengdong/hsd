package com.stsing.util;

import com.google.gson.Gson;

/**
 * 
 * @author k1193
 *
 */
public class GsonUtil {
    
	/**
	 * declare gson
	 */
	private static Gson gson = null;
	
	
	/**
	 * 
	 * @return ������gson
	 */
	public static Gson getInstance() {
		if(gson == null) {
			gson = new Gson();
		}
		return gson;
	}
}
