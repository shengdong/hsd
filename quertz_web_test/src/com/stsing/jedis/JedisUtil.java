package com.stsing.jedis;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface JedisUtil {
	public static final String CLUSTER = "cluster";
	
	public static final String JEDIS = "jedis";
	
	public boolean ping();
	
	public Map<String, String> hgetAll(String key);
	
	public String hmset(String key, Map<String, String> map);

	public Set<String> keys(String pattern);
	
	public Set<String> smembers(String key);
	
	public List<Map<String, String>> piplined(Set<String> keys); 
	
	public void close();
}
