package com.stsing.provide.job;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.stsing.dispatcher.JobDisPatcher;

public class ProvideJob implements Job {
	private static final Logger logger = Logger.getLogger(ProvideJob.class);
	

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName =  context.getTrigger().getJobKey().getName();
		String result = JobDisPatcher.getIntance().executeJob(jobName);
		logger.info(jobName + " execute : " + result);
	}
}
