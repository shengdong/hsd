package com.stsing.provide.job;

public class ProvideQB extends ProvideAbstract {
	
	@Override
	public String getFileName() {
		return "Qbdata";
	}

	@Override
	public String getFileSplit() {
		return ",";
	}

	@Override
	public String getFileType() {
		return ".csv";
	}

	public static void main(String[] args) {
		ProvideAbstract provide = new ProvideQB();
		provide.beginToCreateFile();
	}
}
