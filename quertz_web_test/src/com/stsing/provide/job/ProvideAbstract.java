package com.stsing.provide.job;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.QuertzConfigUtil;

public abstract class ProvideAbstract implements ProvideTemplate {
	private static final Logger logger = Logger.getLogger(ProvideAbstract.class);

	private DBUtil dbutil = null;
	
	private String fileName = "";
	
	private String section;

	@Override
	public List<String> getDataFromOracle(String sql) {
		List<String> result = new ArrayList<>(2000);
		int number = Integer.parseInt(sql.split("@")[1]);
		String sqlString = sql.split("@")[0];
		logger.info("sql hsd : " + sqlString);
		dbutil = DBUtil.getIntance();
		Connection connection = dbutil.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		StringBuilder builder = new StringBuilder();
		try {
			statement = connection.prepareStatement(sqlString);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				for (int i = 1; i <= number; i++) {
					String field = resultSet.getString(i);
					if(StringUtils.isBlank(field)) {
						field = "\"" + "\"";
					} else {
						field = "\"" + field + "\"";
					}
					
					if (i == number) {
						builder.append(field);
					} else {
						builder.append(field + getFileSplit());
					}
				}
				result.add(builder.toString());
				builder.delete(0, builder.toString().length());
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			try {
				if (statement != null) {
					statement.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			try {
				if (resultSet != null) {
					resultSet.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return result;
	}

	@Override
	public boolean createFileToPath(List<String> dataSet, String titleData) {
		mkdir(getFilePath() + File.separator + getToDay());
		String filePath = getFilePath() + File.separator + getToDay() + File.separator + getFileName() + getFileType();
		BufferedWriter writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath)));
			writer.write(titleData);
			writer.newLine();

			for (String data : dataSet) {
				writer.write(data);
				writer.newLine();
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return true;
	}

	@Override
	public boolean createAllFile(List<String> sqls, Map<String, String> titleDatas) {
		for (String sql : sqls) {
			List<String> list = getDataFromOracle(sql);
			String tableName = getTableNameFrom(sql);
			fileName = StringUtils.upperCase(tableName);
			boolean flag = createFileToPath(list, titleDatas.get(tableName));
			if (!flag) {
				return false;
			}
		}
		return createOKFile();
	}
	
	@Override
	public void setSection(String section) {
		this.section = section;
	}
	
	public String getSection() {
		return section;
	}

	@Override
	public boolean createOKFile() {
		logger.info("begin to create OK file");
		String file = getFilePath() + File.separator + getToDay() + File.separator
				+ QuertzConfigUtil.getValue("fileFlag") + "_" + getToDay() + ".OK";
		File fileOK = new File(file);

		if (!fileOK.exists()) {
			try {
				fileOK.createNewFile();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return false;
	}

	@Override
	public String getFileName() {
		if(fileName.startsWith("WIND")) {
			fileName = "NBCB_" + fileName;
		}
		fileName = fileName.replaceAll("DSS", "NBCB");
		return fileName;
	}
	
	@Override
	public String getFilePath() {
		return QuertzConfigUtil.getValue("filepath") + getSection();
	}

	public boolean beginToCreateFile() {
		List<String> sqls = new ArrayList<>();
		Map<String, String> titleDatas = new HashMap<>();
		Map<String, String> map = QuertzConfigUtil.getMapBySections(getSection());
		for (String key : map.keySet()) {
			if (key.startsWith("sql")) {
				sqls.add(map.get(key));
			} else {
				titleDatas.put(key, map.get(key));
			}
		}

		return createAllFile(sqls, titleDatas);
	}

	private String getToDay() {
		return ConfigUtil.getToday("YYYYMMdd");
	}

	private String getTableNameFrom(String sql) {
		logger.info(sql);
		String tableName = sql.split("from")[1].split("where")[0];
		return tableName.trim();
	}

	private void mkdir(String filePath) {
		logger.info(filePath);
		File file = new File(filePath);
		if (!file.exists()) {
			logger.info("begin to create mkdir" + file.mkdirs());
		}
	}
}
