package com.stsing.client;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.stsing.model.Quertz;
import com.stsing.util.QuertzConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class BootStrap {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(BootStrap.class);
	
	/**
	 * declare scheduler
	 */
	private Scheduler scheduler = null;
	
	/**
	 * declare bootstrap
	 */
	public static BootStrap bootStrap = null;
	
	
	
	/**
	 * 
	 * @return get instance
	 */
	public synchronized static BootStrap getInstance() {
		if(bootStrap == null) {
			synchronized (BootStrap.class) {
				bootStrap = new BootStrap();
			}
		}
		return bootStrap;
	}
	
	
	/**
	 * begin to execute quartz, 
	 */
	@SuppressWarnings("unchecked")
	public void execute() {
		
		try {
			scheduler =  StdSchedulerFactory.getDefaultScheduler();
			for(Quertz quertz : QuertzConfigUtil.getListMapOfJobFromDB()) {
				String className = quertz.getClassName().trim();
				String jobName = quertz.getJobName().trim();
				String triggerName = quertz.getTriggerName().trim();
				String triggerTime = quertz.getTriggerTime().trim();
				
				Class<Job> classJob = (Class<Job>) Class.forName(className);
				scheduler.scheduleJob(JobBuilder.newJob(classJob)
						.withIdentity(jobName).build(), TriggerBuilder.newTrigger().withIdentity(
								triggerName).withSchedule(
								CronScheduleBuilder.cronSchedule(triggerTime))
								.startNow().build());
			}
			scheduler.start();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * stop code
	 */
	public void stop() {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
	}
	/**
	 * 热加载
	 */
	public void reLoad() {
		stop();
		execute();
	}
}
