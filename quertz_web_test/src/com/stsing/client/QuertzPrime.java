package com.stsing.client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.stsing.util.DBUtil;
import com.stsing.util.QuertzConfigUtil;
/**
 * 
 * @author k1193
 * 
 */
public class QuertzPrime {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(QuertzPrime.class);

	/**
	 * declare dbutil
	 */
	private DBUtil dbUtil = null;

	/**
	 * declare main
	 */
	private Main bootStrap = null;

	/**
	 * 初始化quertzPrime
	 */
	public QuertzPrime() {
		dbUtil = new DBUtil();
		bootStrap = new Main();
	}

	/**
	 * ִ��main����
	 * 
	 * @param args
	 *            ִ��main����Ҫ����Ĳ���
	 */
	public static void main(String[] args) {
		PropertyConfigurator.configure("../conf/log4j.properties");
		new QuertzPrime().runMain();
	}
	

	/**
	 * ����main����
	 */
	public void runMain() {
		try {
			final String service = QuertzConfigUtil.getValue("service");
			final String process = QuertzConfigUtil.getValue("process");
			int heart = QuertzConfigUtil.getIntValue("heartbeat");
			final int primeTimeOut = QuertzConfigUtil.getIntValue("primeTimeOut");
			logger.warn("service : " + service + " process: " + process + " heart : " + heart + " primeTimeOut : " + primeTimeOut);
			
			final long heartbeat = (long) heart;
			if (shouldRun(service, process, heartbeat, primeTimeOut)) {
				logger.info("should run : true");
				bootStrap.run();
				new Thread(new Runnable() {
					@Override
					public void run() {
						boolean flag = true;
						while (flag) {
							try {
								logger.warn("update status..........");
								flag = updateStatus(service, process);
								Thread.sleep(heartbeat * 1000);
							} catch (InterruptedException e) {
								logger.error(e.getMessage(), e);
							}
						}
						bootStrap.stop();
						checkRun(service, process, heartbeat, primeTimeOut);
					}
				}).start();
				
			} else {
				checkRun(service, process, heartbeat, primeTimeOut);
			}
				
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * @param service 进程号
	 * @param process 主备名称
	 * @param heartbeat 心跳设置
	 */
	private void checkRun(final String service, final String process, final long heartbeat, final int primeOutTime) {
		boolean hasRun = false;
		while (!hasRun) {
			try {
				Thread.sleep(heartbeat * 1000);
				logger.info("check should run........");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			if (shouldRun(service, process, heartbeat, primeOutTime)) {
				logger.info("Should run: true");
				hasRun = true;
				bootStrap.run();
				new Thread(new Runnable() {
					@Override
					public void run() {
						boolean flag = true;
						while (flag) {
							try {
								logger.warn("update statuts.........");
								flag = updateStatus(service, process);
								Thread.sleep(heartbeat * 1000);
							} catch (InterruptedException e) {
								logger.error(e.getMessage(), e);
							}
						}
						bootStrap.stop();
						checkRun(service, process, heartbeat, primeOutTime);
					}
				}).start();
				
			}
		}
	}

	/**
	 * �������
	 * 
	 * @param service
	 *            ��������
	 * @param process
	 *            ������̵����
	 */
	private boolean updateStatus(String service, String process) {
		Connection connection = dbUtil.getConnection();
		PreparedStatement statement = null;
		ResultSet set = null;
		try {
			String selectSql = "select process from dualprocess where service = ? ";
			statement = connection.prepareStatement(selectSql);
			statement.setString(1, service);
			set = statement.executeQuery();
			while (set.next()) {
				String processNow = set.getString(1);
				if (!StringUtils.equals(process, processNow)) {
					return false;
				}
			}
			String sql = "update dualprocess set updatetime = sysdate, process=? where service = ? ";
			statement = connection.prepareStatement(sql);
			statement.setString(1, process);
			statement.setString(2, service);
			statement.execute();
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return true;
		} finally {
			try {
				if (set != null) {
					set.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * �жϳ����Ƿ�Ӧ������
	 * 
	 * @param service
	 *            ��������
	 * @param process
	 *            ��̵����
	 * @param heartbeat
	 *            �����ʵ����
	 * @return �����Ƿ�Ӧ������ true or false
	 */
	private boolean shouldRun(String service, String process, long heartbeat, int primeOutHeart) {
		boolean shouldRun = true;
		Connection connection = dbUtil.getConnection();
		String sql = "select process, ROUND(TO_NUMBER(SYSDATE - UPDATETIME) * 24 * 60 * 60) from dualprocess where service = ? ";
		PreparedStatement statement = null;
		ResultSet set = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, service);
			set = statement.executeQuery();
			if (!set.next()) {
				String insertSql = "insert into dualprocess (SERVICE, STATUS, UPDATETIME, PROCESS) values (?, 'RUNNING', SYSDATE, ?)";
				statement = connection.prepareStatement(insertSql);
				statement.setString(1, service);
				statement.setString(2, process);
				statement.execute();
				return true;
			} else {
				String nowProcess = set.getString(1);
				int interval = set.getInt(2);
				if (StringUtils.equals(nowProcess, process)) {
					return true;
				} else {
					if (interval > primeOutHeart) {
						String updateSql = "update dualprocess set updatetime = sysdate, process=? where service = ? ";
						statement = connection.prepareStatement(updateSql);
						statement.setString(1, process);
						statement.setString(2, service);
						statement.execute();
						return true;
					} else {
						logger.warn("interval : " + interval + " primeOutHeart : " + primeOutHeart);
						shouldRun = false;
					}
				}
			}
			set.close();
			statement.close();
			connection.close();
			return shouldRun;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return shouldRun;
		} finally {
			if (set != null) {
				try {
					set.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}

			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}
}
