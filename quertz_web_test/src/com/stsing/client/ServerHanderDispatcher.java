package com.stsing.client;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.stsing.handler.ReceivedHandler;
/**
 * NettyHandler
 *
 */
public class ServerHanderDispatcher extends SimpleChannelHandler {
	private final String HQPT1 = "hqpt.001";
	
	private final String HQPT2 = "hqpt.002";
	
	
	/**
	 * declare logger
	 */
    private static final Logger logger = Logger.getLogger(ServerHanderDispatcher.class);
    
    
	@Override
	public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		logger.info("Channel " + e.getChannel().hashCode() + " closed");
	}
	
	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		logger.info("new Channel hashcode: " + e.getChannel().hashCode() + " connect");
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		logger.error("exception from downStrem : " + e);
		e.getChannel().close();
		
	}
	
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		 ChannelBuffer receivedChannelBuffer = (ChannelBuffer) e.getMessage();
		 Channel channel = e.getChannel();
		 byte[] data = receivedChannelBuffer.array();
		 String receivedXml = new String(data).trim();
		 String result = "";
		 logger.info("received message : " + receivedXml);
		 if(receivedXml.startsWith(HQPT2)) {
			 /**
			  * 收到热加载的数据
			  */
			 BootStrap.getInstance().reLoad();
			 logger.info("****************************");
			 logger.info("****************************");
			 logger.info("****quertz reload success***");
			 logger.info("****************************");
			 logger.info("****************************");
			 channel.write("success");
			 channel.close();
		 } else if(receivedXml.startsWith(HQPT1)) {
			 /**
			  * 进行手工调度
			  */
			 String jobName = receivedXml.split(";")[1].trim();
			 result = ReceivedHandler.sendToClient(jobName);
			 channel.write(result);
			 channel.close();
		 } else {
			 channel.write("error");
			 channel.close();
		 }
	}
}

