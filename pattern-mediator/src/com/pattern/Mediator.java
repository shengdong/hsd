package com.pattern;

public abstract class Mediator {
	
	public abstract void contact(String content, Colleague coll);
	
}
