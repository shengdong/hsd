package com.pattern;

public class Colleague {

	public String name;
	
	public Mediator mediator;
	
	public Colleague(String name, Mediator mediator) {
		this.name = name;
		this.mediator = mediator;
	}
}
