package com.pattern;

public class Client {

	public static void main(String[] args) {
		ConcreteMediator mediator = new ConcreteMediator();
		
		ColleagueA colleagueA = new ColleagueA("Zhang San", mediator);
		ColleagueB colleagueB = new ColleagueB("Li Si", mediator);
		
		mediator.setColla(colleagueA);
		mediator.setCollb(colleagueB);
		
		colleagueA.contact("我是Ａ，我要和同事Ｂ说说工作的事情");
		colleagueB.contact("我是Ｂ，我下午有时间，下午商量吧");
	}
}
