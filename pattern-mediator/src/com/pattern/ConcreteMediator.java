package com.pattern;

public class ConcreteMediator extends Mediator {
	private ColleagueA colla;
	
	private ColleagueB collb;
	

	public ColleagueA getColla() {
		return colla;
	}




	public void setColla(ColleagueA colla) {
		this.colla = colla;
	}




	public ColleagueB getCollb() {
		return collb;
	}




	public void setCollb(ColleagueB collb) {
		this.collb = collb;
	}




	@Override
	public void contact(String content, Colleague coll) {
		if(coll == colla) {
			collb.getMessage(content);
		} else {
			colla.getMessage(content);
		}
	}

}
