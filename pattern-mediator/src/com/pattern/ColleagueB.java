package com.pattern;

public class ColleagueB extends Colleague {
	

	public ColleagueB(String name, Mediator mediator) {
		super(name, mediator);
	}

	
	public void getMessage(String message) {
		System.out.println(" colleague b " + name + " get message :" + message);
	}
	
	public void contact(String message) {
		mediator.contact(message, this);
	}
}
