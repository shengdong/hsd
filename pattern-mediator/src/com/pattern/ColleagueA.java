package com.pattern;

public class ColleagueA extends Colleague {
	

	public ColleagueA(String name, Mediator mediator) {
		super(name, mediator);
	}

	
	public void getMessage(String message) {
		System.out.println(" colleague a " + name + " get message :" + message);
	}
	
	public void contact(String message) {
		mediator.contact(message, this);
	}
}
