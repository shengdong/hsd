package com.redis;

import com.util.ConfigUtil;
import com.util.JedisClusterUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by hsd on 16-12-5.
 */
public class RedisTest {
    private static final Logger logger = Logger.getLogger(RedisTest.class);

    private static JedisCluster jedisCluster;


    public static void main(String[] args) {
        Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();

        String ips[] = ConfigUtil.getString("redis.clusterIP").split(";");

        for (String ip : ips) {
            String host = ip.split(":")[0].trim();
            String portString = ip.split(":")[1].trim();
            int port = Integer.parseInt(portString);
            HostAndPort hostAndPort = new HostAndPort(host, port);
            jedisClusterNodes.add(hostAndPort);
        }

        jedisCluster = new JedisCluster(jedisClusterNodes);
//        String key = "count*";
//        JedisClusterUtil clusterUtil = new JedisClusterUtil();
//        Set<String> sets = clusterUtil.keys(key, jedisCluster);
//        for(String k : sets) {
//            jedisCluster.del(k);
//        }
//
//        try {
//            clusterUtil.close();
//            jedisCluster.close();
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        }
        Jedis jedis = new Jedis("12.99.108.155", 6379);
        Set<String> sets =  jedis.keys("*");
        for(String key : sets) {
            if(StringUtils.equals(jedis.type(key), "hash")) {
                jedisCluster.hmset(key, jedis.hgetAll(key));
            } else if(StringUtils.equals(jedis.type(key), "set")) {
                Set<String> keys = jedis.smembers(key);
                for(String s : keys) {
                    jedisCluster.sadd(key, s);
                }
            }
        }
    }
}
