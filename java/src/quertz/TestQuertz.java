package quertz;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import job.TestJob;

/**
 * Created by hsd on 16-12-2.
 */
public class TestQuertz {
    private static final Logger logger = Logger.getLogger(TestQuertz.class);

    private Scheduler scheduler = null;


    public TestQuertz () {
        try {
            scheduler = new StdSchedulerFactory().getDefaultScheduler();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void start() {
        try {

            JobDetail job = JobBuilder.newJob(TestJob.class).withIdentity("job1", "group1").build();

            SimpleScheduleBuilder simpleScheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                    .withIntervalInSeconds(5).repeatForever();

            Trigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1").startNow()
                    .withSchedule(simpleScheduleBuilder).build();

            scheduler.scheduleJob(job, trigger);

            scheduler.start();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void stop() {
        try {
            scheduler.shutdown();
        } catch (Exception  e) {
            logger.error(e.getMessage(), e);

        }
    }

    public static void main(String[] args) throws Exception{
//        PropertyConfigurator.configure("./control/log4j.properties");
        TestQuertz quertz = new TestQuertz();
        quertz.start();
    }

}
