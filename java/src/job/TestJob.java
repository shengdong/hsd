package job;


import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by hsd on 16-12-2.
 */
public class TestJob implements Job {
    private static final Logger logger = Logger.getLogger(TestJob.class);


    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        System.out.println("fojobName : " + jobExecutionContext.getTrigger().getJobKey().getName());
    }
}
