package redis;

import org.apache.log4j.Logger;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by hsd on 16-12-24.
 */
public class JedisPub {
    private static final Logger logger = Logger.getLogger(JedisPub.class);


    @Test
    public  void  testJedis() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(100);
        config.setMaxIdle(5);

        config.setMaxWaitMillis(1000 * 100);

        final JedisPool jedisPool = new JedisPool(config, "127.0.0.1", 6379, 1000*10);


        String channel = "hsd";
        String message = "hello";

       for(int i=0; i<50000; i++) {
           new Thread(new Runnable() {
               @Override
               public void run() {
                    Jedis jedis = jedisPool.getResource();
                    logger.info(message);
                    jedis.publish(channel, message);
                    jedis.close();

               }
           }).start();
       }
    }
}
