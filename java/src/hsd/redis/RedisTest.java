package hsd.redis;

import java.util.Set;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;

public class RedisTest {
	private static final Logger logger = Logger.getLogger(RedisTest.class);
	

	public static void main(String[] args) {
		Jedis jedis = new Jedis("12.99.108.155", 6379);
		
		Set<String> sets =  jedis.keys("*=CFHB");
		
		for(String key : sets) {
			logger.info(key);
		}
	}
}
