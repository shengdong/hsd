package hsd.cyclicBarrier;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

public class CyclicBarrierTest {
	private static final Logger logger = Logger.getLogger(CyclicBarrierTest.class);

	// private static CyclicBarrier cyclicBarrier = new CyclicBarrier(2);
	//
	//
	// public static void main(String[] args) {
	// new Thread(new Runnable() {
	//
	// @Override
	// public void run() {
	// try {
	// cyclicBarrier.await();
	// } catch (InterruptedException | BrokenBarrierException e) {
	// logger.error(e.getMessage(), e);
	// }
	//
	// logger.info(1);
	// }
	// }).start();
	//
	// try {
	// cyclicBarrier.await();
	// } catch (InterruptedException | BrokenBarrierException e) {
	// logger.error(e.getMessage(), e);
	// }
	//
	// logger.info(2);
	// }

	public static void main(String[] args) {
		CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

		ExecutorService service = Executors.newFixedThreadPool(3);

		service.submit(new Thread(new Runner(cyclicBarrier, "1 runner")));
		service.submit(new Thread(new Runner(cyclicBarrier, "2 runner")));
		service.submit(new Thread(new Runner(cyclicBarrier, "3 runner")));
		
		service.shutdown();
	}

}

class Runner implements Runnable {
	private CyclicBarrier barrier;

	private String name;

	
	public Runner(CyclicBarrier barrier, String name) {
		super();
		this.barrier = barrier;
		this.name = name;
	}

	@Override
	public void run() {
		try {
			
			Thread.sleep(1000);
			System.out.println(name + " : is ready......");
			barrier.await();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(name + ": runing!!!");
	}

}
