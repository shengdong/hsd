package hsd.cluster;


import org.apache.log4j.Logger;
import redis.clients.jedis.JedisPubSub;

/**
 * 
 * @author k1193
 * 
 */
public class JedisClutserSub extends JedisPubSub {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JedisClutserSub.class);

	
	@Override
	public void onMessage(String channel, String message) {
		logger.info(channel + "=" + message);
	}
}
