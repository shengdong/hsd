package hsd.cluster;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import com.google.gson.Gson;
import hsd.util.ConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class JedisClutserTest {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JedisClutserTest.class);
	
	/**
	 * declare jedisCluster
	 */
	private JedisCluster jedisCluster;
	
	/**
	 * declare Gson
	 */
	private Gson gson;
	
	
	@Before
	public void setUp() throws Exception {
		Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();

		String ips[] = ConfigUtil.getString("redis.clusterIP").split(";");

		for (String ip : ips) {
			String host = ip.split(":")[0].trim();
			String portString = ip.split(":")[1].trim();
			int port = Integer.parseInt(portString);
			HostAndPort hostAndPort = new HostAndPort(host, port);
			jedisClusterNodes.add(hostAndPort);
		}

		jedisCluster = new JedisCluster(jedisClusterNodes);
		
		logger.info(jedisCluster);
		
		gson = new Gson();
	}
	
	@Test
	public void testKeys() {
		String key = "*";
		JedisClusterUtil clusterUtil = new JedisClusterUtil();
		Set<String> sets = clusterUtil.keys(key, jedisCluster);
		for(String k : sets) {
			logger.info(jedisCluster.get(k));
		}
		
	    try {
	    	clusterUtil.close();
	    	jedisCluster.close();
	    } catch (Exception e) {
	    	logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void ping() {
	}
	
	@Test
	public void testJedisClutserPipline() {
		Set<String> sets = jedisCluster.smembers("LIBOR=");

		JedisClusterUtil jcp = JedisClusterUtil.pipelined(jedisCluster);
		jcp.refreshCluster();
		List<Object> batchResult = null;
		try {
			for (String key : sets) {
				jcp.hgetAll(key);
			}
			
			batchResult = jcp.syncAndReturnAll();
			for(Object object : batchResult) {
				logger.info(object);
			}
			
			jedisCluster.close();
			jcp.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testSetMap() {
		String key = "test";
		Map<String, String> map = new HashMap<String, String>();
		map.put("k1", "v1");
		map.put("k2", "v2");
		map.put("k3", "v3");
		map.put("k4", "v4");
		jedisCluster.hmset(key, map);
		
		try {
			jedisCluster.close();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testGetMap() {
		String key = "test";
		Map<String, String> map = jedisCluster.hgetAll(key);
		logger.info("test : " + map);
		try {
			jedisCluster.close();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testPub() {
		String channel = "HKD";
		Map<String, String> map = jedisCluster.hgetAll("CAD1MFSR=");
		String value = gson.toJson(map);
		jedisCluster.publish(channel, value);
		
		try {
			jedisCluster.close();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testSub() {
		String channel = "HKD";
		JedisClutserSub clutserSub = new JedisClutserSub();
		jedisCluster.subscribe(clutserSub, channel);
	}
	
	@Test
	public void testSet() {
		for(int i=0; i<1000; i++) {
			jedisCluster.set("count" + i, "count" + i);
		}
		
		try {
			jedisCluster.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testSetSet() {
		String key = "SZSE";
		jedisCluster.sadd(key, "hsd");
		jedisCluster.sadd(key, "dong");
		try {
			jedisCluster.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testGetSet() { 
		String key = "SZSE";
		Set<String> sets = jedisCluster.smembers(key);
		for(String k : sets) {
			logger.info(k);
		}
		
		try {
			jedisCluster.close();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
}
