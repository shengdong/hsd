package hsd.thread;

import org.apache.log4j.Logger;

public class NewThread {
	private static final Logger logger = Logger.getLogger(NewThread.class);
	
	public static void main(String[] args) {
		
		for (int i = 0;;i++) {
			logger.info("i = " + i);
			new Thread(new HelloWorldThread()).start();
		}
	}
}
