package hsd.thread;

import org.apache.log4j.Logger;

public class Test {
	private static final Logger logger = Logger.getLogger(Test.class);
	
	
	public static void main(String[] args) {
		while(true) {
			logger.info("hsd");
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
