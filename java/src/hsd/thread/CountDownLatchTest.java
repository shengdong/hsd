package hsd.thread;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

public class CountDownLatchTest {
	private static final Logger logger = Logger.getLogger(CountDownLatchTest.class);
	
	private static CountDownLatch latch = new CountDownLatch(2);

	public static void main(String[] args) throws InterruptedException {

		new Thread(new Runnable() {
			
			@Override
			public void run() {
				logger.info(1);
				latch.countDown();
				
				logger.info(2);
				latch.countDown();
			}
		}).start();
		
		latch.await();
		
		logger.info(3);
	}

}
