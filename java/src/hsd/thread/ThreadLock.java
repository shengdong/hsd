package hsd.thread;

import org.apache.log4j.Logger;

public class ThreadLock extends Thread {
	private static final Logger logger = Logger.getLogger(ThreadLock.class);
	
	private String lock;
	
	private String name;

	
	public ThreadLock(String name, String lock) {
		this.name = name;
		this.lock = lock;
	}
	
	@Override
	public void run() {
		synchronized (lock) {
			for(int i=0; i<3; i++) {
				logger.info(name + " run.........");
			}
		}
	}
	
	public static void main(String[] args) {
		String lock = new String("test");
		for(int i=0; i<5; i++) {
			new ThreadLock("thread_" + i, lock).start();
		}
	}
}
