package hsd.thread;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

public class HelloWorldThread extends Thread {
	private static final Logger logger = Logger.getLogger(HelloWorldThread.class);
	
	
	CountDownLatch latch = new CountDownLatch(1);
	
	public HelloWorldThread() {
		this.setDaemon(true);
	}

	
	@Override
	public void run() {
		try {
			latch.await();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
}
