package hsd.thread;

import org.apache.log4j.Logger;

public class JoinCountDownLatchTest {
	private static final Logger logger = Logger.getLogger(JoinCountDownLatchTest.class);
	

	public static void main(String[] args) throws Exception {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				logger.info("thread finish");
			}
		});
		
		Thread thread2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				logger.info("thread2 finish");
			}
		});
		
		thread.start();
		thread2.start();
		
		thread.join();
		thread2.join();
		
		logger.info("all thread finish");
		
	}
}
