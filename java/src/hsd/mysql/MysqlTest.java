package hsd.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by hsd on 16-12-16.
 */
public class MysqlTest {

    public static final String url = "jdbc:mysql://localhost:3306/dong";
    public static final String name = "com.mysql.jdbc.Driver";
    public static final String user = "root";
    public static final String password = "123456";

    public static void main(String[] args) {
        Connection connection = null;

        PreparedStatement statement = null;

        ResultSet set = null;

        try {
            Class.forName(name);
            connection =  DriverManager.getConnection(url, user, password);
            statement = connection.prepareStatement("select * from user ");

            set = statement.executeQuery();

            while (set.next()) {
                System.out.println("MysqlTest.main : " + set.getObject(1));
                System.out.println("MysqlTest.main : " + set.getObject(2));
                System.out.println("MysqlTest.main : " + set.getObject(3));
                System.out.println("MysqlTest.main : " + set.getObject(4));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if(set != null) {
                    set.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if(statement != null) {
                    statement.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
