package hsd.util;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 读取配置的util
 *
 */
public class ConfigUtil {
	/**
	 * 声明logger对象，用来记录日志
	 */
	public static Logger logger = Logger.getLogger(ConfigUtil.class);
	
	/**
	 * 声明HierarchicalINIConfiguration对象
	 */
	private static HierarchicalINIConfiguration config = null;
	
	/**
	 * declare filePath
	 */
	private static final String FILEPATH = "./control/config.ini";
	
	/**
	 * 读取配置
	 */
	private static void readConfig(){
		
		try {
			config = new HierarchicalINIConfiguration(FILEPATH);
		} catch (ConfigurationException e) {
			logger.error(e.getMessage(),e);
		}
		
	}
	
	/**
	 * 读取String类型的value
	 * @param key 
	 * @return
	 */
	public static String getString(String key){
		if(config == null){
			readConfig();
		}
		return config.getString(key);
	}
	
	/**
	 * 读取int类型的value
	 * @param key
	 * @return
	 */
	public static int getInt(String key){		
		try {
			int reslut = Integer.parseInt(getString(key));
			return reslut;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return 0;
		}
	}
	
	/**
	 * 
	 * @param sections 节点
	 * @param key key值
	 * @return 返回该节点值value
	 */
	public static String getValue(String sections, String key) {
		try {
			config = new HierarchicalINIConfiguration(FILEPATH);
			Set<String> setOfSections = config.getSections();
			Iterator<String> sectionNames = setOfSections.iterator();
			while(sectionNames.hasNext()) {
				String seactionName = sectionNames.next();
				if(StringUtils.equals(sections, seactionName)) {
					return config.getSection(seactionName).getString(key);
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}
	
	/**
	 * 
	 * @param sections 节点值
	 * @return 返回该节点下面全部的key 和 value
	 */
	public static List<String> getListBySections(String sections) {
		List<String> result = new ArrayList<String>();
		HierarchicalINIConfiguration configuration = null;
		try {
			configuration = new HierarchicalINIConfiguration(FILEPATH);
			SubnodeConfiguration subnodeConfiguration = configuration.getSection(sections);
			Iterator<String> inIterator = subnodeConfiguration.getKeys();
			String key = "";
			while(inIterator.hasNext()) {
				key = inIterator.next().trim();
				result.add(key);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	/**
	 * 
	 * @param pattern 日期的格式
	 * @return 当天日期的格式
	 */
	public static String getToday(String pattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(new Date());
	}
	
	/**
	 * 
	 * @param data 数值
	 * @param pattern 格式
	 * @return 对应格式的数值
	 */
	public static String getDouble(double data, String pattern) {
		DecimalFormat format = new DecimalFormat(pattern);
		return format.format(data);
	}
	
	
}
