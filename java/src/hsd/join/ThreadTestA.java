package hsd.join;

import org.apache.log4j.Logger;

public class ThreadTestA implements Runnable {
	private static final Logger logger = Logger.getLogger(ThreadTestA.class);
	
	private int counter = 0;

	
	@Override
	public void run() {
		while(counter < 10) {
			logger.info("counter = " + counter);
			counter ++;
		}
		
		logger.info("");
	}

}
