package hsd.join;

import org.apache.log4j.Logger;
public class ThreadTester {
	private static final Logger logger = Logger.getLogger(ThreadTester.class);
	
	
	public static void main(String[] args) throws InterruptedException {
		Thread thread = new Thread(new ThreadTestA());
		
		Thread thread2 = new Thread(new ThreadTestB());
		
		thread.start();
		thread2.start();
		
		thread.join();
		thread2.join();
		
		logger.info("all Thread finish ");
	}
}
