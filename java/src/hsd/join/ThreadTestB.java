package hsd.join;

import org.apache.log4j.Logger;

public class ThreadTestB implements Runnable {
	private static final Logger logger = Logger.getLogger(ThreadTestB.class);
	
	
	private int i = 0;

	@Override
	public void run() {
		while(i < 10) {
			logger.info("i = " + i);
			i ++;
		}
		
		logger.info("");
	}

}
