package guava;

import com.google.common.base.CaseFormat;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by s.he on 16-12-16.
 * 
 */
public class StringTest {
	private static final Logger logger = Logger.getLogger(StringTest.class);

	@Test
	public void testJoiner() {
		Joiner joiner = Joiner.on("; ").skipNulls();
		String result = joiner.join("Harry", null, "Ron", "Hermione");
		logger.info(result);

		result = Joiner.on(", ").join(Arrays.asList(1, 3, 5));
		logger.info(result);
	}

	@Test
	public void testSplitter() {
		Iterable<String> iterable = Splitter.on(',').trimResults().
				omitEmptyStrings().split("foo,bar,, qux");
		for(String s : iterable) {
			System.out.println(s);
		}
	}
	
	@Test
	public void testString() {
		String hsd = "hsd";
		logger.info(Strings.isNullOrEmpty(hsd));
		
	}
	
	@Test
	public void testCaseFormat() {
		String result = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, "CONSTANT_NAME");
		logger.info(result);
		
		result = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, "CONSTANT_NAME");
		logger.info(result);
		
		result = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, "CONSTANTNAME");
		logger.info(result);
	}
	
	@Test
	public void testCharset() {
		String s = "heshengdong";
		byte[] bs = s.getBytes(Charsets.UTF_8);
		logger.info(bs);
	}
}
