package com.pattern;

public class Thief implements Watcher {

	@Override
	public void update() {
		System.out.println("transform move, thief action");
	}

}
