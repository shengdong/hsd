package com.pattern;

public interface Watcher {

	public void update();
}
