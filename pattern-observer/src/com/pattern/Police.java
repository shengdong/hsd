package com.pattern;

public class Police implements Watcher {

	@Override
	public void update() {
		System.out.println("transform move, police action");
	}

}
