package com.roc.meomory;

public class GameRole {

	private int vit;
	
	private int atk;
	
	public void init() {
		vit = 100;
		atk = 100;
	}
	
	
	public void show() {
		System.out.println(" tili: " + vit);
		System.out.println(" gongjili :" + atk);
	}
	
	public void fightBoss() {
		this.vit = 0;
		this.atk = 0;
	}
	
	public RoleStateMemento saveMemento() {
		return new RoleStateMemento(vit, atk);
	}
	
	public void recove(RoleStateMemento memento) {
		this.vit = memento.getVit();
		this.atk = memento.getAtk();
	}
}
