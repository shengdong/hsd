package com.roc.meomory;

public class Client {

	public static void main(String[] args) {

		GameRole gameRole = new GameRole();
		gameRole.init();
		gameRole.show();
		
		RoleStateMange mange = new RoleStateMange();
		mange.setMemento(gameRole.saveMemento());
		
		gameRole.fightBoss();
		gameRole.show();
		gameRole.recove(mange.getMemento());
		gameRole.show();
		
	}

}
