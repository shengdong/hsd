package com.stsing.provide;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.stsing.data.MysqlDB;

public class FxProvide implements Provide {
	private final static String FILENAME = "CFETS_FX_";

	@Override
	public boolean provideData(String date) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		File file = null;
		OutputStream stream = null;
		BufferedWriter writer = null;

		try {
			String path = PATH + File.separator + date;
			file = new File(path);
			if (!file.exists()) {
				file.mkdirs();
			}
			String filePath = path + File.separator + FILENAME + date + ".csv";
			System.out.println(filePath);
			stream = new FileOutputStream(filePath);
			writer = new BufferedWriter(new OutputStreamWriter(stream));
			String sql = "select  t.data_date, t.data_source, t.f1, t.f2,t.f5, t.f3 from cmds_static_data t where (t.data_type ='SHOUPANJIA' or t.data_type = 'waihuidiaoqiquxian') and t.data_date = ?";
			connection = MysqlDB.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setString(1, date);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				StringBuffer buffer = new StringBuffer();
				buffer.append(resultSet.getString(1) + ",");
				buffer.append(resultSet.getString(2) + ",");
				buffer.append(resultSet.getString(3) + ",");
				buffer.append(resultSet.getString(4) + ",");
				buffer.append(resultSet.getString(5) + ",");
				buffer.append(resultSet.getString(6));

				writer.write(buffer.toString());
				writer.newLine();
			}
			
			System.out.println("save message success");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (stream != null) {
					stream.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (resultSet != null) {
					resultSet.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (statement != null) {
					statement.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (connection != null) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
