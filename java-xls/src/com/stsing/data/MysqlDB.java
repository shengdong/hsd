package com.stsing.data;

import java.sql.Connection;
import java.sql.DriverManager;

public class MysqlDB {
	private static String username = "root";

	private static String password = "123456";

	private static String driverClassName = "com.mysql.jdbc.Driver";

	private static String url = "jdbc:mysql://127.0.0.1:3306/dong?useUnicode=true&characterEncoding=gbk&autoReconnect=true&failOverReadOnly=false&useSSL=true";

	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, username, password);

			return connection;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
