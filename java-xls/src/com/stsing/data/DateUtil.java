package com.stsing.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static String getDay(String day) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = dateFormat.parse(day);
			return dateFormat1.format(date);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}
}
