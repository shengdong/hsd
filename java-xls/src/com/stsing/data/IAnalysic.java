package com.stsing.data;

import java.util.List;

public interface IAnalysic {

	List<List<String>> analysis(String path);
}
