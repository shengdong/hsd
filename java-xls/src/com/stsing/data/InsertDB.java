package com.stsing.data;

import java.util.List;

public interface InsertDB {

	public boolean insertDB(List<List<String>> lists);
}
