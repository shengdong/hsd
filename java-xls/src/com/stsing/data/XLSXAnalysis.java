package com.stsing.data;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class XLSXAnalysis implements IAnalysic {

	@Override
	public List<List<String>> analysis(String path) {
		InputStream stream = null;
		Workbook workbook = null;
		Sheet sheet = null;
		List<List<String>> result = new ArrayList<List<String>>();
		try {
			stream = new FileInputStream(path);
			workbook = WorkbookFactory.create(stream);
			sheet = workbook.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			for(int i=1; i<=rowNum; i++) {
				List<String> lists = new ArrayList<String>();
				 Row row = sheet.getRow(i);
				 for(int j=0; j<row.getLastCellNum(); j++) {
					 Cell cell = row.getCell(j);
					 if(cell != null) {
						 cell.setCellType(Cell.CELL_TYPE_STRING);
						 String value = cell.getStringCellValue();
						 lists.add(value);
					 }
					 
				 }
				 result.add(lists);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			 try {
				 if(stream != null) {
					 stream.close();
				 }
				 
			 } catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

}
