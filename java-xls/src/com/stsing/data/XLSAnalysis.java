package com.stsing.data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

public class XLSAnalysis implements IAnalysic{

	@Override
	public List<List<String>> analysis(String path) {
		WorkbookSettings settings = new WorkbookSettings();
		settings.setLocale(Locale.ENGLISH);
		List<List<String>> result = new ArrayList<List<String>>();

		Workbook workbook = null;
		try {
			workbook = Workbook.getWorkbook(new File(path));
			Cell[] row = null;
			Sheet sheet = workbook.getSheet(0);
			for (int i = 1; i < sheet.getRows(); i++) {
				List<String> lists = new ArrayList<String>();
				row = sheet.getRow(i);

				for (int j = 0; j < row.length; j++) {
					lists.add(row[j].getContents());
				}
				result.add(lists);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook.close();
		}
		return result;
	}
}
