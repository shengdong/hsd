package com.stsing.data;

import java.sql.Connection;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

public class QuXianBoDongLvDB implements InsertDB {

	@Override
	public boolean insertDB(List<List<String>> lists) {
		String insertsql = "insert into cmds_static_data(data_type, data_date, data_source, f1, f2, f3, f5, ric) values(?,?,?,?,?,?,?,?)";

		Connection connection = MysqlDB.getConnection();
		PreparedStatement statement = null;
		try {
			connection.setAutoCommit(false);
			statement = (PreparedStatement) connection.prepareStatement(insertsql);
			for (List<String> list : lists) {
				if(list.size() == 0) {
					continue;
				}
				statement.setString(1, "QIQUANBODONGLV");
				statement.setString(2, DateUtil.getDay(list.get(0).trim()));
				statement.setString(3, "CFETS");
				statement.setString(4, "USD");
				statement.setString(5, "CNY");
				statement.setString(6, list.get(4));
				statement.setString(7, list.get(3));
				statement.setString(8, list.get(2));
				statement.addBatch();
			}

			statement.executeBatch();
			connection.commit();
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
