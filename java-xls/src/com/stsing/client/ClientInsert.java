package com.stsing.client;

import java.io.File;
import java.util.List;

import com.stsing.data.IAnalysic;
import com.stsing.data.InsertDB;
import com.stsing.data.QuXianBoDongLvDB;
import com.stsing.data.XLSAnalysis;

public class ClientInsert {

	public static void main(String[] args) {
		ClientInsert clientInsert = new ClientInsert();
		int count =0;
		while(true) {
			
			clientInsert.run();
			
			try {
				Thread.sleep(5000);
				System.out.println(count++);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void run() {
		IAnalysic analysis = new XLSAnalysis();
		String path = "/home/hsd/文档/cmds/quxian";
		File file = new File(path);
		File [] files = file.listFiles();
		InsertDB mysqlDB = new QuXianBoDongLvDB();
		for(File f : files) {
			List<List<String>> lists = analysis.analysis(f.getAbsolutePath());
			mysqlDB.insertDB(lists);
		}
	}
}
