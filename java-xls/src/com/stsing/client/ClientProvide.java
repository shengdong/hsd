package com.stsing.client;

import com.stsing.provide.FXOption;
import com.stsing.provide.FxProvide;
import com.stsing.provide.Provide;

public class ClientProvide {

	public static void main(String[] args) {
		String dates[] = {"20170206", "20170207", "20170208", "20170209", "20170210"};
		for(String date : dates) {
			provide(date);
		}
	}
	
	private static void provide(String date) {
		Provide provide = new FxProvide();
		provide.provideData(date);
		
		provide = new FXOption();
		provide.provideData(date);
	}
}
