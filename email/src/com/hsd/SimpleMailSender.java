package com.hsd;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by hsd on 16-12-22.
 */
public class SimpleMailSender {

    private final transient Properties properties = System.getProperties();

    private transient MailAuthenticator authenticator;

    private Session session;

    public SimpleMailSender(final String smtpHostName, final String username, final String password) {
        init(smtpHostName, username, password);
    }

    public SimpleMailSender(String username, String password) {
        String smtpHostName = "smtp." + username.split("@")[1];
        init(smtpHostName, username, password);
    }


    private void init(String smtpHostName, String username, String password) {
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.host", smtpHostName);

        authenticator = new MailAuthenticator(username, password);

        session = Session.getInstance(properties, authenticator);
    }

    public void send(String recipient, String subject, Object content) throws Exception {
        final MimeMessage mimeMessage = new MimeMessage(session);
        mimeMessage.setFrom(new InternetAddress(authenticator.getUserName()));
        mimeMessage.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(recipient));
        mimeMessage.setSubject(subject);
        mimeMessage.setContent(content.toString(), "text/html;charset=utf-8");
        Transport.send(mimeMessage);
    }
}
