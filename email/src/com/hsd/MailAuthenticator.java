package com.hsd;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Created by hsd on 16-12-22.
 */
public class MailAuthenticator extends Authenticator {
    private String userName;

    private String password;


    public MailAuthenticator(String userName, String password) {
        this.password = password;
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(userName, password);
    }
}
