package java_desinger.Singleton;

public class Singleton {

	private volatile static Singleton singleton = null;

	
	private Singleton() {

	}

	public static Singleton getIntance() {
		if(singleton == null) {
			synchronized (Singleton.class) {
				if(singleton == null) {
					singleton = new Singleton();
				}
			}
		}
		return singleton;
	}
	
	public static void main(String[] args) {
		Singleton singleton = 	Singleton.getIntance();
		System.out.println(singleton);
	}
}
