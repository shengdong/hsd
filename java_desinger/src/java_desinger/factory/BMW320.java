package java_desinger.factory;

import org.apache.log4j.Logger;

public class BMW320 implements BMW{
	private static final Logger logger = Logger.getLogger(BMW320.class);

	@Override
	public void car() {
		logger.info("create BMW320 Car");
	}

}
