package java_desinger.factory;

import org.apache.log4j.Logger;

public class BMW720 implements BMW {
	private static final Logger logger = Logger.getLogger(BMW720.class);
	
	
	@Override
	public void car() {
		logger.info("create BMW720 car");

	}

}
