package java_desinger.factory;

public class Customer {
	public static void main(String[] args) {
		BMW bmw = null;
		Factory factory = new BMW320Factory();
		bmw = factory.createCar();
		bmw.car();
		
		factory = new BMW520Factory();
		bmw = factory.createCar();
		bmw.car();
		
		factory = new BMW720Factory();
		bmw = factory.createCar();
		bmw.car();
	}

}
