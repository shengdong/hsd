package java_desinger.factory;

public interface Factory {
	
	public BMW createCar();
	
}
