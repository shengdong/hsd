package java_desinger.factory;

public class BMW720Factory implements Factory {

	@Override
	public BMW createCar() {
		return new BMW720();
	}

}
