package java_desinger.factory;

public class BMW520Factory implements Factory {

	@Override
	public BMW createCar() {
		return new BMW520();
	}

}
