package java_desinger.factory;

import org.apache.log4j.Logger;

public class BMW520 implements BMW {
	private static final Logger logger = Logger.getLogger(BMW520.class);

	@Override
	public void car() {
		logger.info("create BMW520 car");
	}

}
