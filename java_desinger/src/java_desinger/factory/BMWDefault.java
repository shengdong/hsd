package java_desinger.factory;

import org.apache.log4j.Logger;

public class BMWDefault implements BMW {
	private static final Logger logger = Logger.getLogger(BMWDefault.class);
	
	@Override
	public void car() {
		logger.info("create defalut car");

	}

}
