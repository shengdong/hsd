package java_desinger.clone;

public class ConcretePrototype2 implements Prototype {

	public Prototype clone() {
		Prototype prototype = new ConcretePrototype2();
		return prototype;
	}
}
