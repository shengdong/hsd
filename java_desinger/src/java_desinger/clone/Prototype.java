package java_desinger.clone;

public interface Prototype {

	public Object clone();
}
