package java_desinger.adater;

public class Test {

	public static void main(String[] args) {
		Target concreteTarget = new ConcreteTarget();  
        concreteTarget.request();  
        
        Target adapter = new Adapter();  
        adapter.request(); 
	}
}
