package java_desinger.Builder;

import org.apache.log4j.Logger;

public class ManBuilder implements PersonBuilder {
	private static final Logger logger = Logger.getLogger(ManBuilder.class);
	
	private Person person = null;
	
	
	public ManBuilder() {
		person = new Man();
	}

	@Override
	public void buildHead() {
		logger.info("create to man head");

	}

	@Override
	public void buildBody() {
		logger.info("create to man body");
	}

	@Override
	public void buildFoot() {
		logger.info("create to man foot");
	}

	@Override
	public Person buildPerson() {
		return person;
	}

}
