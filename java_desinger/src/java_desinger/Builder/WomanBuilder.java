package java_desinger.Builder;

import org.apache.log4j.Logger;

public class WomanBuilder implements PersonBuilder {
	private static final Logger logger = Logger.getLogger(WomanBuilder.class);
	
	private Person person = null;
	

	public WomanBuilder() {
		person = new WoMan();
	}
	
	@Override
	public void buildHead() {
		logger.info("create to build woman head");
	}

	@Override
	public void buildBody() {
		logger.info("create to build woman body");
	}

	@Override
	public void buildFoot() {
		logger.info("create to build woman foot");
	}

	@Override
	public Person buildPerson() {
		return person;
	}

}
