package java_desinger.Builder;

public class PersonDirector {

	public Person constructPerson(PersonBuilder builder) {
		builder.buildBody();
		builder.buildHead();
		builder.buildHead();
		return builder.buildPerson();
	}
}
