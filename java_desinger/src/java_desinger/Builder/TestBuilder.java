package java_desinger.Builder;

public class TestBuilder {

	
	public static void main(String[] args) {
		PersonDirector director = new PersonDirector();
		Person man = director.constructPerson(new ManBuilder());
		Person woman = director.constructPerson(new WomanBuilder());
	}
}
