package com.facade;

public class ClassA {

	public void methodA() {
		System.out.println(" print Class A Method A");
	}
	
	public void methodB() {
		System.out.println(" print Class A Method B");
	}
	
	public void methodC() {
		System.out.println(" print Class A Method C");
	}
}
