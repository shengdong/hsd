package com.facade;

public class ClassC {

	public void methodA() {
		System.out.println(" print Class C Method A");
	}
	
	public void methodB() {
		System.out.println(" print Class C Method B");
	}
	
	public void methodC() {
		System.out.println(" print Class C Method C");
	}
}
