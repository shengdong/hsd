package com.facade;

public class ClassB {

	public void methodA() {
		System.out.println(" print Class B Method A");
	}
	
	public void methodB() {
		System.out.println(" print Class B Method B");
	}
	
	public void methodC() {
		System.out.println(" print Class B Method C");
	}
}
