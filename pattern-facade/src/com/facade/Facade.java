package com.facade;

public class Facade {
	private ClassA classA;
	
	private ClassB classB;
	
	private ClassC classC;
	
	
	public void method() {
		classA = new ClassA();
		classB = new ClassB();
		classC = new ClassC();
		
		classA.methodA();
		classB.methodA();
		classC.methodC();
	}
}
