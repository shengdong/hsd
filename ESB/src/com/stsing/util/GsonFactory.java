package com.stsing.util;

import com.google.gson.Gson;

/**
 * 
 * @author k1193
 *
 */
public class GsonFactory {
	/**
	 * declare gson
	 */
	private static Gson gson = null;
	

	/**
	 * 
	 * @return gson的单例对象
	 */
	public static Gson getGson() {
		if(gson == null) {
             gson = new Gson();
		}
		return gson;
	}
}
