package com.stsing.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.job.ESBQuertz;

/**
 * 
 * @author shengdong.he
 * 
 */
public class DBUtil {
	private static BasicDataSource dataSource;
	
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(DBUtil.class);

	/**
	 * declare URL
	 */
	private static String url = "";

	/**
	 * declare driverClassName
	 */
	private static String driverClassName = "";

	/**
	 * declare userName
	 */
	private static String username = "";

	/**
	 * declare password
	 */
	private static String password = "";

	static {
		dataSource = new BasicDataSource();
		url = ConfigUtil.getString("oracle.url");
		driverClassName = ConfigUtil.getString("oracle.driverClassname");
		username = ConfigUtil.getString("oracle.username");
		password = ConfigUtil.getString("oracle.password");
		url = ConfigUtil.getString("oracle.url");
		driverClassName = ConfigUtil.getString("oracle.driverClassname");
		username = ConfigUtil.getString("oracle.username");
		password = ConfigUtil.getString("oracle.password");
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		dataSource.setMaxActive(ConfigUtil.getInt("oracle.maxActive"));
		dataSource.setDriverClassName(driverClassName);
		dataSource.setInitialSize(ConfigUtil.getInt("oracle.initialSize"));
		dataSource.setMinIdle(ConfigUtil.getInt("oracle.minIdle"));
		dataSource.setMaxWait(1000 * 10);
		dataSource.setRemoveAbandoned(true);
	}

	/**
	 * get connection
	 * 
	 * @return
	 * @throws Exception
	 */
	public Connection getConnection() {
		Connection connection = null;
		try {
//			Class.forName(driverClassName);
//			connection = DriverManager.getConnection(url, username, password);
			connection = dataSource.getConnection();
			return connection;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * close connection
	 * 
	 * @param object connection or resultSet
	 * @throws SQLException sql异常
	 */
	public void close(Object object) throws SQLException {
		if (object instanceof Connection) {
			((Connection) object).close();
		} else if (object instanceof PreparedStatement) {
			((PreparedStatement) object).close();
		} else if (object instanceof ResultSet) {
			((ResultSet) object).close();
		}
	}

	/**
	 * 
	 * @param sale 价格
	 *            
	 * @param tranCode  交易码
	 *           
	 * @return 折扣信息
	 */
	// public double getDiscount(double sale,String tranCode) {
	// Connection connection = getConnection();
	// PreparedStatement statement = null;
	// ResultSet set = null;
	// String discount = "";
	// String sql =
	// "SELECT DISCOUNT FROM ESB_DISCOUNT WHERE TRAN_CODE='"+tranCode+"' AND (('"+sale+"'>PRICE_HEAD"
	// +
	// " AND '"+sale+"'<=PRICE_TAIL) OR ('"+sale+"'>PRICE_HEAD AND PRICE_TAIL IS NULL))";
	//
	// try {
	// logger.info("sql:" + sql);
	// statement = connection.prepareStatement(sql);
	// set = statement.executeQuery();
	// while(set.next()) {
	// discount = set.getString("discount");
	// }
	//
	// } catch (Exception e) {
	// logger.error(e.getMessage(), e);
	// return 0;
	// } finally {
	// if(set != null) {
	// try {
	// close(set);
	// } catch (SQLException e) {
	// logger.error(e.getMessage(), e);
	// }
	// }
	// if(statement != null) {
	// try {
	// close(statement);
	// } catch (SQLException e) {
	// logger.error(e.getMessage(), e);
	// }
	// }
	// if(connection != null) {
	// try {
	// close(connection);
	// } catch (SQLException e) {
	// logger.error(e.getMessage(), e);
	// }
	// }
	// }
	//
	// return Double.parseDouble(discount);
	// }

	/**
	 * 
	 * @param sale 价格
	 *            
	 * @param tranCode  交易码
	 *           
	 * @param ccy 币种
	 *            
	 * @param saleType 买还是卖
	 *            
	 * @return 返回map对象，1表示百分比折扣，折扣信息，2表示掉期点，掉期点数
	 */
	public Map<String, Object> getDiscount(double sale, String ccy,
			String saleType) {
		String sql = "SELECT DISCOUNT, DISCOUNT_TYPE FROM ESB_DISCOUNT WHERE (('"
				+ sale
				+ "'>PRICE_HEAD"
				+ " AND '"
				+ sale
				+ "'<=PRICE_TAIL) OR ('"
				+ sale
				+ "'>PRICE_HEAD AND PRICE_TAIL IS NULL)) AND ccy ='"
				+ ccy
				+ "' and sale_type ='" + saleType + "'";

		List<Map<String, Object>> lists = getDataFromDB(sql);
		return lists.get(0);

	}

	/**
	 * 
	 * @param endDate 到期日
	 * @return 期限
	 */
	public List<String> getQiXianBy(String endDate, String ccys) {
		List<String> lists = new ArrayList<String>(2);
		Connection connection = getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String todayString = format.format(new Date());
		List<String> list = new ArrayList<String>(2 << 5);
		try {
			String sql = "select * from valuedate t where t.tradedate = '"
					+ todayString + "' and ccys = '" + ccys + "'";
			logger.info("sql : " + sql);
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				list.add("VALUEDATE_ON," + resultSet.getString("VALUEDATE_ON"));
				list.add("VALUEDATE_TN," + resultSet.getString("VALUEDATE_TN"));
				list.add("VALUEDATE_SPOT,"
						+ resultSet.getString("VALUEDATE_SPOT"));
				// list.add("VALUEDATE_SN," +
				// resultSet.getString("VALUEDATE_SN"));
				list.add("VALUEDATE_SW," + resultSet.getString("VALUEDATE_SW"));
				list.add("VALUEDATE_2W," + resultSet.getString("VALUEDATE_2W"));
				list.add("VALUEDATE_3W," + resultSet.getString("VALUEDATE_3W"));
				list.add("VALUEDATE_1M," + resultSet.getString("VALUEDATE_1M"));
				list.add("VALUEDATE_2M," + resultSet.getString("VALUEDATE_2M"));
				list.add("VALUEDATE_3M," + resultSet.getString("VALUEDATE_3M"));
				list.add("VALUEDATE_4M," + resultSet.getString("VALUEDATE_4M"));
				list.add("VALUEDATE_5M," + resultSet.getString("VALUEDATE_5M"));
				list.add("VALUEDATE_6M," + resultSet.getString("VALUEDATE_6M"));
				list.add("VALUEDATE_9M," + resultSet.getString("VALUEDATE_9M"));
				list.add("VALUEDATE_1Y," + resultSet.getString("VALUEDATE_1Y"));
				list.add("VALUEDATE_18M,"
						+ resultSet.getString("VALUEDATE_18M"));
				list.add("VALUEDATE_2Y," + resultSet.getString("VALUEDATE_2Y"));
				list.add("VALUEDATE_3Y," + resultSet.getString("VALUEDATE_3Y"));
			}

			Long endDateLong = format.parse(endDate).getTime();
			for (int i = 0; i < list.size() - 1; i++) {
				String dateStart = list.get(i).split(",")[1];
				String qixianStart = list.get(i).split(",")[0];
				String dateEnd = list.get(i + 1).split(",")[1];
				String qixianEnd = list.get(i + 1).split(",")[0];

				Long startDateLong = format.parse(dateStart).getTime();
				Long endDateLongString = format.parse(dateEnd).getTime();
				if (StringUtils.equals(endDate, dateStart)) {
					lists.add(qixianStart.split("_")[1]);
					return lists;
				} else if (StringUtils.equals(endDate, dateEnd)) {
					lists.add(qixianEnd.split("_")[1]);
					return lists;
				} else if (betweenDate(startDateLong, endDateLongString,
						endDateLong)) {
					lists.add(qixianStart.split("_")[1]);
					lists.add(qixianEnd.split("_")[1]);
					return lists;
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);

		} finally {
			if (resultSet != null) {
				try {
					close(resultSet);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			if (statement != null) {
				try {
					close(statement);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			if (connection != null) {
				try {
					close(connection);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return lists;
	}

	/**
	 * 
	 * @param date1 起息日
	 *            
	 * @param date2  到期日
	 *           
	 * @param ccys 币种
	 *            
	 * @return 返回期限
	 */
	public List<String> getDate(String date1, String date2, String ccys) {
		List<String> lists = new ArrayList<String>(2);
		Connection connection = getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String todayString = format.format(new Date());
		String sql = "select * from valuedate where tradedate='" + todayString
				+ "' and CCYS='" + ccys + "'";
		try {
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				lists.add(resultSet.getString("VALUEDATE_" + date1));
				lists.add(resultSet.getString("VALUEDATE_" + date2));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		return lists;
	}

	/**
	 * 判断是否在日期之间
	 * 
	 * @param dateStart 开始时间
	 * @param dataEnd 结束时间
	 * @param date 当前时间
	 * @return 返回true or false
	 */
	private boolean betweenDate(long dateStart, long dataEnd, long date) {
		if (date > dateStart && date < dataEnd) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param sql
	 *            执行的sql
	 * @return 返回sql的结果集
	 */
	public List<Map<String, Object>> getDataFromDB(String sql) {
		List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
		Connection connection = getConnection();
		while (connection == null) {
			try {
				Thread.sleep(200);
				Class.forName(driverClassName);
				connection = DriverManager.getConnection(url, username, password);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		PreparedStatement statement = null;
		ResultSet set = null;
		try {
			statement = connection.prepareStatement(sql);
			set = statement.executeQuery();
			while (set.next()) {
				Map<String, Object> map = new HashMap<String, Object>(20);
				ResultSetMetaData data = set.getMetaData();
				int count = data.getColumnCount();
				for (int i = 1; i <= count; i++) {
					String key = data.getColumnName(i);
					Object value = set.getObject(key);
					map.put(key, value);
				}
				lists.add(map);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (set != null) {
				try {
					set.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}

			if (statement != null) {
				try {
					statement.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return lists;
	}

	/**
	 * 
	 * @return quertz的值
	 */
	public List<ESBQuertz> getQuertz() {
		List<ESBQuertz> result = new ArrayList<ESBQuertz>();
		String sql = "select * from esb_quertz t";
		List<Map<String, Object>> lists = getDataFromDB(sql);
		for (Map<String, Object> map : lists) {
			ESBQuertz quertz = new ESBQuertz();
			quertz.setClassName((String) map.get("CLASSNAME"));
			quertz.setJobName((String) map.get("JOBNAME"));
			quertz.setTriggerName((String) map.get("TRIGGERNAME"));
			quertz.setTriggerTime((String) map.get("TRIGGERTIME"));
			result.add(quertz);
		}
		return result;
	}
}
