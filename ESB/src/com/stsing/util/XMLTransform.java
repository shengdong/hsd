package com.stsing.util;



import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.stsing.handler.RedisHandler;
import com.stsing.symbol.ConsumerSeqNoUtil;

/**
 * 
 * @author k1193
 *
 */
public class XMLTransform {
	/**
	 * declare logger
	 */
    private static Logger logger = Logger.getLogger(XMLTransform.class);

    /**
     * 
     * @param resultTempPath 收到文件的路径
     * @return 返回报文
     */
	public static String resultTempsql(String resultTempPath){
        Document doc = null;
            try {
				doc = new SAXReader().read(new File(resultTempPath));
			
				Element body = doc.getRootElement().element("BODY");
           
				String expDt = body.element("ExpDt").getText();
				String ccy = body.element("Ccy").getText();
				String ccy1 = body.element("Ccy1").getText();
				String txnAmt = body.element("TxnAmt").getText();
				String txnTp = body.element("TxnTp").getText();
				Element localHead = doc.getRootElement().element("LOCAL_HEAD");
				String tranCode = localHead.element("TranCode").getText();
				return checkXML(expDt, ccy, ccy1, txnAmt, txnTp,tranCode);
            } catch (DocumentException e) {
            	logger.error(e.getMessage());
            	String returnCode = "2222";
    			String returnMsg = "系统异常";
    			return getRep(returnCode,returnMsg,"","");
			} 
    }
	
	/**
	 * 
	 * @param expDt 到期日
	 * @param ccy 主币种
	 * @param ccy1 次币种
	 * @param txnAmt 期限
	 * @param txnTp 日期
	 * @param tranCode 编码
	 * @return 返回的报文
	 */
	private static String checkXML(String expDt,String ccy,String ccy1,String txnAmt,String txnTp,String tranCode){	
		String returnCode = "";
		String returnMsg = "";
		String spotFrxRate = "";
		String swapPnt = "";
		try{
			 logger.info("expDt : " + expDt + ", ccy: " + ccy + ", ccy1:" + ccy1 + ", txnAmt:" + txnAmt + ", tranCode:" + tranCode);
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	
			 
			if(Double.parseDouble(txnAmt)<=0){
				 returnCode = "0001";
				 returnMsg = "交易金额必须为正数";
			}else if(sdf.parse(expDt).getTime() <= new Date().getTime()){
				returnCode = "0002";
				returnMsg = "到期日必须大于当前日期";
			}else if((!"ask".equalsIgnoreCase(txnTp)) && (!"bid".equalsIgnoreCase(txnTp))){
				returnCode = "0003";
				returnMsg = "交易类型有误";
			}
        
			if("".equals(returnMsg)){
				RedisHandler handler = new RedisHandler();
				Map<String, String> result = handler.getSpot(expDt, ccy, ccy1, txnAmt, txnTp);
				if(result.size() == 2){
					returnCode = "0000";
					spotFrxRate = result.get("spot");
					swapPnt = result.get("changeOver");
				}else{
					returnCode = "1111";
					returnMsg = result.get("error");
				}
			}
			return getRep(returnCode,returnMsg,spotFrxRate,swapPnt);
		} catch (ParseException e) {
			logger.error(e.getMessage());
			returnCode = "2222";
			returnMsg = "到期日格式不正确";
			return getRep(returnCode,returnMsg,spotFrxRate,swapPnt);
		}
	}
	
	/**
	 * 
	 * @param returnCode 返回码
	 * @param returnMsg 返回的信息
	 * @param spotFrxRate SPOT的值
	 * @param swapPnt 掉期点
	 * @return 返回消息
	 */
	private static String getRep(String returnCode,String returnMsg,String spotFrxRate,String swapPnt){
		String tranTime = ConfigUtil.getToday("HH:mm:ss");
		String tranDate = ConfigUtil.getToday("yyyy-MM-dd");
		String consumerSeqNo = ConsumerSeqNoUtil.getConsumerSeqNo();
		String ConsumerId = consumerSeqNo.substring(consumerSeqNo.length()-8, consumerSeqNo.length());
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><service><SYS_HEAD>" +
				"<array><Ret><ReturnCode>"+returnCode+"</ReturnCode><ReturnMsg>"+returnMsg+"</ReturnMsg></Ret></array>" +
       			"<ServiceCode>12002000002</ServiceCode><ServiceScene>01</ServiceScene>" +
       			"<TranTime>" +tranTime+"</TranTime><TranDate>"+tranDate+"</TranDate>" +
       			"<ConsumerSeqNo>"+consumerSeqNo+"</ConsumerSeqNo><ConsumerId>" +ConsumerId+"</ConsumerId></SYS_HEAD>" +
       			"<APP_HEAD><TranTellerNo>100085</TranTellerNo><TranBranchId>8801</TranBranchId></APP_HEAD>" +
       			"<BODY><SpotFrxRate>"+spotFrxRate+"</SpotFrxRate><SwapPnt>"+swapPnt+"</SwapPnt></BODY>" +
       			"</service>";
	}
}
