package com.stsing.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import org.apache.log4j.Logger;

/**
 * 
 * @author k1193
 *
 */
public class InfoUtil {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(InfoUtil.class);
	
	/**
	 * declare dbutil
	 */
	private static DBUtil dbUtil = null;
	
	/**
	 * 初始化dbutil的对象
	 */
	static {
		dbUtil = new DBUtil();
	}
	
	/**
	 * 
	 * @param info info的日志信息
	 */
	public static void insertIntoInfo(Info info) {
		logger.info("info: " + info);
		Connection connection = dbUtil.getConnection();
		String sql = ConfigUtil.getString("info.insertSql");
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, info.isSuccess() + "");
			statement.setString(2, info.getErrorInfo());
			statement.setString(3, info.getErrorMsg());
			statement.setString(4, (String)info.getObject());
			statement.execute();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
			try {
				if(connection != null) {
					connection.close();
				}
			}catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
