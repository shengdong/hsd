package com.stsing.util;

/**
 * 
 * @author k1193
 *
 */
public class Info {
	/**
	 * result is true or false
	 */
	private boolean success;
	
	/**
	 * error message 
	 */
	private String errorMsg;
	
	/**
	 * message from exception
	 */
	private String errorInfo;
	
	/**
	 * return object
	 */
	private Object object;

	
	/**
	 * @return get success is true or false
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * set success
	 * @param success �Ƿ�ɹ�����Ϣ
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return get error message
	 */
	public String getErrorMsg() {
		return errorMsg;
	}


	/**
	 * set error message
	 * @param errorMsg ������Ϣ
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}


	/**
	 * @return get error info
	 */
	public String getErrorInfo() {
		return errorInfo;
	}

	/**
	 * set error info
	 * @param errorInfo �쳣��Ϣ
	 */
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}


	/**
	 * @return get object
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * set object
	 * @param object �Զ��������
	 */
	public void setObject(Object object) {
		this.object = object;
	}
	
	
}
