package com.stsing.test;

import com.stsing.client.SocketClient;
import org.apache.log4j.Logger;

/**
 * Created by hsd on 17-2-28.
 */
public class TestSocket {
    private static final Logger logger = Logger.getLogger(TestSocket.class);

    public static void main(String[] args) {
        final String s = "hello";

        logger.info("hello........start");
        for(int i=0; i<1; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
//                    SocketClient.sendToESB("12.17.27.164", 8989, 6000, s);
                    SocketClient.sendToESB("127.0.0.1", 8989, 6000, s);
                }
            }).start();
        }
        logger.info("hello........end");
    }
}
