package com.stsing.fixclient;

/**
 * 
 * @author k1193
 *
 */
public class PriceObject {
	/**
	 * ric
	 */
	public String ric;
	
	/**
	 * declare symbol
	 */
	public String symbol;//QuotePair
	
	/**
	 * declare tenor
	 */
	public String tenor;//QuoteTenor
	
	/**
	 * declare bid
	 */
	public String bid;//MDEntryPx
	
	/**
	 * declare ask
	 */
	public String ask;//MDEntryPx
	
	/**
	 * declare side
	 */
	public String side;//MDEntryType
	
	/**
	 * declare valid
	 */
	public String vaild;
	
	/**
	 * declare source
	 */
	public String source;//QuoteSource
	
	/**
	 * declare provider_ask
	 */
	public String provider_ask;
	
	/**
	 * declare provider_bid
	 */
	public String provider_bid;
	
	/**
	 * declare biddate
	 */
	public String biddate;//MDEntryDate
	
	/**
	 * declare bidtime
	 */
	public String bidtime;//MDEntryTime
	
	/**
	 * declare askdate 
	 */
	public String askdate;//MDEntryDate
	
	/**
	 * declare ashtime
	 */
	public String asktime;//MDEntryTime
	
	/**
	 * declare securityDesc
	 */
	public String securityDesc;//MarketIndicator
	
	/**
	 * declare askCash
	 */
	public String askCash;
	
	/**
	 * declare bidCash
	 */
	public String bidCash;

    /**
     * declare askClient
     */
    public String askClient;

    /**
     * declare
     */
	public String bidClient;
	
	/**
	 * 
	 * @param ric ric码
 	 */
	public void setRic(String ric) {
		this.ric = ric;
	}
	
	/**
	 * 
	 * @return 返回ric码
	 */
	public String getRic() {
		return ric;
	}
	
	/**
	 * 
	 * @param symbol 货币对
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * 
	 * @return 返回货币对
	 */
	public String getSymbol() {
		return symbol;
	}
	
	/**
	 * 
	 * @param tenor 期限
	 */ 
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	
	/**
	 * 
	 * @return 返回期限
	 */
	public String getTenor() {
		return tenor;
	}
	
	/**
	 * 
	 * @param bid bid值
	 */
	public void setBid(String bid) {
		this.bid = bid;
	}
	
	/**
	 * 
	 * @return 返回bid值
	 */
	public String getBid() {
		return bid;
	}
	
	/**
	 * 
	 * @param ask ask的数值
	 */
	public void setAsk(String ask) {
		this.ask = ask;
	}
	
	/**
	 * 返回ask的数值
	 * @return ask
	 */
	public String getAsk() {
		return ask;
	}
	
	/**
	 * 
	 * @param side side值
	 */
	public void setSide(String side) {
		this.side = side;
	}
	
	/**
	 * 
	 * @return 返回side的值
	 */
	public String getSide() {
		return side;
	}
	
	/**
	 * 
	 * @param vaild valid数值
	 */
	public void setVaild(String vaild) {
		this.vaild = vaild;
	}
	
	/**
	 * 
	 * @return 返回valid的值
	 */
	public String getVaild() {
		return vaild;
	}
	
	/**
	 * 
	 * @param source source的值
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
	/**
	 * 
	 * @return 返回source数值
	 */
	public String getSource() {
		return source;
	}
	
	/**
	 * 
	 * @param provider_ask  provider_ask的值
	 */
	public void setProvider_ask(String provider_ask) {
		this.provider_ask = provider_ask;
	}
	
	/**
	 * 
	 * @return 返回provider_ask的值
	 */
	public String getProvider_ask() {
		return provider_ask;
	}
	
	/**
	 * 
	 * @param provider_bid provider_bid的值
	 */
	public void setProvider_bid(String provider_bid) {
		this.provider_bid = provider_bid;
	}
	
	/**
	 * 
	 * @return 返回provider_bid的值
	 */
	public String getProvider_bid() {
		return provider_bid;
	}
	
	/**
	 * 
	 * @param securityDesc securityDesc的值
	 */
	public void setSecurityDesc(String securityDesc) {
		this.securityDesc = securityDesc;
	}
	
	/**
	 * 
	 * @return 返回securityDesc的值
	 */
	public String getSecurityDesc() {
		return securityDesc;
	}
	
	/**
	 * 
	 * @param askdate ask的日期
	 */
	public void setAskdate(String askdate) {
		this.askdate = askdate;
	}
	
	/**
	 * 
	 * @return 返回askdate的日期
	 */
	public String getAskdate() {
		return askdate;
	}
	
	/**
	 * 
	 * @param asktime ask的时间
	 */
	public void setAsktime(String asktime) {
		this.asktime = asktime;
	}
	
	/**
	 * 
	 * @return 返回ask的时间
	 */
	public String getAsktime() {
		return asktime;
	}
	
	/**
	 * 
	 * @param biddate bid日期
	 */
	public void setBiddate(String biddate) {
		this.biddate = biddate;
	}
	
	/**
	 * 
	 * @return 返回bid的日期
	 */
	public String getBiddate() {
		return biddate;
	}
	
	/**
	 * 
	 * @param bidtime bid时间
	 */
	public void setBidtime(String bidtime) {
		this.bidtime = bidtime;
	}
	
	/**
	 * 
	 * @return 返回bid的时间
	 */
	public String getBidtime() {
		return bidtime;
	}
	
	
	/**
	 * 
	 * @return askCash
	 */
	public String getAskCash() {
		return askCash;
	}

	/**
	 * 
	 * @param askCash set askCash value
	 */
	public void setAskCash(String askCash) {
		this.askCash = askCash;
	}

	/**
	 * 
	 * @return get bidCash
	 */
	public String getBidCash() {
		return bidCash;
	}

	/**
	 * 
	 * @param bidCash set bidCash
	 */
	public void setBidCash(String bidCash) {
		this.bidCash = bidCash;
	}

    /**
     *
     * @return 对客ask价格
     */
    public String getAskClient() {
        return askClient;
    }

    /**
     *
     * @param askClient 对客价格
     */
    public void setAskClient(String askClient) {
        this.askClient = askClient;
    }

    /**
     *
     * @return 返回对客bid价格
     */
    public String getBidClient() {
        return bidClient;
    }

    /**
     *
     * @param bidClient 对客bid价格
     */
    public void setBidClient(String bidClient) {
        this.bidClient = bidClient;
    }

  /**
   * @return toString
   */
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("ric").append("=").append(ric).append(",");
		sb.append("symbol").append("=").append(symbol).append(",");
		sb.append("tenor").append("=").append(tenor).append(",");
		sb.append("bid").append("=").append(bid).append(",");
		sb.append("ask").append("=").append(ask).append(",");
		sb.append("side").append("=").append(side).append(",");
		sb.append("vaild").append("=").append(vaild).append(",");
		sb.append("source").append("=").append(source).append(",");
		sb.append("provider_ask").append("=").append(provider_ask).append(",");
		sb.append("provider_bid").append("=").append(provider_bid).append(",");
		sb.append("biddate").append("=").append(getBiddate()).append(",");
		sb.append("bidtime").append("=").append(getBidtime()).append(",");
		sb.append("askdate").append("=").append(getAskdate()).append(",");
		sb.append("asktime").append("=").append(getAsktime()).append(",");
		sb.append("bidCash").append("=").append(getBidCash()).append(",");
		sb.append("askCash").append("=").append(getAskCash()).append(",");
        sb.append("bidCash").append("=").append(getBidClient()).append(",");
        sb.append("askCash").append("=").append(getAskClient()).append(",");
		sb.append("securityDesc").append("=").append(securityDesc).append(",");
		return sb.toString();
	}
}
