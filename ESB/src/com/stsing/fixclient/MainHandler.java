package com.stsing.fixclient;
import org.apache.log4j.Logger;

import com.stsing.symbol.SymbolHandler;

import quickfix.Connector;
import quickfix.FileStoreFactory;
import quickfix.SLF4JLogFactory;
import quickfix.Session;
import quickfix.SessionSettings;
import quickfix.SocketInitiator;
import quickfix.field.MDReqID;
import quickfix.field.SenderCompID;
import quickfix.field.SubscriptionRequestType;
import quickfix.field.Symbol;
import quickfix.field.TargetCompID;
import quickfix.fix44.MarketDataRequest;
import quickfix.fix44.MessageFactory;

/**
 * fix 主程序
 * @author k1193
 *
 */
public class MainHandler {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(MainHandler.class);
	
	
	/**
	 * 定义一个mainHandler 对象
	 */
	private static MainHandler instance = null;
	
	/**
	 * 初始化一个 同步锁对象
	 */
	private static Object lockObj = new Object();
	
	/**
	 * 定义一个 application 对象
	 */
	private FixApplication application;
	
	/**
	 * 定义一个handler 对象
	 */
	private MsgHandler handler;
	
	/**
	 * 定义一个接受的存放字符串数组
	 */
	private String[] rics;
	
	/**
	 * 定义一个名称
	 */
	private String sendcom;
	
	/**
	 * 定义一个USD线程
	 */
	private SymbolHandler usdcny;
	
	/**
	 * 定义一个EUR线程
	 */
	private SymbolHandler eurcny;
	
	/**
	 * 定义一个JPY线程
	 */
	private SymbolHandler jpycny;
	
	/**
	 * 定义一个HKD线程
	 */
	private SymbolHandler hkdcny;
	
	/**
	 * 定义一个GBP线程
	 */
	private SymbolHandler gbpcny;
	
	/**
	 * 定义一个CHF线程
	 */
	private SymbolHandler chfcny;
	
	/**
	 * 定义一个CAD线程
	 */
	private SymbolHandler cadcny;
	
	/**
	 * 定义一个AUD线程
	 */
	private SymbolHandler audcny;
	
	/**
	 * 定义一个SGD线程
	 */
	private SymbolHandler sgdcny;
	
	/**
	 * 定义一个启动器
	 */
	private Connector connector;
	
	/**
	 * 
	 * @return 单例的MainHandler对象
	 */
	public static MainHandler Instance(){
		if(instance == null){
			synchronized (lockObj) {
				instance = new MainHandler();
			}
		}
		return instance;
	}
	
	/**
	 * 空的构造函数
	 */
	private MainHandler(){
		
	}
	
	/**
	 * 
	 * @param configPath 配置文件的路径
	 * @param rics 接受的ric
	 * @param sendcom sendcom的名称
	 */
	public void init(String configPath,String[] rics, String sendcom){
		try {
			this.rics = rics;
			this.sendcom=sendcom;
			application = new FixApplication();
			handler = new MsgHandler();
			handler.start();
			
			usdcny = new SymbolHandler();
			usdcny.start();
			
			eurcny = new SymbolHandler();
			eurcny.start();
			
			jpycny = new SymbolHandler();
			jpycny.start();
			
			hkdcny = new SymbolHandler();
			hkdcny.start();
			
			gbpcny = new SymbolHandler();
			gbpcny.start();
			
			chfcny = new SymbolHandler();
			chfcny.start();
			
			cadcny = new SymbolHandler();
			cadcny.start();
			
			audcny = new SymbolHandler();
			audcny.start();
			
			sgdcny = new SymbolHandler();
			sgdcny.start();
			
			SessionSettings settings = new SessionSettings(configPath);
			connector = new SocketInitiator(application, 
					new FileStoreFactory(settings), settings, new SLF4JLogFactory(settings), new MessageFactory());
			connector.start();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
		}
	}
	
	/**
	 * 停止的程序
	 */
	public void stop() {
		if(connector != null) {
			connector.stop();
		}
	}
	
	/**
	 * 初始化session
	 */
	public void request(){
		try {
			MarketDataRequest request = getRequest(rics, SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES);
			Session.sendToTarget(request);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * @return 返回Msghandler 对象
	 */
	public MsgHandler getMsgHandler(){
		return handler;
	}
	
	/**
	 * 
	 * @param rics 要接受的rics 
	 * @param type 类型
	 * @return 返回request 对象
	 */
	public MarketDataRequest getRequest(String[] rics ,char type){
		MarketDataRequest request = new MarketDataRequest();
		request.getHeader().setString(SenderCompID.FIELD, sendcom);
		request.getHeader().setString(TargetCompID.FIELD, "NBCB-FIX");
		MarketDataRequest.NoRelatedSym group = new MarketDataRequest.NoRelatedSym();
		request.set(new MDReqID("11"));
		request.set(new SubscriptionRequestType(type));
		for(int i = 0; i < rics.length; i++){
			group.set(new Symbol(rics[i]));
			request.addGroup(group);
		}
		return request;
	}

	/**
	 * @return 返回USD线程的对象
	 */
	public SymbolHandler getUsdcny() {
		return usdcny;
	}

	/**
	 * 
	 * @return 返回EUR线程的对象
	 */
	public SymbolHandler getEurcny() {
		return eurcny;
	}

	/**
	 * 
	 * @return JPY线程的对象
	 */
	public SymbolHandler getJpycny() {
		return jpycny;
	}

	/**
	 * 
	 * @return HKD线程的对象
	 */
	public SymbolHandler getHkdcny() {
		return hkdcny;
	}

	/**
	 * 
	 * @return 返回GBP线程的对象
	 */
	public SymbolHandler getGbpcny() {
		return gbpcny;
	}

	/**
	 * 
	 * @return 返回CHF线程的对象
	 */
	public SymbolHandler getChfcny() {
		return chfcny;
	}

	/**
	 * 
	 * @return 返回CAD线程的对象
	 */
	public SymbolHandler getCadcny() {
		return cadcny;
	}

	/**
	 * 
	 * @return 返回AUD线程的对象
	 */
	public SymbolHandler getAudcny() {
		return audcny;
	}

	/**
	 * 
	 * @return 返回SGD线程的对象
	 */
	public SymbolHandler getSgdcny() {
		return sgdcny;
	}
	
	
}
