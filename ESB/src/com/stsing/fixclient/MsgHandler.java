package com.stsing.fixclient;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.log4j.Logger;

import quickfix.DataDictionary;
import quickfix.Group;
import quickfix.fix44.MarketDataSnapshotFullRefresh;

import com.stsing.symbol.SymbolDispatcher;

/**
 *  MSGHandler 处理接收到消息的对象
 * @author k1193
 *
 */
public class MsgHandler extends Thread{
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(MsgHandler.class);
	
	/**
	 * 初始化 同步锁对象
	 */
	private Object lock = new Object();
	
	/**
	 * 定义一个阻塞的消息队列
	 */
	private LinkedBlockingDeque<MarketDataSnapshotFullRefresh> queue;
	
	/**
	 * 定义一个dataDic
	 */
	@SuppressWarnings("unused")
	private DataDictionary dataDic;
	
	/**
	 * 构造函数，初始化消息队列
	 */
	public MsgHandler(){
		queue = new LinkedBlockingDeque<MarketDataSnapshotFullRefresh>();
		try {
			dataDic = new DataDictionary("../control/FIX44.xml");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * @param msg 消息信息
	 */
	public void addMsg(MarketDataSnapshotFullRefresh msg){
		queue.add(msg);
	}
	
	@Override
	public void run(){
		while(!isInterrupted()){
			if(queue.isEmpty()){
				synchronized (lock) {
					try {
						lock.wait(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}else{
				MarketDataSnapshotFullRefresh msg = queue.poll();
				if(msg != null)
					process(msg);
			}
		}
	}
	
	/**
	 * 
	 * @param msg 接受到消息
	 */
	public void process(MarketDataSnapshotFullRefresh msg){
		PriceObject obj = new PriceObject();
		try {
//			if(msg.isSetField(10253)){
//                msg.getString(10253);
//			}
			if(msg.isSetField(48)){
				obj.ric=msg.getString(48);
			}
			if(msg.isSetField(55)){
				obj.symbol=msg.getString(55);
			}
			if(msg.isSetField(10004)){
				obj.tenor=msg.getString(10004);
			}
			if(msg.isSetField(10005)){
				obj.source=msg.getString(10005);
			}
			if(msg.isSetField(10002)){
				obj.securityDesc=msg.getString(10002);
			}

			if(msg.isSetField(268)){
				int nGroup = msg.getInt(268);

				for(int i =1 ; i <= nGroup; i++){
					Group group = msg.getGroup(i, 268);
					String direct ="";
					if(group.isSetField(269)){
						direct = group.getString(269);
					}
					if("0".equals(direct)){
						if(group.isSetField(270)){
							obj.bid=group.getString(270);
						}
						if(group.isSetField(272)){
							obj.biddate=group.getString(272);
						}
						if(group.isSetField(273)){
							obj.bidtime=group.getString(273);
						}
                        if(group.isSetField(10236)) {
                            obj.bidClient = group.getString(10236);
                        }
                        if(group.isSetField(10237)) {
                            obj.bidCash = group.getString(10237);
                        }

					}else if("1".equals(direct)) {
						if(group.isSetField(270)){
							obj.ask=group.getString(270);
						}
						if(group.isSetField(272)){
							obj.askdate=group.getString(272);
						}
						if(group.isSetField(273)){
							obj.asktime=group.getString(273);
						}

                        if(group.isSetField(10236)) {
                            obj.askClient = group.getString(10236);
                        }
                        if(group.isSetField(10237)) {
                            obj.askCash = group.getString(10237);
                        }
					}

				}
			}

			SymbolDispatcher.getInstance().processPriceObject(obj);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
