package com.stsing.fixclient;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.stsing.util.ConfigUtil;

/**
 * fix client
 * @author k1193
 *
 */
public class FixMain {
    /**
     * declare logger
     */
    private static final Logger logger = Logger.getLogger(FixMain.class);

	/**
	 * 定义 主程序
	 */
	private MainHandler handler = null;
	
	
	/**
	 * 开始启动程序
	 */
	public void run() {
		try {
//			PropertyConfigurator.configure("../control/FixClientLog1_test.ini");
//			MainHandler.Instance().init("../control/FixClient1_test.ini",new String[]{"EURCNYSW=BANK"},"CLIENT1");
			handler = MainHandler.Instance();
			PropertyConfigurator.configure("../control/FixClientLog1.ini");
			String rics[] = ConfigUtil.getString("esb.ric").split(",");
            String client = ConfigUtil.getString("fix.client");
			handler.init("../control/FixClient1.ini",rics, client);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 停止程序
	 */
	public void stop() {
		if(handler != null) {
			handler.stop();
		}
	}
}
