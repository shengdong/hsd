package com.stsing.fixclient;
import org.apache.log4j.Logger;

import quickfix.Application;
import quickfix.DoNotSend;
import quickfix.FieldNotFound;
import quickfix.IncorrectDataFormat;
import quickfix.IncorrectTagValue;
import quickfix.Message;
import quickfix.RejectLogon;
import quickfix.SessionID;
import quickfix.UnsupportedMessageType;
import quickfix.fix44.MessageCracker;

/**
 * FixApplication， 跟fix server交互的主程序
 * @author k1193
 *
 */
public class FixApplication extends MessageCracker implements Application{
	/**
	 * decalre logger
	 */
	private static final Logger logger = Logger.getLogger(FixApplication.class);
	

	@Override
	public void onMessage(quickfix.fix44.MarketDataSnapshotFullRefresh message, SessionID sessionID) 
			throws FieldNotFound ,UnsupportedMessageType ,IncorrectTagValue {

        MainHandler.Instance().getMsgHandler().addMsg(message);
	};
	
	@Override
	public void onMessage(quickfix.fix44.MarketDataRequestReject message, SessionID sessionID) 
		throws FieldNotFound ,UnsupportedMessageType ,IncorrectTagValue {
		logger.info("##RequestReject: "+message.toString());
	};
	
	@Override
	public void fromAdmin(Message message, SessionID sessionid) throws FieldNotFound,
			IncorrectDataFormat, IncorrectTagValue, RejectLogon {
	}

	@Override
	public void fromApp(Message message, SessionID sessionid) throws FieldNotFound,
			IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
		crack(message, sessionid);
	}

	@Override
	public void onCreate(SessionID sessionid) {
	}

	@Override
	public void onLogon(SessionID sessionid) {
		MainHandler.Instance().request();
	}

	@Override
	public void onLogout(SessionID sessionid) {
	}

	@Override
	public void toAdmin(Message message, SessionID arg1) {
	}

	@Override
	public void toApp(Message arg0, SessionID arg1) throws DoNotSend {
		
	}

}
