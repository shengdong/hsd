package com.stsing.client;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

/**
 * netty
 */
public class MessageDecoder extends FrameDecoder {

    @Override
    protected Object decode(ChannelHandlerContext context, Channel channel, ChannelBuffer buffer)
            throws Exception {
        if (buffer.readableBytes() < 8) {
            return null;
        }
        byte[] lengthByte = new byte[8];
        buffer.getBytes(0, lengthByte);
        int length = Integer.parseInt(getLength(lengthByte));
        if (buffer.readableBytes() < (length + 8)) {
            return null;
        }

        buffer.readBytes(new byte[8]);
        ChannelBuffer cb = buffer.readBytes(length);
        return cb;
    }

    /**
     * 
     *
     * @param data 数据
     * @return decode 8 byte , get length
     */
    private String getLength(byte[] data) {
        byte[] result = new byte[8];
        int counter = 1;
        for (int i = 0; i < data.length; i++) {
            char k = (char) data[i];
            if (k != '0') {
                counter = i;
                break;
            }
        }
        System.arraycopy(data, counter, result, 0, data.length - counter);
        return new String(result).trim();
    }
}
