package com.stsing.client;

import org.apache.log4j.PropertyConfigurator;

/**
 * @author k1193
 */
public class Main {


    /**
     * @param args main args
     */
    public static void main(String[] args) {
        PropertyConfigurator.configure("../control/log4j.properties");

        // 启动server，接受客户端发送过来的报文，
        new SocketServer().run();


        // 启动定时器，将定时发送给核心报文
        new Thread(new Runnable() {

            @Override
            public void run() {
                new QuertzPrime().runMain();

            }
        }).start();

		 //启动fix client，从fix server接受消息，推送给核心系统
//
		new Thread(new Runnable() {

			@Override
			public void run() {
				new FixClientPrime().runMain();

			}
		}).start();
    }
}
