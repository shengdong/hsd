package com.stsing.client;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.stsing.handler.MessageHandler;
import com.stsing.handler.TenorHandler;
import com.stsing.job.JobDisPatcherHandler;
/**
 * NettyHandler
 *
 */
public class ServerHanderDispatcher extends SimpleChannelHandler {
	/**
	 * declare logger
	 */
    private static final Logger logger = Logger.getLogger(ServerHanderDispatcher.class);
    
    
	@Override
	public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		logger.info("Channel " + e.getChannel().hashCode() + " closed");
	}
	
	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		logger.info("new Channel hashcode: " + e.getChannel().hashCode() + " connect");
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		logger.error("exception from downStrem : " + e);
		e.getChannel().close();
		
	}
	
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		 ChannelBuffer receivedChannelBuffer = (ChannelBuffer) e.getMessage();
		 Channel channel = e.getChannel();
		 byte[] data = receivedChannelBuffer.array();
		 String receivedXml = new String(data).trim();
		 if(receivedXml.startsWith("ESB")) {
			 String jobName = receivedXml.substring(3, receivedXml.length());
			 logger.info("jobName : " + jobName);
			 String rspMsg = JobDisPatcherHandler.executeJob(jobName);
			 channel.write(rspMsg);
			 channel.close();
		 } else if(receivedXml.startsWith("TENOR")) {
			 logger.info(receivedXml);
			 receivedXml = receivedXml.substring(5);
			 String resultMsg = TenorHandler.getInstance().handleMsg(receivedXml);
			 channel.write(resultMsg);
			 channel.close();
		 } else {
			 String returnXml = MessageHandler.getInstance().handlMSG(receivedXml);
			 logger.info(new String(returnXml.getBytes("utf-8")));
			 channel.write(new String(returnXml.getBytes("utf-8")));
			 channel.close();
		 }
	}
}

