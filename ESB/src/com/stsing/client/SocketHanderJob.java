package com.stsing.client;

import org.apache.log4j.Logger;

import com.stsing.job.JobDisPatcherHandler;

/**
 * 
 * @author k1193
 *
 */
public class SocketHanderJob {
	/**
	 * declare
	 */
	private static final Logger logger = Logger.getLogger(SocketHanderJob.class);
	
	/**
	 * 
	 * @param jobName job name, execute job by jobName
	 * @return rsp message
	 */
	public static String handlerJob(String jobName) {
		String message = JobDisPatcherHandler.executeJob(jobName);
		logger.info("message : " + message);
		return message;
	}

}
