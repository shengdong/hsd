package com.stsing.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

/**
 * @author k1193
 */
public class EsbSocketClient {
    /**
     */
    private static final Logger logger = Logger.getLogger(EsbSocketClient.class);

    /**
     * declare Header
     */
    public static final String HEADER = "TENOR";

    /**
     * send to esb
     *
     * @param ip ip
     * @param port port
     * @param timeout timeout
     * @param message message
     * @return rsp of send to esb
     */
    public static String sendToESB(String ip, int port, int timeout, String message) {
        String sendMessage = getHeader(message);
        logger.info("send message: " + sendMessage);
        Socket socket = null;

        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            socket = new Socket(ip, port);
            socket.setSoTimeout(timeout);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            outputStream.write(sendMessage.getBytes());
            outputStream.flush();

            byte[] receivedMsg = new byte[1024];
            StringBuffer buffer = new StringBuffer("");
            int count = 0;
            while ((count = inputStream.read(receivedMsg, 0, receivedMsg.length)) > 0) {
                String str = new String(receivedMsg, 0, count, "utf-8").trim();

                buffer.append(str);
            }
            return buffer.toString();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return "";
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }

            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * 
     *
     * @param message message
     * @return get header by message
     */
    private static String getHeader(String message) {
        int lenMsg = message.getBytes().length;
        String lengthString = lenMsg + "";

        int length = lengthString.length();
        StringBuffer buffer = new StringBuffer();
        for (int i = length; i < 8; i++) {
            buffer.append("0");
        }
        buffer.append(lengthString).append(message);
        return buffer.toString();
    }
}
