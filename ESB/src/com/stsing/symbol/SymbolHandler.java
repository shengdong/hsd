package com.stsing.symbol;

import java.util.Calendar;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.stsing.fixclient.PriceObject;
import com.stsing.util.ConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class SymbolHandler extends Thread{
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SymbolHandler.class);
	
	/**
	 * declare  blockingQueue
	 */
	private LinkedBlockingQueue<PriceObject> blockingQueue = null;

    /**
     * declare threadTime
     */
    private int threadTime = 0;


	/**
	 * 构造函数，初始化blockingQueue对象
	 */
	public SymbolHandler() {
		blockingQueue = new LinkedBlockingQueue<PriceObject>();
        threadTime = ConfigUtil.getInt("fix.sleepTime") * 1000;
	}
	
	/**
	 * @param object 接受的报文
	 */
	public void addPriceObject(PriceObject object) {
        synchronized (this) {
            if(blockingQueue.size() > 1000) {
                blockingQueue.clear();
            }
            blockingQueue.add(object);
        }
	}

	@Override
	public void run() {
		while(true) {
			try {
				int count = blockingQueue.size();
				if(count > 1) {
					for(int i=1; i<count; i++) {
						blockingQueue.take();
					}
				}
				PriceObject object = blockingQueue.take();
				process(object);

				Thread.sleep((long)threadTime);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * 
	 * @param object 对象
	 */
	private void process(PriceObject object) {
		Calendar calendar = Calendar.getInstance();
		int time = calendar.get(Calendar.HOUR_OF_DAY);
		
		String c166s = ConfigUtil.getString("esb.c166time");
		int c166_begin = Integer.parseInt(c166s.split("-")[0].trim());
		int c166_end = Integer.parseInt(c166s.split("-")[1].trim());
		
		if(time >= c166_begin && time < c166_end) {
			String result_c166 = SymbolC166.getIntance().sendMessage(object);
			logger.warn("rsp message c166: " + result_c166);
		}
		
		String c181s = ConfigUtil.getString("esb.c181time");
		int c181_begin = Integer.parseInt(c181s.split("-")[0].trim());
		int c181_end = Integer.parseInt(c181s.split("-")[1].trim());
		
		if(time >= c181_begin && time < c181_end) {
			String result_c181 =  SymbolC181.getInstance().sendMessage(object);
			logger.warn("rsp message c181:  " + result_c181);
		}
	}
}
