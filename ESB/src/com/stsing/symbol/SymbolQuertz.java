package com.stsing.symbol;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.stsing.job.JobC491Symbol;
import com.stsing.job.JobCC40Symbol;
import com.stsing.job.JobTY01Symbol;
import com.stsing.util.ConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class SymbolQuertz {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SymbolHandler.class);

	
	/**
	 * declare scheduler
	 */
	private Scheduler scheduler = null;

	
	/**
	 * 构造函数，初始化SymbolQuertz
	 */
	public SymbolQuertz() {
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * 运行程序
	 */
	public void run() {
		try {
			String cc40Trigger = ConfigUtil.getString("job.cc40Trigger");
			scheduler.scheduleJob(JobBuilder.newJob(JobCC40Symbol.class)
					.withIdentity("cc40Job").build(), TriggerBuilder
					.newTrigger().withIdentity("cc40trigger").withSchedule(
							CronScheduleBuilder.cronSchedule(cc40Trigger)).startNow()
					.build());
			
			String c491Tigger = ConfigUtil.getString("job.c491Trigger");
			scheduler.scheduleJob(JobBuilder.newJob(JobC491Symbol.class)
					.withIdentity("c491Job").build(), TriggerBuilder
					.newTrigger().withIdentity("c491trigger").withSchedule(
							CronScheduleBuilder.cronSchedule(c491Tigger)).startNow()
					.build());
			
			String ty01Trigger = ConfigUtil.getString("job.ty01Trigger");
			scheduler.scheduleJob(JobBuilder.newJob(JobTY01Symbol.class)
					.withIdentity("ty01Job").build(), TriggerBuilder
					.newTrigger().withIdentity("ty01trigger").withSchedule(
							CronScheduleBuilder.cronSchedule(ty01Trigger)).startNow()
					.build());
			
			scheduler.start();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * 停止程序
	 */
	public void stop() {
		try {
			scheduler.shutdown();
			scheduler = null;
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
