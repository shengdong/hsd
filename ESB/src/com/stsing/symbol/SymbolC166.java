package com.stsing.symbol;

import org.apache.commons.lang.StringUtils;
import com.stsing.client.SocketClient;
import com.stsing.fixclient.PriceObject;
import com.stsing.util.ConfigUtil;

import java.text.DecimalFormat;

/**
 * 
 * @author k1193
 *
 */
public class SymbolC166 {
	/**
	 * declare symbolC166
	 */
	private static SymbolC166 symbolC166 = null;
	
	
	/**
	 * 
	 * @return symbolC166
	 */
	public static synchronized SymbolC166 getIntance() {
		if(symbolC166 == null) {
			symbolC166 = new SymbolC166();
		}
		return symbolC166;
	}
	
	/**
	 * 
	 * @param object 
	 * @return  发送得报文
	 */
	public String sendMessage(PriceObject object) {
		String ip = ConfigUtil.getString("esb.ip");
		int port = ConfigUtil.getInt("esb.port");
		int timeout = ConfigUtil.getInt("esb.timeout");
		String message = getMessage(object);
		return SocketClient.sendToESB(ip, port, timeout, message);
	}

	
	/**
	 * 
	 * @param object 
	 * @return 得到发送的报文
	 */
	private String getMessage(PriceObject object) {
		String ConsumerSeqNo = ConsumerSeqNoUtil.getConsumerSeqNo();
		String ConsumerId = ConsumerSeqNo.substring(ConsumerSeqNo.length()-8, ConsumerSeqNo.length());
		String TranDate = ConfigUtil.getToday("yyyy-MM-dd");
		String TranTime = ConfigUtil.getToday("HHmmss");
		String QtnDt = ConfigUtil.getToday("yyyyMMdd");
		
		String tenor = object.getTenor().trim();
		tenor = "SPOT";
		
		String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><service><LOCAL_HEAD><TranCode>C166</TranCode></LOCAL_HEAD>" +
		   		"<SYS_HEAD><ServiceCode>07002000001</ServiceCode><ServiceScene>13</ServiceScene><ConsumerSeqNo>" + ConsumerSeqNo + "</ConsumerSeqNo>" +
		   		"<ConsumerId>" + ConsumerId + "</ConsumerId>" + "<TranDate>" + TranDate + "</TranDate>" + "<TranTime>" +TranTime + "</TranTime>"+ "</SYS_HEAD>"+
		   		"<APP_HEAD><TranTellerNo>999</TranTellerNo>"+"<array><ApprTellerArray><ApprTellerNo></ApprTellerNo>"+"</ApprTellerArray></array></APP_HEAD>"+
		   		"<BODY><CcyPr>" + object.getSymbol() + "</CcyPr><BranchId>99999</BranchId><QtnTm>" + TranTime + "</QtnTm>" +
		   		"<TxnCd>9999</TxnCd><TxnTp>FX.CROSS</TxnTp><QtnDt>" + QtnDt + "</QtnDt><Tms>" + 1 + "</Tms><array>";
		String body = "<SbmtInf><TermTp>" + tenor + "</TermTp>" + 
   		"<SellPrc>" + formatPrice(object.getAsk(), object.getSymbol()) + "</SellPrc><ASKFlg>" + getFlg(object.getAsk()) + "</ASKFlg><ASKPrcTm>" + getPrcVldTm(object.getAsktime()) + "</ASKPrcTm><BuyPrc>" +
                formatPrice(object.getBid(), object.getSymbol()) + "</BuyPrc><BINFlg>" + getFlg(object.getBid()) + "</BINFlg><BINPrcTm>" + getPrcVldTm(object.getBidtime()) + "</BINPrcTm>"
   		+"</SbmtInf>";

        String footer = "</array></BODY></service>";
		return header + body + footer;
	}
	

	/**
	 * 
	 * @param price 
	 * @return  得到标示
	 */
	private String getFlg(String price) {
		if(StringUtils.contains(price, "-")) {
			return "1";
		}
		return "0";
	}
	
	/**
	 * 
	 * @param time 时间
	 * @return 格式转化后的时间
	 */
	private String getPrcVldTm(String time) {
		String str = "";
		
		for(int i=0; i<time.length(); i++) {
			if(time.charAt(i) >=48 && time.charAt(i) <= 57) {
				str = str + time.charAt(i);
			}
		}
		return ConfigUtil.getToday("yyyyMMdd") + str;
	}

    /**
     *
     * @param price `价格
     * @return 一定格式的价格
     */
    private static String formatPrice(String priceString, String symbol) {
    	String price = priceString;
        if(StringUtils.contains(price, "-")) {
            price = price.substring(1);
        }
        DecimalFormat format = new DecimalFormat("000000000000.0000");
        String result = format.format(Double.parseDouble(price));
        return result;
    }
  
}
