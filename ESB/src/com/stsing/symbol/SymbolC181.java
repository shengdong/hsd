package com.stsing.symbol;

import com.stsing.client.SocketClient;
import com.stsing.fixclient.PriceObject;
import com.stsing.job.C181JobUtil;
import com.stsing.util.ConfigUtil;

/**
 * 
 * @author k1193
 * 
 */
public class SymbolC181 extends SymbolHandler {
	/**
	 * declare symbolC181
	 */
	private static SymbolC181 symbolC181 = null;

	/**
	 * 
	 * @return 返回单例的symbolC181对象
	 */
	public static synchronized SymbolC181 getInstance() {
		if (symbolC181 == null) {
			symbolC181 = new SymbolC181();
		}
		return symbolC181;
	}

	/**
	 * 
	 * @param object
	 *            接收到的对象
	 * @return 接收到的报文
	 */
	public String sendMessage(PriceObject object) {
		String ip = ConfigUtil.getString("esb.ip");
		int port = ConfigUtil.getInt("esb.port");
		int timeout = ConfigUtil.getInt("esb.timeout");
		String message = C181JobUtil.getMessage(object);
		return SocketClient.sendToESB(ip, port, timeout, message);
	}
}
