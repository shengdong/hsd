package com.stsing.symbol;

/**
 * 
 * @author k1193
 *
 */
public class SymbolConstants {
	/**
	 * declare USD
	 */
	public static final String USDCNY = "USD";
	
	/**
	 * declare EUR
	 */
	public static final String EURCNY = "EUR";
	
	/**
	 * declare JPY
	 */
	public static final String JPYCNY = "JPY";
	
	
	/**
	 * declare HKD
	 */
	public static final String HKDCNY = "HKD";
	
	/**
	 * declare GBP
	 */
	public static final String GBPCNY = "GBP";
	
	
	/**
	 * declare CHF
	 */
	public static final String CHFCNY = "CHF";
	
	
	/**
	 * declare CAD 
	 */
	public static final String CADCNY = "CAD";
	
	/**
	 * declare AUDS
	 */
	public static final String AUDCNY = "AUD";
	
	
	/**
	 * declare SGD
	 */
	public static final String SGDCNY = "SGD";

}
