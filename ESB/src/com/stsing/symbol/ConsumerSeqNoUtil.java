package com.stsing.symbol;

import com.stsing.util.ConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class ConsumerSeqNoUtil {

	/**
	 * 
	 * @return 返回ConsumerSeqNo
	 */
	public static synchronized String getConsumerSeqNo() {
		String systemID = ConfigUtil.getString("SeqNo.number");
		String systemDate = ConfigUtil.getToday("yyyyMMdd");
		
		String serialNumber = getSerialNumber();
		return systemID + systemDate + serialNumber;
	}
	
    /**
     * 
     * @return 随即的8为数字
     */
	private static String getSerialNumber() {
		String number = Math.random() * 10 + "";
		return number.substring(0, 8).replace(".", "0");
	}
}
