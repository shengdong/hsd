package com.stsing.symbol;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.fixclient.MainHandler;
import com.stsing.fixclient.PriceObject;
/**
 * 
 * @author k1193
 *
 */
public class SymbolDispatcher {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SymbolDispatcher.class);
	
	/**
	 * declare dispatcher
	 */
    private static SymbolDispatcher dispatcher;
    
    
    /**
     * 
     * @return 返回单例的dispather
     */
    public static synchronized SymbolDispatcher getInstance() {
    	if(dispatcher == null) {
    		synchronized (SymbolDispatcher.class) {
				if(dispatcher == null) {
					dispatcher = new SymbolDispatcher();
				}
			}
    	}
    	return dispatcher;
    }
    
    /**
     * 
     * @param priceObject 接受到得对象
     */
    public void processPriceObject(PriceObject priceObject) {
    	logger.info(priceObject);
    	String symbol = priceObject.symbol;
    	if(checkSymbol(symbol, SymbolConstants.AUDCNY)) {
    		MainHandler.Instance().getAudcny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.CADCNY)) {
    		MainHandler.Instance().getCadcny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.CHFCNY)) {
    		MainHandler.Instance().getChfcny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.EURCNY)) {
    		MainHandler.Instance().getEurcny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.GBPCNY)) {
    		MainHandler.Instance().getGbpcny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.HKDCNY)) {
    		MainHandler.Instance().getHkdcny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.JPYCNY)) {
    		MainHandler.Instance().getJpycny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.SGDCNY)) {
    		MainHandler.Instance().getSgdcny().addPriceObject(priceObject);
    	}
    	
    	if(checkSymbol(symbol, SymbolConstants.USDCNY)) {
    		MainHandler.Instance().getUsdcny().addPriceObject(priceObject);
    	}
    }
    
    /**
     * 
     * @param ric 接收到的RIC码
     * @param symbol 货币对
     * @return 是否是我们要的货币对
     */
    private boolean checkSymbol(String ric, String symbol) {
    	if(StringUtils.contains(ric, symbol)) {
    		return true;
    	}
    	return false;
    }
	
}
