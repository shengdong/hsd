package com.stsing.handler;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;

import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;

/**
 * RedisHandler
 *
 */
public class RedisHandler {
	/**
	 * declare logger
	 */
    private static final Logger logger = Logger.getLogger(RedisHandler.class);
    
    /**
     * 
     * @param day 时间
     * @param ccy 主币种
     * @param ccy1 次币种
     * @param sale 价格
     * @param type 类型
     * @return  得到Spot结果Map
     */
    public Map<String, String> getSpot(String day, String ccy_z, String ccy_c, String sale, String type) {
    	String ccy = ccy_z;
    	String ccy1 = ccy_c;
    	Map<String, String> map = new HashMap<String, String>(2);
    	try {
    		String ccys = ccy+"."+ccy1;
	    	DBUtil util = new DBUtil();
	    	logger.info(day + "," + ccy + "," + ccy1 + "," + sale + "," + type);
	    	if(StringUtils.equals(ccy, "USD")) {
	    		ccy = "";
	    	}
	    	
	    	if(StringUtils.equals(ccy1, "USD")) {
	    		ccy1 = "";
	    	}
	    	String[] days = day.split("-");
	    	List<String> lists = util.getQiXianBy(days[0]+days[1]+days[2], ccys);
	    	
	    	logger.info("hsd : lists: " + lists);
	    	
	    	if(lists == null || lists.size() == 0) {
	    		map.put("error", "can not get price of " + ccys);
	    		return map;
	    	}
	    	
	    	Map<String, Object> discountMap = util.getDiscount(Double.parseDouble(sale), ccy+"/"+ccy1, type);

	    	String resultSpot = getPrice(ccy + ccy1 + "=BANK", type);
	    	
	    	double resultDiaoQi = 0;
    	
	    	if (lists.size() == 1) {
	    		String key = ccy + ccy1 + lists.get(0) + "=BANK";
	    		if(StringUtils.containsIgnoreCase(lists.get(0), "SPOT")) {
	    			resultDiaoQi = 0;
	    		} else {
	    			double value = Double.parseDouble(getPrice(key, type));

                    int dis_type = (Integer)discountMap.get("DISCOUNT_TYPE");
                    double discount = (Double)discountMap.get("DISCOUNT");

                    if(dis_type == 2) {
                        resultDiaoQi = value * discount;
                    } else {
                        resultDiaoQi = value + discount;
                    }
	    		}
	    		
	    	} else if (lists.size() == 2) {
	    		String key1 = ccy + ccy1 + lists.get(0) + "=BANK";
	    		logger.info(key1);
	    		double value1 = 0;
	    		if(!StringUtils.containsIgnoreCase(key1, "SPOT")) {
	    			value1 = Double.parseDouble(getPrice(key1, type));
	    		}
	    		
	    		
	    		String key2 = ccy + ccy1 + lists.get(1) + "=BANK";
	    		logger.info(key2);
	    		double value2 = 0;
	    		if(!StringUtils.containsIgnoreCase(key2, "SPOT")) {
	    			value2 = Double.parseDouble(getPrice(key2, type));
	    		}
	    		logger.info(value2);
	    		List<String> date = util.getDate(lists.get(0), lists.get(1), ccys);
	    		
	    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	    		long numerator = (sdf.parse(days[0]+days[1]+days[2]).getTime()-sdf.parse(date.get(0)).getTime())/1000/60/60/24;
	    		long denominator = (sdf.parse(date.get(1)).getTime()-sdf.parse(date.get(0)).getTime())/1000/60/60/24;
	    		logger.info("hsd: " + date.get(0));
	    		logger.info("hsd: " + date.get(1));
	    		logger.info("hsd: " + numerator);
	    		logger.info("hsd: " + denominator);
	    		logger.info("hsd: " + value1);
	    		logger.info("hsd: " + value2);


                int dis_type = (Integer)discountMap.get("DISCOUNT_TYPE");
                double discount = (Double)discountMap.get("DISCOUNT");
                if(dis_type == 2) {
                    resultDiaoQi =  (value1 + (value2 - value1) * numerator/denominator) * discount;
                } else {
                    resultDiaoQi = (value1 + (value2 - value1) * numerator/denominator) + discount;
                }
	    	}
	    	java.text.DecimalFormat df = new java.text.DecimalFormat("#.####");
	    	map.put("spot", resultSpot);
	    	map.put("changeOver", df.format(resultDiaoQi) + "");
	    	return map;
    	} catch (Exception e) {
    		map.put("error", "connection error, on price");
			return map;
		}
    }
	
    /**
     * 
     * @param key key
     * @param type type
     * @return 得到价格 
     * @throws Exception 错误异常
     */
    private String getPrice(String key, String type) throws Exception {
    	try {
	    	String ip = ConfigUtil.getString("redis.ip");
	    	int port = ConfigUtil.getInt("redis.port");
	    	Jedis jedis = new Jedis(ip, port);
	    	Map<String, String> map = jedis.hgetAll(key);
	    	return map.get(type);
    	} catch (Exception e) {
			throw new Exception();
		}
    }
    

    
}
