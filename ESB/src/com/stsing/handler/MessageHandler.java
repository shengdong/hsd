package com.stsing.handler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.util.ConfigUtil;
import com.stsing.util.XMLTransform;

/**
 * 
 * @author k1193
 *
 */
public class MessageHandler {
    /**
     * declare logger 
     */
	private static final Logger logger = Logger.getLogger(MessageHandler.class);
	
	
	/**
	 * declare messageHandler
	 */
	private static MessageHandler messageHandler = null;
	
	
	
	/**
	 * 
	 * @return get instance if MessageHandler
	 */
	public static synchronized MessageHandler getInstance() {
		if(messageHandler == null) {
			messageHandler = new MessageHandler();
		}
		return messageHandler;
		
	}
	
	/**
	 * 
	 * @param receivedXml
	 * @return Handler MSG XML
	 */
	public String handlMSG(String receivedXml) {
		logger.info("received message : " + receivedXml);
		logger.info("save to file");
		String rep = "";
		String filePath = saveToFile(receivedXml);
		if(StringUtils.isBlank(filePath)) {
			handlMSG(receivedXml);
		} else {
			logger.warn("filepath:" + filePath);
			rep = XMLTransform.resultTempsql(filePath);
			
		}
		return MessageHandler.generateStreamHeader(rep);
	}
	
	/**
	 * 
	 * @param FileValue 解析后的内容
	 * @return 得到报文头
	 */
	private static String generateStreamHeader(String FileValue){
		
        long fileLength = FileValue.getBytes().length;
        logger.info("file`s length: " + fileLength);
        
        DecimalFormat df = new DecimalFormat("00000000");
        String str = df.format(fileLength)+FileValue;
        return str;
    }
	
	/**
	 * 
	 * @param msg message
	 * @return svae to file
	 */
    private String saveToFile(String msg) {
    	String filePath = ConfigUtil.getString("netty.filePath") + File.separator + getToday();
    	File file = new File(filePath);
    	if(!file.exists()) {
    		logger.info("make mkdirs");
    		file.mkdir();
    	}
    	
    	String fileName = UUID.randomUUID().toString().substring(1, 10) + ".xml";
    	OutputStream stream = null;
    	filePath = filePath + File.separator + fileName;
    	try {
    		stream = new FileOutputStream(filePath);
    		stream.write(msg.getBytes("UTF-8"));
    		stream.flush();
    		return filePath;
    		
    	} catch (Exception e) {
    		logger.error(e.getMessage(), e);
    		return "";
		} finally {
			if(stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
    }
    
    /**
     *
     * @return  get String of today
     */
    private String getToday() {
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    	return dateFormat.format(new Date());
    }
}
