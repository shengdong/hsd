package com.stsing.handler;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.stsing.jedis.JedisUtil;
import com.stsing.jedis.TenorUtil;
import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.GsonFactory;

/**
 * 
 * @author k1193
 *
 */
public class TenorHandler {

	/**
     * declare logger 
     */
	private static final Logger logger = Logger.getLogger(TenorHandler.class);
	
	
	/**
	 * declare messageHandler
	 */
	private static TenorHandler tenorHandler = null;
	
	
	/**
	 * get instance if TenorHandler
	 * @return
	 */
	public static synchronized TenorHandler getInstance() {
		if(tenorHandler == null) {
			synchronized (TenorHandler.class) {
				tenorHandler = new TenorHandler();
			}
		}
		return tenorHandler;
	}
	
	/**
	 * 
	 * @param message message
	 * @return 处理好的message
	 */
	public String handleMsg(String message) {
		String symbol = message.split(";")[0];
		String tenor = message.split(";")[1];
		logger.info(symbol);
		//select VALUEDATE_7M  from valuedate t  where t.tradedate = '20161222' and t.ccys = 'USD.CNY'
		String sql = "select VALUEDATE_" + tenor + " from valuedate t where t.tradedate = '" + ConfigUtil.getToday("yyyyMMdd") + "' and t.ccys = '" + symbol + "'";
		List<Map<String, Object>> lists = new DBUtil().getDataFromDB(sql);
		String endTime = (String)lists.get(0).get("VALUEDATE_" + tenor);
		TenorUtil tenorUtil = new TenorUtil(JedisUtil.CLUSTER);
		Map<String, String> map = tenorUtil.getPriceBy(symbol, endTime);
		tenorUtil.close();
		return GsonFactory.getGson().toJson(map);
	}
	
	/**
	 * 
	 * @param symbol 币种
	 * @param tenor 期限
	 * @return 从oracle获得的值
	 */
	public Map<String, String> handleMsg(String symbol, String tenor) {
		String sql = "select VALUEDATE_" + tenor + " from valuedate t where t.tradedate = '" + ConfigUtil.getToday("yyyyMMdd") + "' and t.ccys = '" + symbol + "'";
		List<Map<String, Object>> lists = new DBUtil().getDataFromDB(sql);
		String endTime = (String)lists.get(0).get("VALUEDATE_" + tenor);
		TenorUtil tenorUtil = new TenorUtil(JedisUtil.CLUSTER);
		Map<String, String> map = tenorUtil.getPriceBy(symbol, endTime);
		tenorUtil.close();
		return map;
	}
}
