package com.stsing.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.stsing.client.SocketClient;
import com.stsing.jedis.JedisClusterUtil;
import com.stsing.jedis.JedisSingleUtil;
import com.stsing.jedis.JedisUtil;
import com.stsing.symbol.ConsumerSeqNoUtil;
import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.GsonFactory;

/**
 * TY01的报文
 * 
 * @author k1193
 * 
 */
public class JobTY01 implements JobExecute {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JobTY01.class);

	/**
	 * declare rics
	 */
	private String[] rics = null;

	/**
	 * 初始化ccyMap
	 */
	private Map<String, String> ccyMap = null;
	
	/**
	 * 初始化dbutil
	 */
	private DBUtil dbUtil = null;
	
	/**
	 * 定义 tenorLists
	 */
	private List<String> tenorLists = null;
	
	/**
	 * 定义 bors
	 */
	private static String bors[] =  null;

	/**
	 * 构造函数，初始化ccymap的Map
	 */
	public JobTY01() {
		dbUtil = new DBUtil();
		rics = ConfigUtil.getString("esb.ty01ccy").split(",");
		ccyMap = new HashMap<String, String>();
		for (String ric : rics) {
			ccyMap.put(ric.split("=")[0], ric.split("=")[1]);
		}
		bors = ConfigUtil.getString("esb.ty01bors").split(",");
		String tenors[] = ConfigUtil.getString("esb.ty01tenor").split(",");
		
		tenorLists = new ArrayList<String>(15);
		for(String tenor : tenors) {
			tenorLists.add(tenor.trim());
		}
	}

//	@Override
//	public String execute() {
//		Jedis jedis = JedisSingleUtil.getInstance().getJedis();
//		/**
//		 * result，该数据可以直接取出放在报文中,发送给核心
//		 */
//		List<Map<String, String>> result = new ArrayList<Map<String, String>>(100);
//		/**
//		 * dataFromRedisOrOracle 存放的是从redis 或者 从 oralce 上拿到的原始数据，没有经过过滤和封装的数据
//		 */
//		List<Map<String, String>> dataFromRedisOrOracle = new ArrayList<Map<String, String>>(100);
//		
//		if (jedis != null) {
//			logger.info("get date from redis.....");
//			for(String bor : bors) {
//				Set<String> liborLists = jedis.smembers(bor);
//				logger.info("bor : " + bor + ", number of lists is : " + liborLists.size());
//				List<Response<Map<String, String>>> rsp = new ArrayList<Response<Map<String, String>>>(
//						liborLists.size());
//				Pipeline pipeline = jedis.pipelined();
//				for (String key : liborLists) {
//					rsp.add(pipeline.hgetAll(key));
//				}
//				pipeline.sync();
//				for (Response<Map<String, String>> rs : rsp) {
//					dataFromRedisOrOracle.add(rs.get());
//				}
//			}
//			
//			String sgdSibors = ConfigUtil.getString("esb.SGDSIBOR");
//			Set<String> sgdSibor = 	jedis.smembers(sgdSibors);
//			for(String key : sgdSibor) {
//				key = "/" + key;
//				Map<String, String> map = jedis.hgetAll(key);
//				dataFromRedisOrOracle.add(map);
//			}
//			if(jedis != null) {
//				jedis.close();
//			}
//
//		} else {
//            logger.info("get date from oracle.........");
//			dataFromRedisOrOracle = getDataFromOracle();
//		}
//
//        for(Map<String, String> p : dataFromRedisOrOracle) {
//            String ric = p.get("ric");
//            if(StringUtils.contains(ric, "SGD")) {
//                p.put("tenor", p.get("symbol").substring(5, 7));
//            }
//        }
//
//		for (Map<String, String> m : dataFromRedisOrOracle) {
//			if (m != null && m.size() != 0 && m.get("ric") != null) {
//					Map<String, String> mapResult = new HashMap<String, String>();
//					String ric = m.get("ric");
//					if(StringUtils.contains(ric, "365")) {
//						continue;
//					}
//					String ccy = "";
//					double bid = Double.parseDouble(m.get("primact_1"));
//					String term = m.get("tenor").trim();
//					
//					if(!tenorLists.contains(term)) {
//						continue;
//					}
//					if (StringUtils.contains(ric, "CNY")) {
//						mapResult.put("FrxRateKnd", "1");
//						ccy = "CNY";
//					} else {
//						mapResult.put("FrxRateKnd", "2");
//						ccy = ric.substring(0, 3);
//					}
//					
//					if(StringUtils.contains(ric, "HKD")) {
//						ccy = "HKD";
//					} else if(StringUtils.contains(ric, "SISGD")) {
//						ccy = "SGD";
//					}
//
//					if (!isCcy(ccy)) {
//						continue;
//					}
//					ccy = ccyMap.get(ccy);
//					mapResult.put("Ccy", ccy);
//					if(StringUtils.equals(term, "SW")) {
//						term = "1W";
//					}
//					mapResult.put("IntRate", ConfigUtil.getDouble(bid, "00.000000").replace(".", "").replace("-", ""));
//					mapResult.put("Term", term.substring(0, term.length() - 1));
//					mapResult.put("TermTp", term.substring(term.length() - 1, term.length()));
//					mapResult.put("PblsDt", ConfigUtil.getToday("yyyyMMdd"));
//					mapResult.put("FrxRateFlg", bid >= 0 ? "+" : "-");
//					result.add(mapResult);
//				}
//		}
//		String rsp = sendMessage(result);
//		return rsp;
//	}
	
	@Override
	public String execute() {
		String flag = ConfigUtil.getString("redis.flag");
		JedisUtil jedis = null;
		if(StringUtils.equals(flag, JedisUtil.CLUSTER)) {
			jedis = new JedisClusterUtil();
		} else if (StringUtils.equals(flag, JedisUtil.JEDIS)) {
			jedis = new JedisSingleUtil();
		}
		/**
		 * result，该数据可以直接取出放在报文中,发送给核心
		 */
		List<Map<String, String>> result = new ArrayList<Map<String, String>>(100);
		/**
		 * dataFromRedisOrOracle 存放的是从redis 或者 从 oralce 上拿到的原始数据，没有经过过滤和封装的数据
		 */
		List<Map<String, String>> dataFromRedisOrOracle = new ArrayList<Map<String, String>>(100);
		
		if (jedis != null && jedis.ping()) {
			logger.info("get date from redis.....");
			for(String bor : bors) {
				Set<String> liborLists = jedis.smembers(bor);
				dataFromRedisOrOracle.addAll(jedis.piplined(liborLists));
			}
			
			String sgdSibors = ConfigUtil.getString("esb.SGDSIBOR");
			Set<String> sgdSibor = 	jedis.smembers(sgdSibors);
			String key1 = "";
			for(String key : sgdSibor) {
				key1 = "/" + key;
				Map<String, String> map = jedis.hgetAll(key1);
				dataFromRedisOrOracle.add(map);
			}
			

		} else {
            logger.info("get date from oracle.........");
			dataFromRedisOrOracle = getDataFromOracle();
		}
		
		if(jedis != null) {
			jedis.close();
		}

        for(Map<String, String> p : dataFromRedisOrOracle) {
            String ric = p.get("ric");
            if(StringUtils.contains(ric, "SGD")) {
                p.put("tenor", ric.substring(6, 8));
            }
        }

		for (Map<String, String> m : dataFromRedisOrOracle) {
			if (m != null && m.size() != 0 && m.get("ric") != null) {
					Map<String, String> mapResult = new HashMap<String, String>();
					String ric = m.get("ric");
					if(StringUtils.contains(ric, "365")) {
						continue;
					}
					String ccy = "";
					double bid = Double.parseDouble(m.get("primact_1"));
					String term = m.get("tenor").trim();
					
					if(!tenorLists.contains(term)) {
						continue;
					}
					if (StringUtils.contains(ric, "CNY")) {
						mapResult.put("FrxRateKnd", "1");
						ccy = "CNY";
					} else {
						mapResult.put("FrxRateKnd", "2");
						ccy = ric.substring(0, 3);
					}
					
					if(StringUtils.contains(ric, "HKD")) {
						ccy = "HKD";
					} else if(StringUtils.contains(ric, "SISGD")) {
						ccy = "SGD";
					}

					if (!isCcy(ccy)) {
						continue;
					}
					ccy = ccyMap.get(ccy);
					mapResult.put("Ccy", ccy);
					if(StringUtils.equals(term, "SW")) {
						term = "1W";
					}
					mapResult.put("IntRate", ConfigUtil.getDouble(bid, "00.000000").replace(".", "").replace("-", ""));
					mapResult.put("Term", term.substring(0, term.length() - 1));
					mapResult.put("TermTp", term.substring(term.length() - 1, term.length()));
					mapResult.put("PblsDt", ConfigUtil.getToday("yyyyMMdd"));
					mapResult.put("FrxRateFlg", bid >= 0 ? "+" : "-");
					result.add(mapResult);
				}
		}
		String rsp = sendMessage(result);
		return rsp;
	}

	/**
	 * 
	 * @param lists 发送的数据
	 * @return  开启一个socket，发送给核心的数据，将结果打印出来
	 *            
	 */
	private String sendMessage(List<Map<String, String>> lists) {
		if (lists.size() == 0) {
			logger.error("send message is null.........");
		}
		String message = getMessage(lists);
		String ip = ConfigUtil.getString("esb.ip");
		int port = ConfigUtil.getInt("esb.port");
		int timeout = ConfigUtil.getInt("esb.timeout");
		return SocketClient.sendToESB(ip, port, timeout, message);
	}
	
	/**
	 * 
	 * @param lists 发送的数据
	 * 
	 * @return 返回报文
	 */
	private String getMessage(List<Map<String, String>> lists) {
		String ConsumerSeqNo = ConsumerSeqNoUtil.getConsumerSeqNo();
		String ConsumerId = ConsumerSeqNo.substring(ConsumerSeqNo.length() - 8,
				ConsumerSeqNo.length());
		String TranDate = ConfigUtil.getToday("yyyy-MM-dd");
		String TranTime = ConfigUtil.getToday("HH:mm:ss");

	
		String body = "";
		int count = 0;
		for (Map<String, String> map : lists) {
			if(!StringUtils.equals((String)map.get("IntRate"), "00000000")) {
				body = body + "<SbmtInf>" + "<Ccy>" + map.get("Ccy") + "</Ccy>"
				+ "<PblsDt>" + map.get("PblsDt") + "</PblsDt>" + "<Term>"
				+ map.get("Term") + "</Term>" + "<TermTp>"
				+ map.get("TermTp") + "</TermTp>" + "<IntRate>"
				+ map.get("IntRate") + "</IntRate>" + "<FrxRateKnd>"
				+ map.get("FrxRateKnd") + "</FrxRateKnd>" + "<FrxRateFlg>"
				+ map.get("FrxRateFlg") + "</FrxRateFlg>" + "</SbmtInf>";
				count ++;
			}
		}
		String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><service><LOCAL_HEAD><TranCode>TY01</TranCode></LOCAL_HEAD>"
			+ "<SYS_HEAD><ServiceCode>06002000003</ServiceCode><ServiceScene>02</ServiceScene><ConsumerSeqNo>"
			+ ConsumerSeqNo
			+ "</ConsumerSeqNo>"
			+ "<ConsumerId>"
			+ ConsumerId
			+ "</ConsumerId>"
			+ "<TranDate>"
			+ TranDate
			+ "</TranDate>"
			+ "<TranTime>"
			+ TranTime
			+ "</TranTime></SYS_HEAD>"
			+ "<APP_HEAD>"
			+ "<TranTellerNo>9999999</TranTellerNo>"
			+ "</APP_HEAD>"
			+ "<BODY>"
			+ "<TellerNo>999</TellerNo>"
			+ "<BranchId>99999</BranchId>"
			+ "<CnlInd>HQ</CnlInd>"
			+ "<Tms>" + count + "</Tms>" + "<array>";
		String footer = "</array></BODY></service>";
		return header + body + footer;
	}

	/**
	 * 
	 * @param ccy
	 *            币种
	 * @return 返回是否要发送的币种
	 */
	private boolean isCcy(String ccy) {
		for (String ric : rics) {
			if (StringUtils.contains(ric, ccy)) {
				return true;
			}
		}
		return false;
	}
	
	
	@SuppressWarnings("unchecked")
//	private List<Map<String, String>> getDataFromOracle() {
//		String borContain = "";
//		for(String bor : bors) {
//	    	borContain = borContain + "'" + bor + "',";
//	    }
//
//        String sgdSibors = ConfigUtil.getString("esb.SGDSIBOR");
//        borContain = borContain + "'" + sgdSibors + "'";
//
//		String sql = "select * from redis_cache t where t.redis_key in(" + borContain +  ")";
//
//		String rics = "";
//		List<Map<String, Object>> lists = dbUtil.getDataFromDB(sql);
//		for(Map<String, Object> map : lists) {
//			rics = rics +  (String)map.get("REDIS_VALUES") + ",";
//		}
//
//		rics = rics.substring(0, rics.length() - 1);
//		String ricArray[] = rics.split(",");
//		String ricContain = "";
//		for(String ric :ricArray) {
//			ricContain = ricContain + "'" + ric + "',";
//		}
//
//		for (String ric : ricArray) {
//			if(StringUtils.contains(ric, "SISGD")) {
//				ric =  "/" + ric;
//			}
//			ricContain = ricContain + "'" + ric + "',";
//		}
//
//
//
//		ricContain = ricContain.substring(0, ricContain.length() - 1);
//		sql = "select * from redis_cache t where t.redis_key in(" + ricContain + ")";
//
//        logger.info("sql : " + sql);
//		List<Map<String, Object>> dataLists = dbUtil.getDataFromDB(sql);
//
//		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
//		for(Map<String, Object> map : dataLists) {
//			result.add(GsonFactory.getGson().fromJson((String)map.get("REDIS_VALUES"), Map.class));
//		}
//		return result;
//	}
	/**
	 * 
	 * @return 如果redis is shutdown, get data from oracle
	 */
    private List<Map<String, String>> getDataFromOracle() {
        String borContain = "";
        for (String bor : bors) {
            borContain = borContain + "'" + bor + "',";
        }
        String sgdSibors = ConfigUtil.getString("esb.SGDSIBOR");
        borContain = borContain + "'" + sgdSibors + "'";

        String sql = "select * from redis_cache t where t.redis_key in(" + borContain + ")";


        String rics = "";
        List<Map<String, Object>> lists = dbUtil.getDataFromDB(sql);
        for (Map<String, Object> map : lists) {
            rics = rics + (String) map.get("REDIS_VALUES") + ",";
        }
        rics = rics.substring(0, rics.length() - 1);
        String ricArray[] = rics.split(",");
        String ricContain = "";
        String ric1 = "";
        for (String ric : ricArray) {
            if(StringUtils.contains(ric, "SISGD")) {
            	ric1 =  "/" + ric;
            }
            ricContain = ricContain + "'" + ric1 + "',";
        }

        ricContain = ricContain.substring(0, ricContain.length() - 1);
        sql = "select * from redis_cache t where t.redis_key in(" + ricContain + ")";
        List<Map<String, Object>> dataLists = dbUtil.getDataFromDB(sql);
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        for (Map<String, Object> map : dataLists) {
            result.add(GsonFactory.getGson().fromJson(
                    (String) map.get("REDIS_VALUES"), Map.class));
        }
        logger.info(result.size());
        return result; 
    }
	
}
