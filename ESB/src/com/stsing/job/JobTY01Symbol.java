package com.stsing.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * C491 job
 * 
 * @author k1193
 * 
 */
public class JobTY01Symbol implements Job {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JobTY01Symbol.class);
	

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getTrigger().getJobKey().getName();
		logger.info("jobName : " + jobName);
		String result = JobDisPatcherHandler.executeJob(jobName);
		logger.info("result : " + result);
	}

}
