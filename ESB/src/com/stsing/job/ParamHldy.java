package com.stsing.job;


import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;

/**
 * 
 * @author k1193
 *
 */
public class ParamHldy {
	/**
	 * 日志
	 */
	private  final Logger logger = Logger.getLogger(ParamHldy.class);
	/**
	 * 节假日
	 */
	private  List<Map<String, String>> holidayList = new ArrayList<Map<String, String>>();
	/**
	 * 节假格式
	 */
	private  SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	/**
	 * 获取节假日
	 */
	private void initHody()  {
		Connection connection = new DBUtil().getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		String ccySql = ConfigUtil.getString("ccyCode.ccySql");
		try {
			statement = connection.prepareStatement(ccySql);
			result = statement.executeQuery();
			while (result.next()) {
				Map<String, String> cc = new HashMap<String, String>();
				cc.put(result.getString("CCY_CODE"), result.getString("HLDY")
						.trim());
				holidayList.add(cc);
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		if (result != null) {
			try {
				result.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * 
	 * @param startDate @param startDate 开始日期
	 * @throws ParseException  ParseException
	 * @throws SQLException SQLException
	 */
	public void setParamHldy(String startDate) throws ParseException,
			SQLException {
		
		initHody();
		
		Connection connection1 = new DBUtil().getConnection();
		PreparedStatement statement1 = null;
		String startDateNew = "";
		if (startDate == null || startDate.equals("") ) {
			Date date = new Date();
			startDate = sdf.format(date);
		}
		try {
			deleteValueDay(startDate);
		} catch (SQLException e1) {
			logger.error(e1.getMessage(), e1);
		}
		String endDate = AddOneMonth(startDate);
		String ccyPair = ConfigUtil.getString("ccy.ccyPair");
		String[] ccyCodes = ccyPair.split(",");
		for (String ccy : ccyCodes) {
			startDateNew = startDate;
			String[] ccyCodes1 = ccy.split("\\.");
			String ccy1 = ccyCodes1[0];
			String ccy2 = ccyCodes1[1];
			Date date = sdf.parse(startDateNew);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			HashMap<String, String> TNSPOT = new HashMap<String, String>();
			String sb = "insert into valuedate(CCYS,TRADEDATE,VALUEDATE_ON,VALUEDATE_TN,VALUEDATE_SPOT ,VALUEDATE_SN,VALUEDATE_SW,VALUEDATE_2W,"
					+ "VALUEDATE_3W,VALUEDATE_1M,VALUEDATE_2M,VALUEDATE_3M,VALUEDATE_4M,VALUEDATE_5M,VALUEDATE_6M,VALUEDATE_7M,VALUEDATE_8M," +
							"VALUEDATE_9M," +
							"VALUEDATE_10M,VALUEDATE_11M,VALUEDATE_1Y,"
					+ "VALUEDATE_13M,VALUEDATE_14M,VALUEDATE_15M,VALUEDATE_16M,VALUEDATE_17M,VALUEDATE_18M,VALUEDATE_19M,VALUEDATE_20M," +
							"VALUEDATE_21M,VALUEDATE_22M,VALUEDATE_23M,"
					+ "VALUEDATE_2Y,VALUEDATE_3Y,VALUEDATE_4Y,VALUEDATE_5Y)values" +
							"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			statement1 = connection1.prepareStatement(sb);

			while (new BigDecimal(startDateNew).compareTo(new BigDecimal(
					endDate)) != 1) {
				TNSPOT = getDates(startDateNew, ccy1, ccy2);
				String spotDay = TNSPOT.get("SPOTDate");
				statement1.setString(1, ccy1 + "." + ccy2);
				statement1.setString(2, startDateNew);
				statement1.setString(3, getONDay(startDateNew, ccy1, ccy2));
				statement1.setString(4, TNSPOT.get("TNDate"));
				statement1.setString(5, spotDay);
				statement1.setString(6, getNextDay(spotDay, ccy1, ccy2));
				statement1.setString(7, getValueDay(spotDay, ccy1, ccy2, "SW"));
				statement1.setString(8, getValueDay(spotDay, ccy1, ccy2, "2W"));
				statement1.setString(9, getValueDay(spotDay, ccy1, ccy2, "3W"));
				statement1
						.setString(10, getValueDay(spotDay, ccy1, ccy2, "1M"));
				statement1
						.setString(11, getValueDay(spotDay, ccy1, ccy2, "2M"));
				statement1
						.setString(12, getValueDay(spotDay, ccy1, ccy2, "3M"));
				statement1
						.setString(13, getValueDay(spotDay, ccy1, ccy2, "4M"));
				statement1
						.setString(14, getValueDay(spotDay, ccy1, ccy2, "5M"));
				statement1
						.setString(15, getValueDay(spotDay, ccy1, ccy2, "6M"));
				statement1
						.setString(16, getValueDay(spotDay, ccy1, ccy2, "7M"));
				statement1
						.setString(17, getValueDay(spotDay, ccy1, ccy2, "8M"));
				statement1
						.setString(18, getValueDay(spotDay, ccy1, ccy2, "9M"));
				statement1.setString(19,
						getValueDay(spotDay, ccy1, ccy2, "10M"));
				statement1.setString(20,
						getValueDay(spotDay, ccy1, ccy2, "11M"));
				statement1
						.setString(21, getValueDay(spotDay, ccy1, ccy2, "1Y"));
				statement1.setString(22,
						getValueDay(spotDay, ccy1, ccy2, "13M"));
				statement1.setString(23,
						getValueDay(spotDay, ccy1, ccy2, "14M"));
				statement1.setString(24,
						getValueDay(spotDay, ccy1, ccy2, "15M"));
				statement1.setString(25,
						getValueDay(spotDay, ccy1, ccy2, "16M"));
				statement1.setString(26,
						getValueDay(spotDay, ccy1, ccy2, "17M"));
				statement1.setString(27,
						getValueDay(spotDay, ccy1, ccy2, "18M"));
				statement1.setString(28,
						getValueDay(spotDay, ccy1, ccy2, "19M"));
				statement1.setString(29,
						getValueDay(spotDay, ccy1, ccy2, "20M"));
				statement1.setString(30,
						getValueDay(spotDay, ccy1, ccy2, "21M"));
				statement1.setString(31,
						getValueDay(spotDay, ccy1, ccy2, "22M"));
				statement1.setString(32,
						getValueDay(spotDay, ccy1, ccy2, "23M"));
				statement1
						.setString(33, getValueDay(spotDay, ccy1, ccy2, "2Y"));
				statement1
						.setString(34, getValueDay(spotDay, ccy1, ccy2, "3Y"));
				statement1
						.setString(35, getValueDay(spotDay, ccy1, ccy2, "4Y"));
				statement1
						.setString(36, getValueDay(spotDay, ccy1, ccy2, "5Y"));
				statement1.addBatch();
				cal.add(Calendar.DATE, 1);
				startDateNew = sdf.format(cal.getTime()).toString();
				if (new BigDecimal(startDateNew).compareTo(new BigDecimal(
						endDate)) == 1) {
					statement1.executeBatch();
					connection1.commit();
				}
			}
		}

		if (statement1 != null) {
			try {
				statement1.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if (connection1 != null) {
			try {
				connection1.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * 删除旧记录
	 * 
	 * @param startDate 开始日期
	 *            
	 * 
	 */
	public void deleteValueDay(String startDate) throws SQLException {
		Connection connection2 = new DBUtil().getConnection();
		PreparedStatement statement2 = null;
		String sb1 = "delete from valuedate  where  tradedate >= " + startDate;
		statement2 = connection2.prepareStatement(sb1);
		statement2.execute();
		if (statement2 != null) {
			try {
				statement2.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if (connection2 != null) {
			try {
				connection2.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}

	}

	/**
	 * 获取到期日
	 * 
	 * @param CurryDay 当前日期
	 *            
	 * 
	 * @param ccy1 币种1
	 *            
	 * 
	 * @param ccy2 币种2
	 *            
	 * 
	 * @param term 期限
	 *            
	 * 
	 * @return ValueDay 到期日
	 */
	public String getValueDay(String CurryDay, String ccy1, String ccy2,
			String term) throws ParseException, java.text.ParseException {
		String ValueDay = "";
		Date date = sdf.parse(CurryDay);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (term.equals("ON")) {
			cal.add(Calendar.DATE, 0);

		} else if (term.equals("TN")) {
			cal.add(Calendar.DATE, 1);

		} else if (term.equals("SPOT")) {
			cal.add(Calendar.DATE, 2);

		} else if (term.equals("SN")) {
			cal.add(Calendar.DATE, 3);

		} else if (term.equals("SW")) {
			cal.add(Calendar.WEDNESDAY, 1);

		} else if (term.equals("2W")) {
			cal.add(Calendar.WEDNESDAY, 2);

		} else if (term.equals("3W")) {
			cal.add(Calendar.WEDNESDAY, 3);

		} else if (term.equals("1M")) {
			cal.add(Calendar.MONTH, 1);

		} else if (term.equals("2M")) {
			cal.add(Calendar.MONTH, 2);

		} else if (term.equals("3M")) {
			cal.add(Calendar.MONTH, 3);

		} else if (term.equals("4M")) {
			cal.add(Calendar.MONTH, 4);

		} else if (term.equals("5M")) {
			cal.add(Calendar.MONTH, 5);

		} else if (term.equals("6M")) {
			cal.add(Calendar.MONTH, 6);

		} else if (term.equals("7M")) {
			cal.add(Calendar.MONTH, 7);

		} else if (term.equals("8M")) {
			cal.add(Calendar.MONTH, 8);

		} else if (term.equals("9M")) {
			cal.add(Calendar.MONTH, 9);

		} else if (term.equals("10M")) {
			cal.add(Calendar.MONTH, 10);

		} else if (term.equals("11M")) {
			cal.add(Calendar.MONTH, 11);

		} else if (term.equals("1Y")) {
			cal.add(Calendar.YEAR, 1);

		} else if (term.equals("13M")) {
			cal.add(Calendar.MONTH, 13);

		} else if (term.equals("14M")) {
			cal.add(Calendar.MONTH, 14);

		} else if (term.equals("15M")) {
			cal.add(Calendar.MONTH, 15);

		} else if (term.equals("16M")) {
			cal.add(Calendar.MONTH, 16);

		} else if (term.equals("17M")) {
			cal.add(Calendar.MONTH, 17);

		} else if (term.equals("18M")) {
			cal.add(Calendar.MONTH, 18);

		} else if (term.equals("19M")) {
			cal.add(Calendar.MONTH, 19);

		} else if (term.equals("20M")) {
			cal.add(Calendar.MONTH, 20);

		} else if (term.equals("21M")) {
			cal.add(Calendar.MONTH, 21);

		} else if (term.equals("22M")) {
			cal.add(Calendar.MONTH, 22);

		} else if (term.equals("23M")) {
			cal.add(Calendar.MONTH, 23);

		} else if (term.equals("2Y")) {
			cal.add(Calendar.YEAR, 2);

		} else if (term.equals("3Y")) {
			cal.add(Calendar.YEAR, 3);

		} else if (term.equals("4Y")) {
			cal.add(Calendar.YEAR, 4);

		} else if (term.equals("5Y")) {
			cal.add(Calendar.YEAR, 5);

		}
		ValueDay = sdf.format(cal.getTime()).toString();
		if ((compareDate(ValueDay, getLastDayOfMonth(ValueDay, ccy1, ccy2),
				"yyyyMMdd") == "00"
				|| compareDate(ValueDay,
						getLastDayOfMonth(ValueDay, ccy1, ccy2), "yyyyMMdd") == "20" || CurryDay
				.equals(getLastDayOfMonth(CurryDay, ccy1, ccy2)))
				&& (term.indexOf("M") == 1 || term.indexOf("Y") == 1)) {
			ValueDay = getLastDayOfMonth(ValueDay, ccy1, ccy2);
			Date date1 = sdf.parse(ValueDay);
			cal.setTime(date1);
		}
		for (int i = 0; i < holidayList.size(); i++) {
			int weekFlag = cal.get(Calendar.DAY_OF_WEEK);

			String weekend = "";
			if (weekFlag == 7) {
				weekend = "SAT";
			} else if (weekFlag == 1) {
				weekend = "SUN";
			}
			if (weekend.equals(holidayList.get(i).get(ccy1))
					|| weekend.equals(holidayList.get(i).get(ccy2))
					|| ValueDay.equals(holidayList.get(i).get(ccy1))
					|| ValueDay.equals(holidayList.get(i).get(ccy2))
					|| weekend.equals(holidayList.get(i).get("USD"))
					|| ValueDay.equals(holidayList.get(i).get("USD"))) {
				if (ValueDay.equals(getLastDayOfMonth(ValueDay, ccy1, ccy2))
						&& (term.indexOf("M") == 1 || term.indexOf("Y") == 1)) {
					ValueDay = getLastValueDay(ValueDay, ccy1, ccy2);
					break;
				}
				cal.add(Calendar.DATE, 1);
				ValueDay = sdf.format(cal.getTime()).toString();
				i = 0;
			}
		}

		return ValueDay;
	}

	/**
	 * 获取ON到期日
	 * 
	 * @param CurryDay 当前日期
	 *            
	 * 
	 * @param ccy1 币种1
	 *            
	 * 
	 * @param ccy2 币种2
	 *            
	 * 
	 * @return CurryDay 到期日
	 */
	public String getONDay(String CurryDay, String ccy1, String ccy2)
			throws ParseException {
		Date date = sdf.parse(CurryDay);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		CurryDay = sdf.format(cal.getTime()).toString();

		for (int i = 0; i < holidayList.size(); i++) {
			int weekFlag = cal.get(Calendar.DAY_OF_WEEK);

			String weekend = "";
			if (weekFlag == 7) {
				weekend = "SAT";
			} else if (weekFlag == 1) {
				weekend = "SUN";
			}
			if (weekend.equals(holidayList.get(i).get(ccy1))
					|| weekend.equals(holidayList.get(i).get(ccy2))
					|| CurryDay.equals(holidayList.get(i).get(ccy1))
					|| CurryDay.equals(holidayList.get(i).get(ccy2))
					|| weekend.equals(holidayList.get(i).get("USD"))
					|| CurryDay.equals(holidayList.get(i).get("USD"))) {

				cal.add(Calendar.DATE, 1);
				CurryDay = sdf.format(cal.getTime()).toString();
				i = 0;
			}

		}

		return CurryDay;
	}

	/**
	 * 获取下一个营业日
	 * 
	 * @param CurryDay  当前日期
	 *           
	 * @param ccy1 币种1
	 *            
	 * @param ccy2 币种2
	 *            
	 * @return CurryDay 到期日
	 */
	public String getNextDay(String CurryDay, String ccy1, String ccy2)
			throws ParseException {
		// TODO Auto-generated method stub
		Date date = sdf.parse(CurryDay);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		CurryDay = sdf.format(cal.getTime()).toString();

		for (int i = 0; i < holidayList.size(); i++) {
			int weekFlag = cal.get(Calendar.DAY_OF_WEEK);

			String weekend = "";
			if (weekFlag == 7) {
				weekend = "SAT";
			} else if (weekFlag == 1) {
				weekend = "SUN";
			}
			if (weekend.equals(holidayList.get(i).get(ccy1))
					|| weekend.equals(holidayList.get(i).get(ccy2))
					|| CurryDay.equals(holidayList.get(i).get(ccy1))
					|| CurryDay.equals(holidayList.get(i).get(ccy2))
					|| weekend.equals(holidayList.get(i).get("USD"))
					|| CurryDay.equals(holidayList.get(i).get("USD"))) {

				cal.add(Calendar.DATE, 1);
				CurryDay = sdf.format(cal.getTime()).toString();
				i = 0;
			}
		}
		return CurryDay;
	}

	/**
	 * 获取TN的下一个营业日
	 * 
	 * @param CurryDay  当前日期
	 *           
	 * @param ccy1 币种1
	 *            
	 * @param ccy2  币种2
	 *           
	 * @return CurryDay 到期日
	 */
	public String getNextDayForTN(String CurryDay, String ccy1,
			String ccy2) throws ParseException {
		// TODO Auto-generated method stub
		Date date = sdf.parse(CurryDay);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		CurryDay = sdf.format(cal.getTime()).toString();

		for (int i = 0; i < holidayList.size(); i++) {
			int weekFlag = cal.get(Calendar.DAY_OF_WEEK);

			String weekend = "";
			if (weekFlag == 7) {
				weekend = "SAT";
			} else if (weekFlag == 1) {
				weekend = "SUN";
			}
			if (weekend.equals(holidayList.get(i).get(ccy1))
					|| weekend.equals(holidayList.get(i).get(ccy2))
					|| CurryDay.equals(holidayList.get(i).get(ccy1))
					|| CurryDay.equals(holidayList.get(i).get(ccy2))
					|| weekend.equals(holidayList.get(i).get("USD"))) {

				cal.add(Calendar.DATE, 1);
				CurryDay = sdf.format(cal.getTime()).toString();
				i = 0;
			}
		}
		return CurryDay;
	}

	/**
	 * 获取TN，SPOT的营业日
	 * 
	 * @param CurryDay  当前日期
	 *           
	 * @param ccy1  币种1
	 *            
	 * @param ccy2 币种2
	 *            
	 * @return Datemap 到期日
	 */
	public HashMap<String, String> getDates(String CurryDay,
			String ccy1, String ccy2) throws ParseException {
		HashMap<String, String> Datemap = new HashMap<String, String>();
		Date date = sdf.parse(CurryDay);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		String TNDate = sdf.format(cal.getTime()).toString();
		int weekFlag = 0;
		String SPOTDate = "";
		String ccyOther = "";

		if ("USD".equals(ccy1) && "CAD".equals(ccy2)) {
			TNDate = "--";
			SPOTDate = getNextDay(CurryDay, ccy1, ccy2);
		} else if ("USD".equals(ccy1) || "USD".equals(ccy2)) {
			if ("USD".equals(ccy1)) {
				ccyOther = ccy2;
			} else {
				ccyOther = ccy1;
			}
			for (int i = 0; i < holidayList.size(); i++) {
				weekFlag = cal.get(Calendar.DAY_OF_WEEK);
				if (weekFlag == 1 || weekFlag == 7
						|| TNDate.equals(holidayList.get(i).get(ccyOther))) {
					cal.add(Calendar.DATE, 1);
					TNDate = sdf.format(cal.getTime()).toString();
					i = 0;
				}
			}
			SPOTDate = getNextDay(TNDate, ccy1, ccy2);
			for (int i = 0; i < holidayList.size(); i++) {
				if (TNDate.equals(holidayList.get(i).get("USD"))) {
					TNDate = "--";
					break;
				}
			}
		} else {
			TNDate = getNextDayForTN(CurryDay, ccy1, ccy2);
			SPOTDate = getNextDay(TNDate, ccy1, ccy2);
			for (int i = 0; i < holidayList.size(); i++) {
				if (TNDate.equals(holidayList.get(i).get("USD"))) {
					TNDate = "--";
					break;
				}
			}
		}
		Datemap.put("TNDate", TNDate);
		Datemap.put("SPOTDate", SPOTDate);
		return Datemap;
	}

	/**
	 * 获取该月最后一天的营业日
	 * 
	 * @param CurryDay 当前日期
	 *            
	 * @param ccy1 币种1
	 *           
	 * @param ccy2 币种2
	 *            
	 * @return lastValueDay 该月最后一天的营业日
	 */
	public  String  getLastDayOfMonth(String CurryDay,String ccy1,String ccy2) throws java.text.ParseException {
		 Date date =sdf.parse(CurryDay);
		Calendar cDay = Calendar.getInstance();
		cDay.setTime(date);
		cDay.set(Calendar.DAY_OF_MONTH, cDay.getActualMaximum(Calendar.DAY_OF_MONTH));
		String lastValueDay = getLastValueDay(sdf.format(cDay.getTime()).toString(),ccy1,ccy2);
		return lastValueDay;
	}
	
	
	/**
	 * 获取上一个的营业日
	 * 
	 * @param CurryDay  当前日期
	 *           
	 * @param ccy1 币种1
	 *            
	 * @param ccy2 币种2
	 *            
	 * @return 上一个营业日
	 */
	public String getLastValueDay(String CurryDay, String ccy1,
			String ccy2) throws java.text.ParseException {
		Date date = sdf.parse(CurryDay);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal
				.getActualMaximum(Calendar.DAY_OF_MONTH));

		for (int i = 0; i < holidayList.size(); i++) {
			int weekFlag = cal.get(Calendar.DAY_OF_WEEK);

			String weekend = "";
			if (weekFlag == 7) {
				weekend = "SAT";
			} else if (weekFlag == 1) {
				weekend = "SUN";
			}
			if (weekend.equals(holidayList.get(i).get(ccy1))
					|| weekend.equals(holidayList.get(i).get(ccy2))
					|| CurryDay.equals(holidayList.get(i).get(ccy1))
					|| CurryDay.equals(holidayList.get(i).get(ccy2))
					|| weekend.equals(holidayList.get(i).get("USD"))
					|| CurryDay.equals(holidayList.get(i).get("USD"))) {
				cal.add(Calendar.DATE, -1);
				CurryDay = sdf.format(cal.getTime()).toString();
				i = 0;
			}
		}
		return sdf.format(cal.getTime()).toString();
	}

	/**
	 * 是否节假日
	 * 
	 * @param CurryDay
	 *            当前日期
	 * @param ccy1
	 *            币种1
	 * @param ccy2
	 *            币种2
	 * @return falg 是否节假日
	 */
	public boolean checkHldy(String CurryDay, String ccy1, String ccy2)
			throws java.text.ParseException {
		boolean falg = false;
		Date date = sdf.parse(CurryDay);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal
				.getActualMaximum(Calendar.DAY_OF_MONTH));

		for (int i = 0; i < holidayList.size(); i++) {
			int weekFlag = cal.get(Calendar.DAY_OF_WEEK);

			String weekend = "";
			if (weekFlag == 7) {
				weekend = "SAT";
			} else if (weekFlag == 1) {
				weekend = "SUN";
			}
			if (weekend.equals(holidayList.get(i).get(ccy1))
					|| weekend.equals(holidayList.get(i).get(ccy2))
					|| CurryDay.equals(holidayList.get(i).get(ccy1))
					|| CurryDay.equals(holidayList.get(i).get(ccy2))
					|| weekend.equals(holidayList.get(i).get("USD"))
					|| CurryDay.equals(holidayList.get(i).get("USD"))) {
				falg = true;
				break;
			}
		}
		return falg;
	}

	/**
	 * 比较日期大小
	 * 
	 * @param date1
	 *            日期1
	 * @param date2
	 *            日期2
	 * @param format
	 *            日期格式
	 * @return 返回码
	 */
	public static String compareDate(String date1, String date2, String format)
			throws ParseException {
		Integer compare = date1.compareTo(date2);
		if (compare != null) {
			switch (compare.intValue()) {
			case 0:// 一样大
				return "20";
			case 1:// date1大
				return "00";
			case -1:// date2大
				return "10";
			default:// 错误
				return "99";
			}
		} else {
			return "99";
		}
	}

	/**
	 * 增加一年
	 * 
	 * @param startDate
	 *            开始日期
	 * @return 返回日期
	 */
	public  String AddOneMonth(String startDate) {
		Date date = new Date();
		try {
			date = sdf.parse(startDate);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, 1);
		return sdf.format(cal.getTime()).toString();
	}
}
