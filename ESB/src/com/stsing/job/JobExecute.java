package com.stsing.job;

/**
 * 
 * @author k1193
 *
 */
public interface JobExecute {

	/**
	 * 
	 * @return 执行的结果
	 */
	public String execute();
}
