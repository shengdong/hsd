package com.stsing.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.client.SocketClient;
import com.stsing.jedis.JedisClusterUtil;
import com.stsing.jedis.JedisSingleUtil;
import com.stsing.jedis.JedisUtil;
import com.stsing.symbol.ConsumerSeqNoUtil;
import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.GsonFactory;

/**
 * 
 * @author k1193
 * 
 */
public class JobC491 implements JobExecute {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JobC491.class);

	/**
	 * 定义要发送的货币
	 */
	private String ccys[] = null;

	/**
	 * 定义DBUTIL 对象
	 */
	private DBUtil dbUtil = null;

    /**
     * 定义期限的范围
     */
	private static String bors[] = null;

	/**
	 * 构造函数,初始化货币对
	 */
	public JobC491() {
		ccys = ConfigUtil.getString("esb.c491ccy").split(",");
		dbUtil = new DBUtil();
		bors = ConfigUtil.getString("esb.c491bors").split(",");
	}

//	@Override
//	public String execute() {
//		String ip = ConfigUtil.getString("esb.ip");
//		int port = ConfigUtil.getInt("esb.port");
//		int timeout = ConfigUtil.getInt("esb.timeout");
//		Jedis jedis = JedisSingleUtil.getInstance().getJedis();
//		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
//
//		List<Map<String, String>> dataFromRedisOrOracle = new ArrayList<Map<String, String>>();
//
//		if (jedis != null) { // 如果redis 没有停,那么数据从redis上获取
//			logger.info("get data from redis");
//			for (String bor : bors) {
//				Set<String> liborLists = jedis.smembers(bor);
//				List<Response<Map<String, String>>> rsp = new ArrayList<Response<Map<String, String>>>(
//						liborLists.size());
//				Pipeline pipeline = jedis.pipelined();
//				for (String key : liborLists) {
//					rsp.add(pipeline.hgetAll(key));
//				}
//				pipeline.sync();
//				for (Response<Map<String, String>> rs : rsp) {
//					dataFromRedisOrOracle.add(rs.get());
//				}
//			}
//			String sgdSibors = ConfigUtil.getString("esb.SGDSIBOR");
//			Set<String> sgdSibor = jedis.smembers(sgdSibors);
//			for (String key : sgdSibor) {
//				key = "/" + key;
//				Map<String, String> map = jedis.hgetAll(key);
//				map.put("tenor", map.get("symbol").substring(5, 7));
//				dataFromRedisOrOracle.add(map);
//			}
//
//			if(jedis != null) {
//				jedis.close();
//			}
//		} else { // 如果redis 停止了，那么数据从oracle获取
//			logger.info("get data from oracle");
//			dataFromRedisOrOracle = getDataFromOracle();
//		}
//		
//		for(Map<String, String> p : dataFromRedisOrOracle) {
//			String ric = p.get("ric");
//			if(StringUtils.contains(ric, "SGD")) {
//				p.put("tenor", p.get("symbol").substring(5, 7));
//			}
//		}
//		for (String ccy : ccys) {
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("ccy", ccy);
//			for (Map<String, String> m : dataFromRedisOrOracle) {
//				if (m != null && m.size() > 0) {
//					if (m.get("ric") != null
//							&& StringUtils.contains(m.get("ric"), ccy)
//							&& !StringUtils.contains(m.get("ric"), "365")) {
//						map.put(m.get("tenor").trim(), m.get("primact_1"));
//					}
//				}
//			}
//			if (map.size() != 1) {
//				result.add(map);
//			}
//		}
//		String getMessage = getMessage(result);
//		return SocketClient.sendToESB(ip, port, timeout, getMessage);
//	}
	
	@Override
	public String execute() {
		String ip = ConfigUtil.getString("esb.ip");
		int port = ConfigUtil.getInt("esb.port");
		int timeout = ConfigUtil.getInt("esb.timeout");
		String flag = ConfigUtil.getString("redis.flag");
		JedisUtil jedis = null;
		if(StringUtils.equals(flag, JedisUtil.CLUSTER)) {
			jedis = new JedisClusterUtil();
		} else if (StringUtils.equals(flag, JedisUtil.JEDIS)) {
			jedis = new JedisSingleUtil();
		}
		
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();

		List<Map<String, String>> dataFromRedisOrOracle = new ArrayList<Map<String, String>>();

		if (jedis != null && jedis.ping() ) { // 如果redis 没有停,那么数据从redis上获取
			logger.info("get data from redis");
			for (String bor : bors) {
				Set<String> liborLists = jedis.smembers(bor);
				List<Map<String, String>> lists = jedis.piplined(liborLists);
				dataFromRedisOrOracle.addAll(lists);
			}
			String sgdSibors = ConfigUtil.getString("esb.SGDSIBOR");
			Set<String> sgdSibor = jedis.smembers(sgdSibors);
			for (String key : sgdSibor) {
				String key1 = "/" + key;
				Map<String, String> map = jedis.hgetAll(key1);
				map.put("tenor", key1.substring(5, 7));
				dataFromRedisOrOracle.add(map);
			}

			
		} else { // 如果redis 停止了，那么数据从oracle获取
			logger.info("get data from oracle");
			dataFromRedisOrOracle = getDataFromOracle();
		}
		
		if(jedis != null) {
			jedis.close();
		}
		
		for(Map<String, String> p : dataFromRedisOrOracle) {
			String ric = p.get("ric");
			if(StringUtils.contains(ric, "SGD")) {
				p.put("tenor", p.get("ric").substring(6, 8));
			}
		}
		for (String ccy : ccys) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("ccy", ccy);
			for (Map<String, String> m : dataFromRedisOrOracle) {
				if (m != null && m.size() > 0) {
					if (m.get("ric") != null
							&& StringUtils.contains(m.get("ric"), ccy)
							&& !StringUtils.contains(m.get("ric"), "365")) {
						map.put(m.get("tenor").trim(), m.get("primact_1"));
					}
				}
			}
			if (map.size() != 1) {
				result.add(map);
			}
		}
		String getMessage = getMessage(result);
		return SocketClient.sendToESB(ip, port, timeout, getMessage);
	}

	/**
	 * 
	 * @param lists 要发送的数据
	 *
	 * @return message of list
	 */
	private String getMessage(List<Map<String, String>> lists) {
		String consumerSeqNo = ConsumerSeqNoUtil.getConsumerSeqNo();
		String consumerId = consumerSeqNo.substring(consumerSeqNo.length() - 8,
				consumerSeqNo.length());
		String tranDate = ConfigUtil.getToday("yyyy-MM-dd");
		String tranTime = ConfigUtil.getToday("HH:mm:ss");

		String body = "";
		int count = 0;
		for (Map<String, String> map : lists) {
			boolean flag = false;
			String ccyBody = "<SbmtInf><Ccy>" + ((String) map.get("ccy")) + "</Ccy>";
			if (!(StringUtils.equals(getData((String) map.get("ON")), "0"))) {
				ccyBody = ccyBody + "<OvngtIntRate>" + getData((String) map.get("ON")) + "</OvngtIntRate>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((map.get("SW") == null) ? (String) map.get("1W") : (String) map.get("SW")), "0"))) {
				ccyBody = ccyBody + "<IntRate1Wk>" + getData((map.get("SW") == null) ? (String) map.get("1W") : (String) map.get("SW")) + "</IntRate1Wk>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("2W")), "0"))) {
				ccyBody = ccyBody + "<IntRate2Wk>"+ getData((String) map.get("2W")) + "</IntRate2Wk>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("1M")), "0"))) {
				ccyBody = ccyBody + "<IntRate1Mo>" + getData((String) map.get("1M")) + "</IntRate1Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("2M")), "0"))) {
				ccyBody = ccyBody + "<IntRate2Mo>" + getData((String) map.get("2M")) + "</IntRate2Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("3M")), "0"))) {
				ccyBody = ccyBody + "<IntRate3Mo>"
						+ getData((String) map.get("3M")) + "</IntRate3Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("4M")), "0"))) {
				ccyBody = ccyBody + "<IntRate4Mo>" + getData((String) map.get("4M")) + "</IntRate4Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("5M")), "0"))) {
				ccyBody = ccyBody + "<IntRate5Mo>" + getData((String) map.get("5M")) + "</IntRate5Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("6M")), "0"))) {
				ccyBody = ccyBody + "<IntRate6Mo>" + getData((String) map.get("6M")) + "</IntRate6Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("7M")), "0"))) {
				ccyBody = ccyBody + "<IntRate7Mo>" + getData((String) map.get("7M")) + "</IntRate7Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("8M")), "0"))) {
				ccyBody = ccyBody + "<IntRate8Mo>" + getData((String) map.get("8M")) + "</IntRate8Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("9M")), "0"))) {
				ccyBody = ccyBody + "<IntRate9Mo>"
						+ getData((String) map.get("9M")) + "</IntRate9Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("10M")), "0"))) {
				ccyBody = ccyBody + "<IntRate10Mo>" + getData((String) map.get("10M")) + "</IntRate10Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("11M")), "0"))) {
				ccyBody = ccyBody + "<IntRate11Mo>" + getData((String) map.get("11M")) + "</IntRate11Mo>";
				flag = true;
			}
			if (!(StringUtils.equals(getData((String) map.get("1Y")), "0"))) {
				ccyBody = ccyBody + "<IntRate1Yr>" + getData((String) map.get("1Y")) + "</IntRate1Yr>";
				flag = true;
			}
			ccyBody = ccyBody + "</SbmtInf>";

			if (flag) {
				body = body + ccyBody;
				++count;
			}
			
			
		}

		String footer = "</array></BODY></service>";
		String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><service>" +
				"<LOCAL_HEAD><TranCode>C491</TranCode></LOCAL_HEAD><SYS_HEAD><ServiceCode>06002000003" +
				"</ServiceCode><ServiceScene>01</ServiceScene><ConsumerSeqNo>"
				+ consumerSeqNo
				+ "</ConsumerSeqNo>"
				+ "<ConsumerId>"
				+ consumerId
				+ "</ConsumerId>"
				+ "<TranDate>"
				+ tranDate
				+ "</TranDate>"
				+ "<TranTime>"
				+ tranTime
				+ "</TranTime></SYS_HEAD>"
				+ "<APP_HEAD>"
				+ "<TranTellerNo>999999</TranTellerNo>"
				+ "</APP_HEAD>"
				+ "<BODY>"
				+ "<TellerNo>999</TellerNo>"
				+ "<BranchId>99999</BranchId>"
				+ "<CnlInd>HQ</CnlInd>"
				+ "<Tms>" + count + "</Tms>" + "<array>";
		return header + body + footer;
	}

	/**
	 *
	 * @param data 要转换的double数据
	 *
	 * @return 特定格式的double
	 */
	private String getData(String data) {
		if ((StringUtils.isBlank(data))
				|| (StringUtils.equals(data.trim(), "0"))) {
			return "0";
		}

		String result = ConfigUtil.getDouble(Double.parseDouble(data),
				"00.000000");
		if (!(StringUtils.contains(result, "-"))) {
			result = "+" + result;
		}

		result = result.replace(".", "");

		if (StringUtils.contains(result, "00000000")) {
			return "0";
		}
		return result;
	}

	/**
	 * 如果redis is shutdown, get data from oracle
	 * 
	 * @return message from oracle
	 */
	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getDataFromOracle() {
		String borContain = "";
		for (String bor : bors) {
			borContain = borContain + "'" + bor + "',";
		}
		String sgdSibors = ConfigUtil.getString("esb.SGDSIBOR");
		borContain = borContain + "'" + sgdSibors + "'";
		
		String sql = "select * from redis_cache t where t.redis_key in(" + borContain + ")";
		
		String rics = ""; 
		List<Map<String, Object>> lists = dbUtil.getDataFromDB(sql);
		for (Map<String, Object> map : lists) {
			rics = rics + (String) map.get("REDIS_VALUES") + ",";
		}
		rics = rics.substring(0, rics.length() - 1);
		String ricArray[] = rics.split(",");
		String ricContain = "";
		String ric1 = "";
		for (String ric : ricArray) {
			if(StringUtils.contains(ric, "SISGD")) {
				ric1 =  "/" + ric; 
			}
			ricContain = ricContain + "'" + ric1 + "',";
		}
		
		ricContain = ricContain.substring(0, ricContain.length() - 1);
		sql = "select * from redis_cache t where t.redis_key in(" + ricContain + ")";

        logger.info("sql:" + sql);
		List<Map<String, Object>> dataLists = dbUtil.getDataFromDB(sql);

		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for (Map<String, Object> map : dataLists) {
			result.add(GsonFactory.getGson().fromJson(
					(String) map.get("REDIS_VALUES"), Map.class));
		}
		logger.info(result.size());

		return result;
	}
}
