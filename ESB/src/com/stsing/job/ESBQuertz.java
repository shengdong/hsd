package com.stsing.job;

/**
 * esb 的 quertz
 * @author k1193
 *
 */
public class ESBQuertz {

	/**
	 * job 的执行类名字
	 */
	private String className;
	
	/**
	 * job的名称
	 */
	private String jobName;
	
	/**
	 * 触发器的名称
	 */
	private String triggerName;
	
	/**
	 * 触发器的时间
	 */
	private String triggerTime;

	/**
	 * 
	 * @return 执行类的名称
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * 设置执行类的名称
	 * @param className 类名称
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * 
	 * @return 返回任务的名称
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * 
	 * @param jobName job的名称
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * 
	 * @return 返回触发器的名称
	 */
	public String getTriggerName() {
		return triggerName;
	}

	/**
	 * 
	 * @param triggerName 触发器的名称
	 */
	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	/**
	 * 
	 * @return 返回触发器的时间
	 */
	public String getTriggerTime() {
		return triggerTime;
	}

	/**
	 * 
	 * @param triggerTime 设置触发器的名称
	 */
	public void setTriggerTime(String triggerTime) {
		this.triggerTime = triggerTime;
	}

	@Override
	public String toString() {
		return "ESBQuertz [className=" + className + ", jobName=" + jobName
				+ ", triggerName=" + triggerName + ", triggerTime="
				+ triggerTime + "]";
	}
	
	
}
