package com.stsing.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 
 * @author k1193
 *
 */
public class JobValueDate implements Job {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JobValueDate.class);


	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getTrigger().getJobKey().getName();
		logger.info("jobName : " + jobName);
		String result = JobDisPatcherHandler.executeJob(jobName);
		logger.info("result : " + result);
	}

}
