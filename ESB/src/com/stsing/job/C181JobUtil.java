package com.stsing.job;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.stsing.fixclient.PriceObject;
import com.stsing.jedis.JedisClusterUtil;
import com.stsing.jedis.JedisSingleUtil;
import com.stsing.jedis.JedisUtil;
import com.stsing.symbol.ConsumerSeqNoUtil;
import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.GsonFactory;

/**
 * 
 * @author k1193
 *
 */
public class C181JobUtil {
	/**
	 * declare dbutil
	 */
	private static DBUtil dbUtil = null;
	
	static {
		dbUtil = new DBUtil();
	}
	
	/**
	 * 
	 * @param key the ric
	 * @return result from oracle
	 */
	@SuppressWarnings("unchecked")
	private static Map<String, String> getOracle(String key) {
		String sql = "select t.redis_values from redis_cache t where t.redis_key = '" + key + "'";
		Map<String, Object> map = dbUtil.getDataFromDB(sql).get(0);
		String redis_values = (String)map.get("REDIS_VALUE");
		return GsonFactory.getGson().fromJson(redis_values, Map.class);
	}
	
	/**
	 * 
	 * @param key the ric
	 * @param symbol the symbol
	 * @return message from the ric and the symbol
	 */
	public static String getMessage(String key, String symbol) {
	
		String flag = ConfigUtil.getString("redis.flag");
		JedisUtil jedis = null;
		if(StringUtils.equals(flag, JedisUtil.CLUSTER)) {
			jedis = new JedisClusterUtil();
		} else if (StringUtils.equals(flag, JedisUtil.JEDIS)) {
			jedis = new JedisSingleUtil();
		}
		Map<String, String> map = new HashMap<String, String>();
		double cnyPrice = 0;
		String ricOfCnyPrice = ConfigUtil.getString("esb.ricOfCnyPrice");
		
		if(jedis != null && jedis.ping()) {
			map = jedis.hgetAll(key);
			cnyPrice = Double.parseDouble(jedis.hgetAll(ricOfCnyPrice).get("ask"));
			
		} else {
			map = getOracle(key);
			cnyPrice = Double.parseDouble(getOracle(ricOfCnyPrice).get("ask"));
		}
		PriceObject object = new PriceObject();
		
		String sendTime = ConfigUtil.getToday("HH:mm:ss.sss");
		String ask = getPrice(cnyPrice, Double.parseDouble(map.get("ask")));
		object.setSymbol(symbol);
		object.setAskCash(ask);
		object.setBidCash(ask);
		object.setBidClient(ask);
		object.setAsktime(sendTime);
		object.setBidtime(sendTime);
		return getMessage(object);
	}
	
	/**
	 * 
	 * @param cnyPrice cny price
	 * @param ask ask price
	 * @return the price
	 */
	private static String getPrice(double cnyPrice, double ask) {
		DecimalFormat format = new DecimalFormat("#.0000");
		return format.format(cnyPrice * ask);
	}
	
	/**
	 * 
	 * @param key the ric
	 * @return message of ric
	 */
	public static String getMessage(String key) {
		String flag = ConfigUtil.getString("redis.flag");
		JedisUtil jedis = null;
		if(StringUtils.equals(flag, JedisUtil.CLUSTER)) {
			jedis = new JedisClusterUtil();
		} else if (StringUtils.equals(flag, JedisUtil.JEDIS)) {
			jedis = new JedisSingleUtil();
		}
		Map<String, String> map = new HashMap<String, String>();
		
		if(jedis != null && jedis.ping()) {
			map = jedis.hgetAll(key);
		} else {
			map = getOracle(key);
		}
		PriceObject object = new PriceObject();
		object.setSymbol(map.get("symbol"));
		object.setBidCash(map.get("bidCash"));
		object.setBidClient(map.get("bidClient"));
		object.setBidtime(map.get("bidtime"));
		object.setAsktime(map.get("asktime"));
		object.setAskCash(map.get("askCash"));
		return getMessage(object);
	}
	
	/**
	 * 
	 * @param object priceObject
	 * @return message
	 */
	public static String getMessage(PriceObject object) {

		String consumerSeqNo = ConsumerSeqNoUtil.getConsumerSeqNo();
		String consumerId = consumerSeqNo.substring(consumerSeqNo.length() - 8,
				consumerSeqNo.length());
		String tranDate = ConfigUtil.getToday("yyyy-MM-dd");
		String tranDate_body = ConfigUtil.getToday("yyyyMMdd");
		String tranTime = ConfigUtil.getToday("HH:mm:ss");
		String tranTime_body = tranTime.replaceAll(":", "");
		String Ccy = object.getSymbol().substring(0, 3);
		String type = "C181";
		
		String result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><service><LOCAL_HEAD><TranCode>C181" +
				"</TranCode></LOCAL_HEAD>"
				+ "<SYS_HEAD><ServiceCode>07002000004</ServiceCode><ServiceScene>01</ServiceScene><ConsumerSeqNo>"
				+ consumerSeqNo
				+ "</ConsumerSeqNo>"
				+ "<ConsumerId>"
				+ consumerId
				+ "</ConsumerId>"
				+ "<TranDate>"
				+ tranDate
				+ "</TranDate>"
				+ "<TranTime>"
				+ tranTime
				+ "</TranTime></SYS_HEAD>"
				+ "<APP_HEAD>"
				+ "<TranTellerNo>9999</TranTellerNo>"
				+ "</APP_HEAD>"
				+ "<BODY>"
				+ "<Ccy>"
				+ Ccy
				+ "</Ccy>"
				+ "<QtnTm>"
				+ tranTime_body
				+ "</QtnTm>"
				+ "<QtnDt>"
				+ tranDate_body
				+ "</QtnDt>"
				+ "<BranchId>99999</BranchId>"
				+ "<QtnTp>"
				+ type
				+ "</QtnTp>"
				+ "<Tms>1</Tms>"
				+ "<array><SbmtInf>"
				+ "<SellPrc>"
				+ formatPrice(object.getAskCash(), object.getSymbol())
				+ "</SellPrc>"
				+ "<ASKPrcTm>"
				+ getPrcVldTm(object.getAsktime())
				+ "</ASKPrcTm>"
				+ "<CashBuyPrc>"
				+ formatPrice(object.getBidCash(), object.getSymbol())
				+ "</CashBuyPrc>"
				+ "<ExgBuyPrc>"
				+ formatPrice(object.getBidClient(), object.getSymbol())
				+ "</ExgBuyPrc>"
				+ "<BINPrcTm>"
				+ getPrcVldTm(object.getBidtime())
				+ "</BINPrcTm>"
				+ "</SbmtInf></array></BODY></service>";

		return result;
	}
	
	/**
	 * 
	 * @param price the price
	 * @param symbol the symbol
	 * @return the message of price and symbol
	 */
	 private static String formatPrice(String price, String symbol) {
	    	if(StringUtils.contains(symbol, "JPY")) {
	    		DecimalFormat format = new DecimalFormat("000000000000.0000");
	    		return format.format(Double.parseDouble(price));
	    		
	    	} else if(StringUtils.contains(symbol, "HKD")) { 
	    		DecimalFormat format = new DecimalFormat("0000000000000.000");
	    		return format.format(Double.parseDouble(price) * 100);
	    	} else if(StringUtils.contains(symbol, "XAU") || StringUtils.contains(symbol, "XAG")) {
	    		DecimalFormat format = new DecimalFormat("000000000000.0000");
	    		return format.format(Double.parseDouble(price));
	    	}
	    	
	    	DecimalFormat format = new DecimalFormat("00000000000000.00");
	    	return format.format(Double.parseDouble(price) * 100);
	      
	    }
	 
	 /**
		 * 
		 * @param time 在字符串中拿到数字
		 *
		 * @return the time
		 */
		private static String getPrcVldTm(String time) {
			if (StringUtils.isBlank(time)) {
				return "";
			}
			String str = "";

			for (int i = 0; i < time.length(); i++) {
				if (time.charAt(i) >= 48 && time.charAt(i) <= 57) {
					str = str + time.charAt(i);
				}
			}
			return ConfigUtil.getToday("yyyyMMdd") + str;
		}
}
