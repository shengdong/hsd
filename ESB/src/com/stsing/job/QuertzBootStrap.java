package com.stsing.job;

import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.Scheduler;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import com.stsing.util.DBUtil;

/**
 * 
 * @author k1193
 *
 */
public class QuertzBootStrap {
	/**
	 * declare logger 
	 */
	private static final Logger logger = Logger.getLogger(QuertzBootStrap.class);

	/**
	 * declare scheuler object
	 */
	private Scheduler scheduler = null;

	/**
	 * declare dbutil object
	 */
	private DBUtil dbUtil = null;
	

	/**
	 * 构造函数，初始化scheuler对象，dbutil对象
	 */
	public QuertzBootStrap() {
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			dbUtil = new DBUtil();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * start quertz
	 */
	@SuppressWarnings("unchecked")
	public void run() {
		List<ESBQuertz> lists = dbUtil.getQuertz();
		
		logger.info("lists : " + lists);
		if (lists == null || lists.size() == 0) {
			logger.error("ESB quertz is null, check out dataBase");
			return;
		}
		try {
			for (ESBQuertz quertz : lists) {
				
				String className = quertz.getClassName().trim();
				String jobName = quertz.getJobName().trim();
				String triggerName = quertz.getTriggerName().trim();
				String triggerTime = quertz.getTriggerTime().trim();

				Class<Job> classJob = (Class<Job>) Class.forName(className);
				scheduler.scheduleJob(JobBuilder.newJob(classJob).withIdentity(
						jobName).build(), TriggerBuilder.newTrigger()
						.withIdentity(triggerName).withSchedule(
								CronScheduleBuilder.cronSchedule(triggerTime))
						.startNow().build());
			}
			
			scheduler.start();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * stop quertz
	 */
	public void stop() {
		try {
			scheduler.shutdown();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
