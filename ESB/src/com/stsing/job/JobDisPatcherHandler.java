package com.stsing.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.util.DBUtil;
import com.stsing.util.ErrorDBHandler;
import com.stsing.util.GsonFactory;
import com.stsing.util.Info;

/**
 * 
 * @author k1193
 *
 */
public class JobDisPatcherHandler {
	
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JobDisPatcherHandler.class);
	
	/**
	 * declare dbutil
	 */
	private static DBUtil dbUtil = null;
	
	
	/**
	 * 
	 * @param jobName jobname
	 * @return message
	 */
	@SuppressWarnings("unchecked")
	public static String executeJob(String jobName) {
		List<Info> list = new ArrayList<Info>(1);
		
		String sql = "select jobClass from esb_quertz where jobname = '" + jobName + "'";
		logger.warn("sql : " + sql);
		if(dbUtil == null) {
			dbUtil = new DBUtil();
		}
		
		String jobClass = "";
		List<Map<String, Object>> lists = dbUtil.getDataFromDB(sql);
		if(lists.isEmpty() || lists.size() == 0) {
			Info info = new Info();
			info.setSuccess(false);
			info.setErrorInfo("jobName is error");
			info.setObject(jobName);
			list.add(info);
			ErrorDBHandler.insertError(list);
			return GsonFactory.getGson().toJson(list);
		} else {
			Map<String, Object> map = lists.get(0);
			jobClass = (String)map.get("JOBCLASS");
		}
		
		if(StringUtils.isBlank(jobClass)) {
			Info info = new Info();
			info.setSuccess(false);
			info.setErrorInfo("oracle data is error, please check esb_quertz");
			info.setObject(jobName);
			list.add(info);
			ErrorDBHandler.insertError(list);
			return GsonFactory.getGson().toJson(list);
		}
		
		try {
			Class<JobExecute> jobClassObject = (Class<JobExecute>) Class.forName(jobClass);
			JobExecute job =  jobClassObject.newInstance();
			String message = job.execute();
			logger.warn("rsp : " + message);
			if(StringUtils.contains(message, "000000")) {
				Info info = new Info();
				info.setSuccess(true);
				info.setObject(jobName);
				list.add(info);
				ErrorDBHandler.insertError(list);
				return GsonFactory.getGson().toJson(list);
			} else {
				Info info = new Info();
				info.setSuccess(false);
				info.setErrorInfo(message.split("ReturnMsg")[1].substring(1,message.split("ReturnMsg")[1].length() - 2));
				info.setObject(jobName);
				list.add(info);
				ErrorDBHandler.insertError(list);
				return GsonFactory.getGson().toJson(list);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Info info = new Info();
			info.setSuccess(false);
			info.setErrorInfo(e.getMessage());
			info.setObject(jobName);
			list.add(info);
			ErrorDBHandler.insertError(list);
			return GsonFactory.getGson().toJson(list);
		}
	}
	
}
