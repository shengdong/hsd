package com.stsing.job;

import org.apache.log4j.Logger;

/**
 * 
 * @author k1193
 *
 */
public class JobValueDateExceute implements JobExecute {
	/**
	 * declare logegr
	 */
	private static final Logger logger = Logger.getLogger(JobValueDateExceute.class);

	
	@Override
	public String execute() {
		try {
			logger.info("begin to strat valuedate...........");
			new ParamHldy().setParamHldy(null);
			logger.info("stop valuedate...........");
			return "000000";
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return "error" + e.getMessage();
		}
	}
}
