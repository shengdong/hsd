package com.stsing.job;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.client.SocketClient;
import com.stsing.handler.TenorHandler;
import com.stsing.jedis.JedisClusterUtil;
import com.stsing.jedis.JedisSingleUtil;
import com.stsing.jedis.JedisUtil;
import com.stsing.jedis.TenorUtil;
import com.stsing.symbol.ConsumerSeqNoUtil;
import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.GsonFactory;

/**
 * CC40的任务
 * 
 * @author k1193
 * 
 */
public class JobCC40 implements JobExecute{
	
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JobCC40.class);

	/**
	 * 定义DButil
	 */
	private DBUtil dbUtil = null;
	
	/**
	 * 构造函数,初始化dbutil的对象
	 */
	public JobCC40() {
		dbUtil = new DBUtil();
	}

	/**
	 * 
	 * @param value 价格对象
	 * @return 如果是null，返回""，否则的话返回字符串类型的
	 */
	private String getData(Object value) {
		if (value == null) {
			return "";
		}
		return (String) value;
	}

	/**
	 * 
	 * @return 返回发送的报文
	 */
	public String getSendMessage() {

        List<Map<String, Object>> list = getResultOfList();
		String ConsumerSeqNo = ConsumerSeqNoUtil.getConsumerSeqNo();
		String ConsumerId = ConsumerSeqNo.substring(ConsumerSeqNo.length() - 8,
				ConsumerSeqNo.length());
		String TranDate = ConfigUtil.getToday("yyyy-MM-dd");
		String TranTime = ConfigUtil.getToday("HH:mm:ss");
		String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><service><LOCAL_HEAD><TranCode>CC40</TranCode></LOCAL_HEAD>"
				+ "<SYS_HEAD><ServiceCode>07002000004</ServiceCode><ServiceScene>02</ServiceScene><ConsumerSeqNo>"
				+ ConsumerSeqNo
				+ "</ConsumerSeqNo>"
				+ "<ConsumerId>"
				+ ConsumerId
				+ "</ConsumerId>"
				+ "<TranDate>"
				+ TranDate
				+ "</TranDate>"
				+ "<TranTime>"
				+ TranTime
				+ "</TranTime></SYS_HEAD>"
				+ "<BODY>"
				+ "<TellerNo>999999</TellerNo>"
				+ "<BranchId>99999</BranchId>"
				+ "<CnlInd>HQ</CnlInd>" + "<array>";

		String body = "";
		for (int i = 0; i < list.size(); i++) {
			Map<String, Object> dataMap = list.get(i);
			body = body + "<SbmtInf>" + "<QtnTp>"
					+ getData(dataMap.get("QtnTp")) + "</QtnTp>" + "<Term>"
					+ getData(dataMap.get("Term")) + "</Term>" + "<ExpDt>"
					+ ConfigUtil.getToday("yyyyMMdd") + "</ExpDt>" + "<USDBuyPrc>"
					+ getData(dataMap.get("USDBuyPrc")) + "</USDBuyPrc>"
					+ "<USDSellPrc>" + getData(dataMap.get("USDSellPrc"))
					+ "</USDSellPrc>" + "<EURBuyPrc>"
					+ getData(dataMap.get("EURBuyPrc")) + "</EURBuyPrc>"
					+ "<EURSellPrc>" + getData(dataMap.get("EURSellPrc"))
					+ "</EURSellPrc>" + "<JPYBuyPrc>"
					+ getData(dataMap.get("JPYBuyPrc")) + "</JPYBuyPrc>"
					+ "<JPYSellPrc>" + getData(dataMap.get("JPYSellPrc"))
					+ "</JPYSellPrc>" + "<HKDBuyPrc>"
					+ getData(dataMap.get("HKDBuyPrc")) + "</HKDBuyPrc>"
					+ "<HKDSellPrc>" + getData(dataMap.get("HKDSellPrc"))
					+ "</HKDSellPrc>" + "<GBPBuyPrc>"
					+ getData(dataMap.get("GBPBuyPrc")) + "</GBPBuyPrc>"
					+ "<GBPSellPrc>" + getData(dataMap.get("GBPSellPrc"))
					+ "</GBPSellPrc>" + "<CHFBuyPrc>"
					+ getData(dataMap.get("CHFBuyPrc")) + "</CHFBuyPrc>"
					+ "<CHFSellPrc>" + getData(dataMap.get("CHFSellPrc"))
					+ "</CHFSellPrc>" + "<CADBuyPrc>"
					+ getData(dataMap.get("CADBuyPrc")) + "</CADBuyPrc>"
					+ "<CADSellPrc>" + getData(dataMap.get("CADSellPrc"))
					+ "</CADSellPrc>" + "<AUDBuyPrc>"
					+ getData(dataMap.get("AUDBuyPrc")) + "</AUDBuyPrc>"
					+ "<AUDSellPrc>" + getData(dataMap.get("AUDSellPrc"))
					+ "</AUDSellPrc>" + "<SGDBuyPrc>"
					+ getData(dataMap.get("SGDBuyPrc")) + "</SGDBuyPrc>"
					+ "<SGDSellPrc>" + getData(dataMap.get("SGDSellPrc"))
					+ "</SGDSellPrc>" + "<ObgtInf></ObgtInf>" + "</SbmtInf>";

		}

		String footer = "</array>" + "</BODY></service>";
		return header + body.replaceAll("SW", "1W") + footer;
	}

//	@SuppressWarnings("unchecked")
//	@Override
//	public String execute(){
//		Jedis jedis = JedisSingleUtil.getInstance().getJedis();
//		List<Map<String, Object>> reslutList = new ArrayList<Map<String, Object>>();
//		String[] tenors = ConfigUtil.getString("esb.cc40tenor").split(",");
//		String sql = "";
//		List<Map<String, String>> lists = new ArrayList<Map<String,String>>();
//		for (String tenor : tenors) {
//			if (StringUtils.isBlank(tenor)) {
//				continue;
//			} else {
//				tenor = tenor.trim();
//			}
//			if(jedis != null) {
//				logger.info("get data from redis");
//				String key = "*CNY" + tenor + "=BANK";
//				Set<String> sets = jedis.keys(key);
//				List<Response<Map<String, String>>> rsp = new ArrayList<Response<Map<String, String>>>(sets.size());
//				Pipeline pipeline = jedis.pipelined();
//				for(String addKey : sets) {
//					rsp.add(pipeline.hgetAll(addKey));
//				}
//				pipeline.sync();
//				
//				for (Response<Map<String, String>> rs : rsp) {
//					lists.add(rs.get());
//				}
//			} else {
//				logger.info("get data from oralce");
//				sql = "select * from redis_cache t where instr(t.redis_key, 'CNY"
//					+ tenor + "=BANK') > 0";
//				List<Map<String, Object>> tempLists = dbUtil.getDataFromDB(sql);
//				for(Map<String, Object> map : tempLists) {
//					String value = (String) map.get("REDIS_VALUES");
//					Map<String, String> dataMap = GsonFactory.getGson().fromJson(value, Map.class);
//					lists.add(dataMap);
//				}
//			}
//			
//			if (lists.size() != 0) {
//				Map<String, Object> map = new HashMap<String, Object>();
//				map.put("QtnTp", "0");
//				if(StringUtils.equals(tenor, "ON")) {
//					tenor = "O/N";
//				} else if(StringUtils.equals(tenor, "TN")){
//					tenor = "T/N";
//				}
//				map.put("Term", tenor);
//
//				for (Map<String, String> maps : lists) {
//					map.put(maps.get("symbol").substring(0, 3) + "BuyPrc",
//							formatSymbol((String) maps.get("ask")));
//					map.put(maps.get("symbol").substring(0, 3) + "SellPrc",
//							formatSymbol((String) maps.get("bid")));
//				}
//				reslutList.add(map);
//			}
//		}
//		
//
//		String getMessage = getSendMessage(reslutList);
//		String ip = ConfigUtil.getString("esb.ip");
//		int port = ConfigUtil.getInt("esb.port");
//		int timeout = ConfigUtil.getInt("esb.timeout");
//		return SocketClient.sendToESB(ip, port, timeout, getMessage);
//	}
	@Override
	public String execute(){
		String getMessage = getSendMessage();
		String ip = ConfigUtil.getString("esb.ip");
		int port = ConfigUtil.getInt("esb.port");
		int timeout = ConfigUtil.getInt("esb.timeout");
		return SocketClient.sendToESB(ip, port, timeout, getMessage);
	}


	/**
	 * 
	 * @return 期限的list
	 */
    @SuppressWarnings("unchecked")
	public List<Map<String, Object>> getnormTenor() {
        String flag = ConfigUtil.getString("redis.flag");
        JedisUtil jedis = null;
        if(StringUtils.equals(flag, JedisUtil.CLUSTER)) {
            jedis = new JedisClusterUtil();
        } else if (StringUtils.equals(flag, JedisUtil.JEDIS)) {
            jedis = new JedisSingleUtil();
        }
        List<Map<String, Object>> reslutList = new ArrayList<Map<String, Object>>();
        String[] tenors = ConfigUtil.getString("esb.cc40tenor").split(",");
        String sql = "";
        List<Map<String, String>> lists = new ArrayList<Map<String,String>>();
        for (String tenor1 : tenors) {
        	String tenor = tenor1;
            if (StringUtils.isBlank(tenor)) {
                continue;
            } else {
                tenor = tenor.trim();
            }
            if(jedis != null && jedis.ping()) {
                logger.info("get data from redis");
                String key = "*CNY" + tenor + "=BANK";
                Set<String> sets = jedis.keys(key);
                lists = jedis.piplined(sets);

            } else {
                logger.info("get data from oralce");
                sql = "select * from redis_cache t where instr(t.redis_key, 'CNY"
                        + tenor + "=BANK') > 0";
                List<Map<String, Object>> tempLists = dbUtil.getDataFromDB(sql);
                for(Map<String, Object> map : tempLists) {
                    String value = (String) map.get("REDIS_VALUES");
                    Map<String, String> dataMap = GsonFactory.getGson().fromJson(value, Map.class);
                    lists.add(dataMap);
                }

            }

            if (lists.size() != 0) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("QtnTp", "0");
                if(StringUtils.equals(tenor, "ON")) {
                    tenor = "O/N";
                } else if(StringUtils.equals(tenor, "TN")){
                    tenor = "T/N";
                }
                map.put("Term", tenor);

                for (Map<String, String> maps : lists) {
                    map.put(maps.get("symbol").substring(0, 3) + "BuyPrc",
                            formatSymbol((String) maps.get("ask")));
                    map.put(maps.get("symbol").substring(0, 3) + "SellPrc",
                            formatSymbol((String) maps.get("bid")));
                }
                reslutList.add(map);
            }
        }
        if(jedis != null) {
            jedis.close();
        }

        return reslutList;
    }

	/**
	 * 
	 * @param price 价格
	 * @return 一定格式的价格
	 */
	private String formatSymbol(String price) {
		DecimalFormat format = new DecimalFormat("00000000000");
		String result = format.format(Double.parseDouble(price) * 10000);
		if (Double.parseDouble(price) >= 0) {
			return "+" + result;
		}
		return result;
	}

	/**
	 *  
	 * @return 得到非期限的列表
	 */
    public List<Map<String, Object>> getUnNormMessage() {
        TenorUtil tenorUtil = new TenorUtil(JedisUtil.CLUSTER);
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(16);
        String unNormTenors[] = ConfigUtil.getString("esb.cc40unnormtenor").split(",");
        for(String tenor : unNormTenors) {
            result.add(getUnNormMap(tenor, tenorUtil));
        }
        tenorUtil.close();
        return result;
    }

    /**
     * 
     * @param tenor 期限
     * @param tenorUtil 期限工具类
     * @return 非期限的列表
     */
    private Map<String, Object> getUnNormMap(String tenor, TenorUtil tenorUtil) {
        Map<String, Object> result = new HashMap<String, Object>(20);
        String ccys[] = ConfigUtil.getString("esb.cc40ccy").split(",");
        result.put("QtnTp", "0");
        result.put("Term", tenor);
        
        try {
	        for(String ccy : ccys) {
	        	String symbol = ccy + "." + "CNY";
	        	Map<String, String> map = TenorHandler.getInstance().handleMsg(symbol, tenor);
	        	logger.error("hsd........................." + symbol);
	            result.put(ccy + "BuyPrc", formatSymbol(map.get("ask")));
	            result.put(ccy + "SellPrc", formatSymbol(map.get("bid")));
	        }
        } catch (Exception e) {
			logger.error(e.getMessage(), e);
		} 
        return result;
    }

    /**
     * 
     * @return 得到结果集
     */
    private List<Map<String,Object>> getResultOfList() {
        List<Map<String, Object>> result = getnormTenor();
        List<Map<String, Object>> result1 = getUnNormMessage();
        List<Map<String, Object>> result1_1 = new ArrayList<Map<String, Object>>(2);
        List<Map<String, Object>> result1_2 = new ArrayList<Map<String, Object>>(2);
        List<Map<String, Object>> result1_3 = new ArrayList<Map<String, Object>>(5);
        List<Map<String, Object>> result1_4 = new ArrayList<Map<String, Object>>(5);
        List<Map<String, Object>> result1_5 = new ArrayList<Map<String, Object>>(2);
        for(int i=0; i<2; i++) {
            result1_1.add(result1.get(i));
        }

        for(int i=2; i<4; i++) {
            result1_2.add(result1.get(i));
        }

        for(int i=4; i<9; i++) {
            result1_3.add(result1.get(i));
        }

        for(int i=9; i<14; i++) {
            result1_4.add(result1.get(i));
        }

        for(int i=14; i<16; i++) {
            result1_5.add(result1.get(i));
        }
        result.addAll(11, result1_1);
        result.addAll(14, result1_2);
        result.addAll(17, result1_3);
        result.addAll(23, result1_4);
        result.addAll(30, result1_5);

        return result;
    }
}
