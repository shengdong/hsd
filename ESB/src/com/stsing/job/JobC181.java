package com.stsing.job;

import com.stsing.client.SocketClient;
import com.stsing.util.ConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class JobC181 implements JobExecute {
	/**
	 * declare XAG
	 */
	private static final String XAG = "XAG=";
	
	/**
	 * declare XAU
	 */
	private static final String XAU = "XAU=";
	

	@Override
	public String execute() {
		String ip = ConfigUtil.getString("esb.ip");
		int port = ConfigUtil.getInt("esb.port");
		int timeout = ConfigUtil.getInt("esb.timeout");
		String message = C181JobUtil.getMessage(XAG, "XAG");
		String rsp = SocketClient.sendToESB(ip, port, timeout, message);
		
		message = C181JobUtil.getMessage(XAU, "XAU");
		rsp = SocketClient.sendToESB(ip, port, timeout, message);
		
		String rics[] = ConfigUtil.getString("esb.ric").split(",");
		for(String ric : rics) {
			message = C181JobUtil.getMessage(ric);
			rsp = SocketClient.sendToESB(ip, port, timeout, message);
		}
		return rsp;
	}
}
