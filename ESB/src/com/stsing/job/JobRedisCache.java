package com.stsing.job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.Gson;
import com.stsing.jedis.JedisClusterUtil;
import com.stsing.jedis.JedisSingleUtil;
import com.stsing.jedis.JedisUtil;
import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.GsonFactory;

/**
 * 
 * @author wendi.zhou
 *
 */
public class JobRedisCache implements Job {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JobRedisCache.class);

	/**
	 * declare dbUtil
	 */
	private DBUtil dbUtil = null;

	/**
	 * 构造对象， 初始化dbutil对象
	 */
	public JobRedisCache() {
		dbUtil = new DBUtil();
	}

	/**
	 * 
	 * @return 返回当前数据库中有的数据，缓存到map中
	 */
	private Map<String, String> getRedisMap() {
		Map<String, String> redisMap = new HashMap<String, String>();
		Connection connection = dbUtil.getConnection();
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			statement = connection.prepareStatement("select * from redis_cache");
			result = statement.executeQuery();
			while (result.next()) {
				redisMap.put(result.getString("REDIS_KEY"), result.getString("REDIS_VALUES").trim());
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
		if (result != null) {
			try {
				result.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(), e);
			}
		}

		return redisMap;
	}

	/**
	 * run code
	 */
	private void run() {
		Map<String, String> redisMap = getRedisMap();
		Connection connection = null;
		PreparedStatement statement = null;
		PreparedStatement statement2 = null;
		connection = dbUtil.getConnection();
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currenttime = format.format(date);
		String sbInsert = "insert into REDIS_CACHE(REDIS_KEY,KEY_TYPE,REDIS_VALUES,LAST_UPD_TIME) values (?,?,?,?)";
		String sbupdate = "update  REDIS_CACHE set REDIS_VALUES= ?,LAST_UPD_TIME=? where REDIS_KEY = ?";
		try {
			statement = connection.prepareStatement(sbInsert);
			statement2 = connection.prepareStatement(sbupdate);
		} catch (SQLException e1) {
			logger.error(e1.getMessage(), e1);
		}
//		String ip = ConfigUtil.getString("redis.ip");
//		int port = ConfigUtil.getInt("redis.port");
//		Jedis jedis = new Jedis(ip, port);
		JedisUtil jedis = null;
		String flag = ConfigUtil.getString("redis.flag");
		if(StringUtils.equals(flag, JedisUtil.JEDIS)) {
			jedis = new JedisSingleUtil();
		} else if(StringUtils.equals(flag, JedisUtil.CLUSTER)) {
			jedis = new JedisClusterUtil();
		}
		Gson gson = GsonFactory.getGson();
		String keysList = ConfigUtil.getString("redis_key.key");
		String keysSetList = ConfigUtil.getString("redis_key.keySet");
		String[] keys = keysList.split(",");
		String[] keysSet = keysSetList.split(",");
		
		if(jedis ==null || !jedis.ping()) {
			logger.error("jedis is null or jedis cluster is down");
			return;
		}

		try {
			connection.setAutoCommit(false);
			for (String keyGroup : keys) {
				Set<String> set = jedis.keys(keyGroup);

				List<Map<String, String>> lists = jedis.piplined(set);
				for (Map<String, String> valuesMap : lists) {
					String json = gson.toJson(valuesMap);
					if (redisMap.get(valuesMap.get("ric")) == null) {
						statement.setString(1, valuesMap.get("ric"));
						statement.setString(2, "hash");
						statement.setString(3, json);
						statement.setString(4, currenttime);
						statement.addBatch();
					} else {
						statement2.setString(1, json);
						statement2.setString(2, currenttime);
						statement2.setString(3, valuesMap.get("ric"));
						statement2.addBatch();
					}
				}
			}
			for (String keySetGroup : keysSet) {
				Set<String> set = jedis.smembers(keySetGroup);
				String jons = "";
				for (String key : set) {
					if (set.size() == 1) {
						jons = key;
					} else {
						jons = jons + "," + key;
					}
				}
				if (jons.length() > 1) {
					jons = jons.substring(1);
				}
				if (redisMap.get(keySetGroup) == null) {

					statement.setString(1, keySetGroup);
					statement.setString(2, "set");
					statement.setString(3, jons);
					statement.setString(4, currenttime);
					statement.addBatch();
				} else {
					statement2.setString(1, jons);
					statement2.setString(2, currenttime);
					statement2.setString(3, keySetGroup);
					statement2.addBatch();
				}
			}
			statement.executeBatch();
			statement2.executeBatch();
			connection.commit();
			statement.close();
			statement2.close();
			connection.close();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);

		} finally {
				if (statement != null) {
					
					try {
						statement.close();
					} catch (SQLException e) {
						logger.error(e.getMessage(), e);
					}
				}
				if (statement2 != null) {
					try {
						statement2.close();
					} catch (SQLException e) {
						logger.error(e.getMessage(), e);
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e) {
						logger.error(e.getMessage(), e);
					}
				}
				
				if(jedis != null) {
					jedis.close();
				}

			} 
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		Date begin = new Date();
		logger.info("get data from redis  synchronize to oracle");
		run();
		Date end = new Date();
		logger.info("synchronized success : take " + (end.getTime() - begin.getTime()) + " ms");
	}
	
	public static void main(String[] args) {
		try {
			new JobRedisCache().execute(null); 
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
