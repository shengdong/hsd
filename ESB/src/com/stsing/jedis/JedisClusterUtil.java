package com.stsing.jedis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import com.stsing.jedis.JedisClusterStsingUtil;
import com.stsing.util.ConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class JedisClusterUtil implements JedisUtil {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(JedisClusterUtil.class);

	/**
	 * declare jedisCluster
	 */
	private JedisCluster jedisCluster = null;

	/**
	 * declare stsingUtil
	 */
	private JedisClusterStsingUtil stsingUtil = null;

	
	/**
	 * 构造方法
	 */
	public JedisClusterUtil() {
		Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();

		String ips[] = ConfigUtil.getString("redis.clusterIP").split(";");

		for (String ip : ips) {
			String host = ip.split(":")[0].trim();
			String portString = ip.split(":")[1].trim();
			int port = Integer.parseInt(portString);
			HostAndPort hostAndPort = new HostAndPort(host, port);
			jedisClusterNodes.add(hostAndPort);
		}

		jedisCluster = new JedisCluster(jedisClusterNodes);
		stsingUtil = new JedisClusterStsingUtil();
		
		logger.error("jedisCluster number : " + jedisCluster.getClusterNodes().size());
	}

	@Override
	public void close() {
		try {
			jedisCluster.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public Map<String, String> hgetAll(String key) {
		return jedisCluster.hgetAll(key);
	}

	@Override
	public String hmset(String key, Map<String, String> map) {
		return jedisCluster.hmset(key, map);
	}

	@Override
	public Set<String> keys(String pattern) {
		return stsingUtil.keys(pattern, jedisCluster);
	}

	@Override
	public boolean ping() {
		try {
			jedisCluster.set("check", "ok");
			jedisCluster.del("check");
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, String>> piplined(Set<String> keys) {
		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
		JedisClusterStsingUtil jcp = JedisClusterStsingUtil.pipelined(jedisCluster);
		jcp.refreshCluster();
		List<Object> batchResult = null;
		try {
			for (String key : keys) {
				jcp.hgetAll(key);
			}
			
			batchResult = jcp.syncAndReturnAll();
			for(Object object : batchResult) {
				result.add((Map<String, String>)object);
			}
			jcp.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

	@Override
	public Set<String> smembers(String key) {
		return jedisCluster.smembers(key);
	}
}
