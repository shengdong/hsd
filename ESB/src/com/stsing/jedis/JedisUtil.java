package com.stsing.jedis;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author k1193
 *
 */

public interface JedisUtil {
	/**
	 * declare CLUSTER
	 */
	public static final String CLUSTER = "cluster";
	
	/**
	 * declare JEDIS
	 */
	public static final String JEDIS = "jedis";
	
	/**
	 * 
	 * @return true of false
	 */
	public boolean ping();
	
	/**
	 * 
	 * @param key key
	 * @return map
	 */
	public Map<String, String> hgetAll(String key);
	
	/**
	 * 
	 * @param key key
	 * @param map map
	 * @return 成功或者是失败
	 */
	public String hmset(String key, Map<String, String> map);

	/**
	 * 
	 * @param pattern 正则表达式
	 * @return Set
	 */
	public Set<String> keys(String pattern);
	
	/**
	 * 
	 * @param key key
	 * @return set 列表
	 */
	public Set<String> smembers(String key);
	
	/**
	 * 
	 * @param keys 批量的key
	 * @return list
	 */
	public List<Map<String, String>> piplined(Set<String> keys); 
	
	/**
	 * 关闭jedis
	 */
	public void close();
}
