package com.stsing.jedis;

import java.util.Set;

import org.apache.log4j.Logger;

public class JedisTest {
	private static final Logger logger = Logger.getLogger(JedisTest.class);
	
	
	public static void main(String[] args) {
		JedisUtil jedisUtil = new JedisClusterUtil();
		Set<String> sets = jedisUtil.keys("*");
		for(String k : sets) {
			logger.info(k);
		}
	}
}
