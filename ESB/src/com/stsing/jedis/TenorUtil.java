package com.stsing.jedis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

import com.stsing.util.ConfigUtil;
import com.stsing.util.DBUtil;
import com.stsing.util.GsonFactory;

/**
 * Created by k1193 on 2016/12/16.
 */
public class TenorUtil {
	/**
	 * declare PATTREN
	 */
	private static final String PATTREN = "yyyyMMdd";

	/**
	 * declare jedisUtil
	 */
	private JedisUtil jedisUtil = null;

	/**
	 * 
	 * @param flag
	 *            jedisCluster or jedisSingle
	 */
	public TenorUtil(String flag) {
		if (StringUtils.equals(flag, JedisUtil.CLUSTER)) {
			jedisUtil = new JedisClusterUtil();
		} else if (StringUtils.equals(flag, JedisUtil.JEDIS)) {
			jedisUtil = new JedisSingleUtil();
		}
	}

	public void close() {
		if (jedisUtil != null) {
			jedisUtil.close();
		}
	}

	/**
	 * 
	 * @param symbol
	 *            USD.CNY
	 * @param endTime
	 *            20170628
	 * @return ask, bid ....
	 */
	public Map<String, String> getPriceBy(String symbol, String endTime) {

		Map<String, String> result = new HashMap<String, String>(10);
		List<String> tenors = getTenor(symbol, endTime);
		if (tenors.size() == 1) {
			String tenor = tenors.get(0).split(";")[0];

			return getValueOfTenorAndSymbol(symbol, tenor);

		} else if (tenors.size() == 2) {
			String tenor1 = tenors.get(0).split(";")[0];
			String tenor1Date = tenors.get(0).split(";")[1];

			Map<String, String> map1 = getValueOfTenorAndSymbol(symbol, tenor1);

			String tenor2 = tenors.get(1).split(";")[0];
			String tenor2Date = tenors.get(1).split(";")[1];

			Map<String, String> map2 = getValueOfTenorAndSymbol(symbol, tenor2);

			int day1 = getDayOfCountBy(tenor1Date);
			int day2 = getDayOfCountBy(tenor2Date);

			int searchDay = getDayOfCountBy(endTime);
			double bid = getValueOfDown(day1, Double.parseDouble(map1
					.get("bid")), day2, Double.parseDouble(map2.get("bid")),
					searchDay);
			double ask = getValueOfUp(day1,
					Double.parseDouble(map1.get("ask")), day2, Double
							.parseDouble(map2.get("ask")), searchDay);
			result.put("bid", bid + "");
			result.put("ask", ask + "");
			result.put("symbol", symbol.replace('.', '/'));
			result.put("source", "BANK");
			double askCash = getValueOfDown(day1, Double.parseDouble(map1
					.get("askCash")), day2, Double.parseDouble(map2
					.get("askCash")), searchDay);
			double bidCash = getValueOfDown(day1, Double.parseDouble(map1
					.get("bidCash")), day2, Double.parseDouble(map2
					.get("bidCash")), searchDay);
			double askClient = getValueOfUp(day1, Double.parseDouble(map1
					.get("askClient")), day2, Double.parseDouble(map2
					.get("askClient")), searchDay);
			double bidClient = getValueOfUp(day1, Double.parseDouble(map1
					.get("bidClient")), day2, Double.parseDouble(map2
					.get("bidClient")), searchDay);

			result.put("askCash", askCash + "");
			result.put("bidCash", bidCash + "");
			result.put("askClient", askClient + "");
			result.put("bidClient", bidClient + "");
		}

		return result;
	}

	/**
	 * 
	 * @param symbol  symbol
	 *           
	 * @param tenor tenor
	 *            
	 * @return the value of ric
	 */
	private Map<String, String> getValueOfTenorAndSymbol(String symbol,
			String tenor) {
		Map<String, String> result = new HashMap<String, String>();
		if (StringUtils.contains(symbol, "USD")) {
			symbol = "CNY";
		} else {
			symbol = symbol.replace(".", "");
		}
		if (StringUtils.contains(tenor, "SPOT")) {
			tenor = "";
		}
		String ric = symbol + tenor + "=BANK";

		if (jedisUtil.ping()) {
			result = jedisUtil.hgetAll(ric);
		} else {
			result = getMapFromOracle(ric);
		}
		return result;
	}

	/**
	 * 
	 * @param symbol 币种
	 * @param endTime 到期日
	 * @return 返回期限
	 */
	public List<String> getTenor(String symbol, String endTime) {
		int search = Integer.parseInt(endTime);
		String sql = "select t.valuedate_sw, t.valuedate_2w, t.valuedate_3w, t.valuedate_1m, t.valuedate_2m, " +
				"t.valuedate_3m, t.valuedate_4m, t.valuedate_5m, t.valuedate_6m, t.valuedate_9m," +
				" t.valuedate_1y, t.valuedate_18m, t.valuedate_2y, t.valuedate_3y from valuedate t where t.ccys = '"
				+ symbol
				+ "' and t.tradedate = '"
				+ ConfigUtil.getToday("yyyyMMdd") + "'";

		List<String> result = new ArrayList<String>(2);
		DBUtil dbUtil = new DBUtil();
		Map<String, String> lists = new HashMap<String, String>(20);
		Map<String, Object> listsOfDB = dbUtil.getDataFromDB(sql).get(0);
		int[] ls = new int[listsOfDB.size()];
		int count = 0;
		for (String key : listsOfDB.keySet()) {
			String value = (String) listsOfDB.get(key);
			if (value.matches("\\d*") && !StringUtils.equals(key, "TRADEDATE")) {
				lists.put(value, key);
				ls[count] = Integer.parseInt(value);
				count++;
			}
		}
		Arrays.sort(ls);
		if (search > ls[ls.length - 1]) {
			result.add(lists.get(ls[ls.length - 2] + "").split("_")[1] + ";"
					+ ls[ls.length - 2]);
			result.add(lists.get(ls[ls.length - 1] + "").split("_")[1] + ";"
					+ ls[ls.length - 1]);
		} else {
			for (int i = 0; i < ls.length; i++) {
				if (ls[i] == search) {
					result.add(lists.get(ls[i] + "").split("_")[1] + ";"
							+ ls[i]);
					break;
				}

				if (ls[i] > search) {
					result.add(lists.get(ls[i - 1] + "").split("_")[1] + ";"
							+ ls[i - 1]);
					result.add(lists.get(ls[i] + "").split("_")[1] + ";"
							+ ls[i]);
					break;
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @param time 时间
	 * @return 返回天数
	 */
	public int getDayOfCountBy(String time) {
		Date date = ConfigUtil.getDayBy(time, PATTREN);

		Date today = ConfigUtil.getDayBy(ConfigUtil.getToday(PATTREN), PATTREN);

		long oneDayTime = 24 * 60 * 60 * 1000l;
		long result = (date.getTime() - today.getTime()) / oneDayTime;
		return new Long(result).intValue();
	}

	/**
	 * 
	 * @param x1 x1
	 * @param y1 y1
	 * @param x2 x2
	 * @param y2 y2
	 * @param x 2
	 * @param flag 标示
	 * @param count 个数
	 * @return 返回值
	 */
	public double getValueBy(double x1, double y1, double x2, double y2,
			double x, int flag, int count) {
		double k = (y2 - y1) / (x2 - x1);
		double b = y1 - x1 * k;
		double result = k * x + b;

		BigDecimal y = new BigDecimal(result);

		return y.setScale(count, flag).doubleValue();
	}

	/**
	 * 
	 * @param x1 x1
	 * @param y1 y1
	 * @param x2 x2
	 * @param y2 y2
	 * @param x 2
	 * @return 返回向上进位的值
	 */
	public double getValueOfUp(double x1, double y1, double x2, double y2,
			double x) {
		return getValueBy(x1, y1, x2, y2, x, BigDecimal.ROUND_CEILING, 0);
	}

	/**
	 * 
	 * @param x1 x1
	 * @param y1 y1
	 * @param x2 x2
	 * @param y2 y2
	 * @param x x
	 * @return 返回向下进位的值
	 */
	public double getValueOfDown(double x1, double y1, double x2, double y2,
			double x) {
		return getValueBy(x1, y1, x2, y2, x, BigDecimal.ROUND_FLOOR, 0);
	}

	/**
	 * 
	 * @param ric ric码
	 * @return 从oracle获得的值
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String> getMapFromOracle(String ric) {
		String sql = "select t.redis_values from redis_cache t where redis_key = '"
				+ ric + "'";
		List<Map<String, Object>> list = new DBUtil().getDataFromDB(sql);
		String value = (String) list.get(0).get("REDIS_KEY");
		return GsonFactory.getGson().fromJson(value, Map.class);
	}

}
