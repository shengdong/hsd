package com.desinger;

public class Client {

	public static void main(String[] args) {
		Component component = new Composite("root node");
		Component child = new Composite("one node child");
		Component child_1 = new Leaf("one node child of 1");
		Component child_2 = new Leaf("one node child of 2");
		child.add(child_1);
		child.add(child_2);
		
		Component child2 = new Composite("one node child2");
		component.add(child);
		component.add(child2);
		
		component.foreach();
	}
}
