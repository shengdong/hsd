package com.desinger;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {
	private List<Component> childs = new ArrayList<Component>(2 << 6);
	

	public Composite(String name) {
		super(name);
	}

	@Override
	public void add(Component component) {
		childs.add(component);
	}

	@Override
	public void remove(Component component) {
		childs.remove(component);
	}

	@Override
	public void foreach() {
		System.out.println("child name : " + super.getName());
		for(Component component : childs) {
			component.foreach();
		}

	}

}
