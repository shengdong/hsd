package com.netty;

import org.apache.log4j.Logger;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;

public class TimeClient {
	private static final Logger logger = Logger.getLogger(TimeClient.class);
	
	public void connect(int port, String host) {
		EventLoopGroup group = new NioEventLoopGroup();
		
		try {
			Bootstrap bootstrap = new Bootstrap();
			bootstrap.group(group).channel(NioSocketChannel.class)
			.option(ChannelOption.TCP_NODELAY, true)
			.handler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel channel) throws Exception {
					channel.pipeline().addLast(new LineBasedFrameDecoder(1024));
					channel.pipeline().addLast(new StringDecoder());
					channel.pipeline().addLast(new TimeClientHandler());
				}
			});
			
			ChannelFuture future = bootstrap.connect(host, port).sync();
			
			future.channel().closeFuture().sync();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			group.shutdownGracefully();
		}
	}
	
	public static void main(String[] args) {
		int port = 8080;
		if(args != null && args.length > 0) {
			try {
				port = Integer.parseInt(args[0]);
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		new TimeClient().connect(port, "127.0.0.1");
	}
}
