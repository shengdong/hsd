package dbcp;
import java.sql.Connection;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
public class DBCP {
	private static final Logger logger = Logger.getLogger(DBCP.class);
	private static BasicDataSource dataSource = null;
	static {
		dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:mysql://localhost:3306/dong?useSSL=true");
		dataSource.setPassword("123456");
		dataSource.setUsername("root");
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setMaxIdle(100);
		dataSource.setInitialSize(10);
		dataSource.setMaxWaitMillis(1000 * 100);
		dataSource.setRemoveAbandonedOnBorrow(true);
	}
	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return connection;
	}
	public static void main(String[] args) {
		Connection connection = new DBCP().getConnection();
		logger.info(connection);
	}
}
