package com.proxy;

public class Client {

	public static void main(String[] args) {
		Subject subject = new RealSubject();
		
		Subject proxy = new Proxy(subject);
		
		proxy.operate();
	}
}
