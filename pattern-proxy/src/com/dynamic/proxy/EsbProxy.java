package com.dynamic.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class EsbProxy implements InvocationHandler {
	private Object subject;
	

	public EsbProxy(Object subject) {
		this.subject = subject;
	}
	

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		
		System.out.println("print before send hexin message");
		
		method.invoke(subject, args);
		
		System.out.println("print after send hexin message");
		
		return null;
	}

}
