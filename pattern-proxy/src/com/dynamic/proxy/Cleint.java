package com.dynamic.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Cleint {

	public static void main(String[] args) {
		HeXinSubject subject = new HQPTSubject();

		InvocationHandler handler = new EsbProxy(subject);

		HeXinSubject subject2 = (HeXinSubject) Proxy.newProxyInstance(handler.getClass().getClassLoader(),
				subject.getClass().getInterfaces(), handler);
		
		subject2.send();
	}
}
