package com.pattern;

public class DeptHandler extends ConSumeHandler {

	@Override
	public void doHandler(String user, double free) {
		if(free < 1000) {
			if(user.equals("zy")) {
				System.out.println("cloud apply : " + free);
			} else {
				System.out.println("cloud not apply : " + free);
			}
		} else {
			if(getNextHandler() != null) {
				getNextHandler().doHandler(user, free);
			}
		}

	}

}
