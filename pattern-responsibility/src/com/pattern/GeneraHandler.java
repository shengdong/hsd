package com.pattern;

public class GeneraHandler extends ConSumeHandler {

	@Override
	public void doHandler(String user, double free) {
		if(free >= 1000) {
			if(user.equals("lwxzy")) {
				System.out.println("cloud apply : " + free);
			} else {
				System.out.println("cloud not apply : " + free);
			}
		} else {
			if(getNextHandler() != null) {
				getNextHandler().doHandler(user, free);
			}
		}
	}

}
