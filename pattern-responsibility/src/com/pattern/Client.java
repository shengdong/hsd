package com.pattern;

public class Client {

	public static void main(String[] args) {
		ProjectHandler projectHandler = new ProjectHandler();
		DeptHandler deptHandler = new DeptHandler();
		GeneraHandler generaHandler = new GeneraHandler();
		
		projectHandler.setNextHandler(deptHandler);
		deptHandler.setNextHandler(generaHandler);
		
		projectHandler.doHandler("lwx", 450);
		projectHandler.doHandler("lwx", 600);
		projectHandler.doHandler("zy", 600);
		projectHandler.doHandler("zy", 1500);
		projectHandler.doHandler("lwxzy", 1500);
	}
}
