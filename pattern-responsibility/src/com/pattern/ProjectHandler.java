package com.pattern;

public class ProjectHandler extends ConSumeHandler {

	@Override
	public void doHandler(String user, double free) {
		if(free < 500) {
			if(user.equals("lwx")) {
				System.out.println("cloud apply : " + free);
			} else {
				System.out.println("cloud not apply : " + free);
			}
		} else {
			if(getNextHandler() != null) {
				getNextHandler().doHandler(user, free);
			}
		}
	}

}
