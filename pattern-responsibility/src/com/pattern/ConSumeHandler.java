package com.pattern;

public abstract class ConSumeHandler {

	private ConSumeHandler nextHandler;

	public ConSumeHandler getNextHandler() {
		return nextHandler;
	}

	public void setNextHandler(ConSumeHandler nextHandler) {
		this.nextHandler = nextHandler;
	}
	
	public abstract void doHandler(String user, double free);
	
}
