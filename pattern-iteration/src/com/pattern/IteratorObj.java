package com.pattern;

public interface IteratorObj {

	public void first();
	
	public boolean hasNextItem();
	
	public Object currentItem();
}
