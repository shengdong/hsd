package com.pattern;

public class DataObj implements DataSet {
	private Object[] objArray = null;
	
	
	public DataObj(Object[] objects) {
		this.objArray = objects;
	}

	@Override
	public IteratorObj getIterator() {
		return new Iterator1(this) ;
	}
	
	public Object getIntem(int index) {
		return objArray[index];
	}
	
	public int getSize() {
		return objArray.length;
	}

}
