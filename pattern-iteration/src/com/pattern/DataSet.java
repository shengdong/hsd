package com.pattern;

public interface DataSet {

	public IteratorObj getIterator();
}
