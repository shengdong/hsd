package com.pattern;

public class Client {

	public static void main(String[] args) {
		String[] str = {"12312", "dasda", "12d", "asd"};
		
		DataObj obj= new DataObj(str);
		
		IteratorObj  iteratorObj = obj.getIterator();
		
		while(iteratorObj.hasNextItem()) {
			System.out.println(iteratorObj.currentItem());
		}
	}
}
