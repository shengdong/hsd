package com.pattern;

public class Iterator1 implements IteratorObj {
	private DataObj set;
	
	private int size;
	
	private int index = 0;
	
	
	public Iterator1(DataObj set) {
		this.set = set;
		this.size = set.getSize();
	}

	@Override
	public void first() {
		this.index = 0;
	}

	@Override
	public boolean hasNextItem() {
		if(index < size) {
			return true;
		}
		
		return false;
	}

	@Override
	public Object currentItem() {
		Object obj = set.getIntem(index);
		if(index < size) {
			index ++;
		}
		return obj;
	}

}
