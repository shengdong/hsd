package com.messagePack;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.msgpack.MessagePack;
import org.msgpack.template.Templates;

public class MessagePackTest {
	private static final Logger logger = Logger.getLogger(MessagePackTest.class);
	
	public void test() throws Exception {
		List<String> src = new ArrayList<>(10);
		src.add("msgpack");
		src.add("kumkfs");
		src.add("viver");
		
		MessagePack messagePack = new MessagePack();
		byte[] raw = messagePack.write(src);
		
		List<String> dst1 = messagePack.read(raw, Templates.tList(Templates.TString));
		logger.info(dst1.get(0));
		logger.info(dst1.get(1));
		logger.info(dst1.get(2));
	}
	
	public static void main(String[] args) {
		try {
			new MessagePackTest().test();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
