package com.netty;

import org.apache.log4j.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

public class EchoClientHandler extends ChannelHandlerAdapter {
	private static final Logger logger = Logger.getLogger(EchoClientHandler.class);
	
	private int count =0;
	
	private static final String MESSAGE = "Hi, HSD.welcome to netty.$_";

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		for(int i=0; i<10; i++) {
			ctx.writeAndFlush(Unpooled.copiedBuffer(MESSAGE.getBytes()));
		}
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		logger.info("this is " + (++count) + " times receive server : [" + msg + " ]");
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}
