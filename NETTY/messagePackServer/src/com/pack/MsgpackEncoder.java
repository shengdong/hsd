package com.pack;

import org.msgpack.MessagePack;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class MsgpackEncoder extends MessageToByteEncoder<Object> {

	@Override
	protected void encode(ChannelHandlerContext context, Object object, ByteBuf buf) throws Exception {
		MessagePack messagePack = new MessagePack();
		byte[] raw = messagePack.write(object);
		buf.writeBytes(raw);
	}

}
