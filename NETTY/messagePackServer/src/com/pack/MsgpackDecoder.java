package com.pack;

import java.util.List;

import org.msgpack.MessagePack;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

public class MsgpackDecoder extends MessageToMessageDecoder<ByteBuf> {

	@Override
	protected void decode(ChannelHandlerContext context, ByteBuf buf, List<Object> list) throws Exception {
		final byte[] array;
		final int length = buf.readableBytes();
		array = new byte[length];
		buf.getBytes(buf.readerIndex(), array, 0, length);
		MessagePack messagePack = new MessagePack();
		list.add(messagePack.read(array));
	}

}
