package com.netty;

import java.util.Date;

import org.apache.log4j.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

public class TimeServerHandler extends ChannelHandlerAdapter {
	private static final Logger logger = Logger.getLogger(TimeServerHandler.class);
	
	private int count;
	

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//		ByteBuf buf = (ByteBuf)msg;
//		byte[] req = new byte[buf.readableBytes()];
//		buf.readBytes(req);
//		
//		String body = new String(req, "UTF-8").substring(0, req.length - System.getProperty("line.separator").length());
		String body = (String)msg;
		logger.info("the time server receive order : " + body + " ; the counter is : " + ++count);
		
		String currentTime = "QUERY TIME ORDER".equalsIgnoreCase(body)?new Date(
				System.currentTimeMillis()).toString() : "BAD ORDER";
		logger.info(currentTime);
		currentTime = currentTime + System.getProperty("line.separator");
		ByteBuf resp = Unpooled.copiedBuffer(currentTime.getBytes());
		ctx.writeAndFlush(resp);
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		// TODO Auto-generated method stub
		ctx.close();
	}

	

}
