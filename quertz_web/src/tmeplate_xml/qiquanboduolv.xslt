<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/" name="qianquanbodonglv">
		<xsl:param name="currency"
			select="message/body/groups/group/groups/group/field[@tag=48]"></xsl:param>
		<xsl:param name="date_data" select="message/body/field[@tag=60]"></xsl:param>
		<xsl:param name="date_type"
			select="message/body/groups/group/groups/group/field[@tag=55]"></xsl:param>
		<sqls>
			<xsl:for-each select="message/body/groups/group/groups/group/groups/group">
				<sql>
					QiQUANBODONGLV,
					<xsl:value-of select="$currency"></xsl:value-of>,
					<xsl:value-of select="substring($date_data,1,8)"></xsl:value-of>,
					CFETS,
					<xsl:value-of select="substring($currency,5,3)"></xsl:value-of>,CNY,
					<xsl:value-of select="groups/group/field[@tag=10446]"></xsl:value-of>,
					<xsl:value-of select="substring($date_type,4,3)"></xsl:value-of>,
					<xsl:if test="field[@tag=10443] = -1">
					   broking
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 1">
					   O/N
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 2">
					   T/N
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 3">
					   S/N
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 7">
					   1W
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 14">
					   2W
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 21">
					   3W
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 30">
					   1M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 60">
					   2M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 90">
					   3M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 120">
					   4M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 150">
					   5M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 180">
					   6M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 270">
					   9M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 540">
					   18M
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 360">
					   1Y
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 720">
					   2Y
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 1080">
					   3Y
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 1440">
					   4Y
					</xsl:if>
					<xsl:if test="field[@tag=10443] = 1800">
					   5Y
					</xsl:if>
				</sql>
			</xsl:for-each>
		</sqls>
	</xsl:template>
</xsl:stylesheet>