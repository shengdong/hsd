<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="qiquanboduolv.xslt" />
	<xsl:import href="waihuidiaoqi.xslt" />
	<xsl:import href="shoupanjia.xslt" />
	<xsl:template match="/">
		<xsl:param name="marketType"
			select="message/body/groups/group/groups/group/field[@tag=48]"></xsl:param>

		<xsl:if test="'CVCC' = substring($marketType,1,4)">
			<xsl:call-template name="qianquanbodonglv"></xsl:call-template>
		</xsl:if>
		
		<xsl:if test="'CSCAUSD0' = $marketType">
			<xsl:call-template name="waihuidiaoqi"></xsl:call-template>
		</xsl:if>
		
		<xsl:if test="substring-after($marketType, '=') = 'CFHBC'">
			<xsl:call-template name="shoupanjia"></xsl:call-template>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>