package com.stsing.send.job;

/**
 * 
 */
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import org.apache.log4j.Logger;
import com.stsing.util.QuertzConfigUtil;

/**
 * @author k1193
 *
 */
public class SocketClient {
	/**
	 *  declare logger
	 */
	private static final Logger logger = Logger.getLogger(SocketClient.class);
	
	
	/**
	 * 
	 * @param ip 中间件ip
	 * @param port 中间件端口
	 * @param message 发送的数据
	 * @return 超时时间
	 * @throws SocketTimeoutException socket超时异常
	 * @throws IOException IO异常
	 */
	public String executeClient(String ip, int port, String message) throws SocketTimeoutException, IOException {
		int timeout = QuertzConfigUtil.getIntValue("timeout");
		return sendDataBySocket(ip, port, message, timeout);
	}
	
	/**
	 * 
	 * @param host 中间件ip
	 * @param port 中间件端口
	 * @param data 发送的数据
	 * @param timeout 超时时间
	 * @return 返回的报文
	 * @throws SocketTimeoutException socket超时异常
	 * @throws IOException IO异常
	 */
	public String sendDataBySocket(String host, int port, String data, int timeout) throws SocketTimeoutException, IOException {
		OutputStreamWriter outputStreamWriter = null;
		BufferedWriter bufferedWriter = null;
		InputStream inputStream = null;
		try {
			logger.info(host + ":" + port);
			Socket socCli = new Socket(host, port);
			socCli.setSoTimeout(timeout);
			outputStreamWriter = new OutputStreamWriter(socCli
					.getOutputStream(), "UTF-8");
			inputStream = socCli.getInputStream();
			byte received[] = new byte[1024 * 10];
			StringBuilder builder = new StringBuilder();
			bufferedWriter = new BufferedWriter(outputStreamWriter);
			bufferedWriter.write(data);
			bufferedWriter.flush();
			logger.info("send message success!! : " + data);
			logger.info("begin get data.........");
			int count = 0;
			while ((count = inputStream.read(received, 0, received.length)) > 0) {
				String str = new String(received, 0, count, "utf-8");
				builder.append(str);
			}
			inputStream.close();
			bufferedWriter.close();
			socCli.close();
			return new String(received).trim();

		}  finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (outputStreamWriter != null) {
					outputStreamWriter.close();
				}
				if (bufferedWriter != null) {
					bufferedWriter.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

}

