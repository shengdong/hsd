package com.stsing.send.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.stsing.model.Info;
import com.stsing.provide.job.CurrentProvideJob;
import com.stsing.util.DBUtil;
import com.stsing.util.ErrorDBHandler;
import com.stsing.util.SocketUtil;

/**
 * 
 * @author k1193
 *
 */
public class SendJob implements Job {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SendJob.class);
	

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getTrigger().getJobKey().getName();
		logger.info("jobName: " + jobName);
		List<Info> lists = sendToServerJob(jobName);
		ErrorDBHandler.insertError(lists);
		
	    
	    if(("sendlcfile3".equals(jobName) || "sendlcfile4".equals(jobName))) {
	    	CurrentProvideJob.provideData(jobName);
	    }
	}
	
	/**
	 * send to server job	
	 * @param jobName the job of name
	 * @return server return message
	 */
	public List<Info> sendToServerJob(String jobName) {
		try {
			String ip = "";
			int port = 0;
			String sql = "select t.*, d.process from quertz t left join dualprocess d on t.service = d.service where t.jobname = '" + jobName + "'";
			List<Map<String, Object>> listMaps = new DBUtil().getDataFromDB(sql);
			Map<String, Object> map = listMaps.get(0);
			String process = (String)map.get("PROCESS");
			if(StringUtils.equals(process, "PRIME")) {
				ip = (String)map.get("IP");
				port = Integer.parseInt((String) map.get("PORT"));
			} else {
				ip = (String)map.get("BACKIP");
				port = Integer.parseInt((String) map.get("BACKPORT"));
			}
			
	        String flag = (String)map.get("FLAG");
	        logger.info("ip: " + ip + " port: " + port + " message : " + flag);
		    String received = new SocketClient().executeClient(ip, port, flag);
		    List<Info> lists = SocketUtil.getLists(received);
		    return lists;
		}  catch (Exception e) {
			Info info = new Info();
			info.setErrorInfo("job error");
			info.setObject(jobName);
			info.setSuccess(false);
			List<Info> lists = new ArrayList<Info>();
			lists.add(info);
			logger.error(e.getMessage(), e);
			return lists;
		}
	}
}
