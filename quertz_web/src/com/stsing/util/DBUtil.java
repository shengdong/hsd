package com.stsing.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.model.Quertz;

/**
 * 
 * @author shengdong.he
 *
 */
public class DBUtil {
	/**
	 *  declare logger
	 */
	private static final Logger logger = Logger.getLogger(DBUtil.class);
	
	/**
	 *  declare URL
	 */
	private  static String url = "";
	
	/**
	 *  declare driverClassName
	 */
	private  static String driverClassName = "";
	
	/**
	 *  declare userName
	 */
	private  static String username = "";
	
	/**
	 *  declare password
	 */
	private  static String password = "";
	
	
	private static DBUtil dbUtil = null;
	
	
	static {
		url = QuertzConfigUtil.getValue("url");
		driverClassName = QuertzConfigUtil.getValue("driverClassname");
		username = QuertzConfigUtil.getValue("username");
		password = QuertzConfigUtil.getValue("password");
	}
	
	public synchronized static DBUtil getIntance() {
		if(dbUtil == null) {
			synchronized (DBUtil.class) {
				dbUtil = new DBUtil();
			}
		}
		return dbUtil;
	}
	
	
	/**
	 * get connection
	 * @return ��ݿ������
	 * @throws Exception
	 */
	public Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, username, password);
			return connection;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	/**
	 * @return get quertz details
	 */
	public List<Quertz> getJObOfListMapFromDB() {
		List<Quertz> result = new ArrayList<Quertz>();
		PreparedStatement preparedStatement = null;
		Connection connection = getConnection();
		ResultSet resultSet = null;
		try {
		    preparedStatement = connection.prepareStatement("select * from quertz");
		    resultSet = preparedStatement.executeQuery();
		    while(resultSet.next()) {
		    	Quertz quertz = new Quertz();
		    	quertz.setClassName(resultSet.getString("CLASSNAME"));
		    	quertz.setJobName(resultSet.getString("JOBNAME"));
		    	quertz.setTriggerName(resultSet.getString("TRIGGERNAME"));
		    	quertz.setTriggerTime(resultSet.getString("TRIGGERTIME"));
		    	result.add(quertz);
		    }
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return result;
		} finally {
			try {
				if(resultSet != null) {
					resultSet.close();
				}
				if(preparedStatement != null) {
					preparedStatement.close();
				}
				if(connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return result;
		
	}
	
	/**
	 * 
	 * @param sql ���ҵ�sql
	 * @param feilds ���ҵ��ֶ����
	 * @return ��ݿ��н��
	 */
	public List<Map<String, String>> getMessageOfListFromDB(String sql, String[] feilds) {
		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Connection connection = getConnection();
		try {
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				Map<String, String> map = new HashMap<String, String>(100);
				for(int i=0; i<feilds.length; i++) {
					map.put(feilds[i], resultSet.getString(i+1));
				}
				result.add(map);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if(resultSet != null) {
					close(resultSet);
				}
				if(statement != null) {
					close(statement);
				}
				if(connection != null) {
					close(connection);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return result;
	}
	
	/**
	 * get data transform list from database
	 * @param sql ���ҵ�sql
	 * @param length �ֶγ���
	 * @param split �ָ��� 
	 * @return ��ݿ��н���
	 * @throws SQLException 
	 */
	public List<String> getMessageOfListFromDB(String sql, int length, String split) throws SQLException {
		List<String> result = new ArrayList<String>();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Connection connection = getConnection();
		try {
			statement = connection.prepareStatement(sql);
			logger.info("sql: " + sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				StringBuffer buffer = new StringBuffer("");
				for(int i=1; i<=length; i++) {
					String field = resultSet.getString(i);
					if(StringUtils.isBlank(field)) {
						buffer.append(split);
					} else {
						buffer.append(field + split);
					}
				
				}
				
				if(StringUtils.equals(split, "|")) {
					result.add(buffer.toString());
				} else {
					result.add(buffer.toString().substring(0, buffer.toString().length() - 1));
				}
				
			}
		}  finally {
			try {
				if(resultSet != null) {
					close(resultSet);
				}
				if(statement != null) {
					close(statement);
				}
				if(connection != null) {
					close(connection);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return result;
	}
	
	/**
	 * close connection
	 * @param object �رյ�����Ϣ
	 * @throws SQLException sql�쳣
	 */
	public void close(Object object) throws SQLException {
		if(object instanceof Connection) {
		    ((Connection) object).close();
		} else if(object instanceof PreparedStatement) {
			((PreparedStatement) object).close();
		} else if(object instanceof ResultSet) {
			((ResultSet) object).close();
		}
	}
	
	/**
	 * 
	 * @param sql sql���
	 * @param ipFlag ip�ı�ʶ
	 * @return ��ݿ��н��
	 */
	public Map<String, String> querySet(String sql, boolean ipFlag) {
		Connection connection = getConnection();
		PreparedStatement statement = null;
		ResultSet set = null;
		Map<String, String> map = new HashMap<String, String>(20);
		try {
			statement = connection.prepareStatement(sql);
			set  = statement.executeQuery();
			while(set.next()) {
				String flag = set.getString("flag");
				if(ipFlag) {
					String ip = set.getString("ip");
					String port = set.getString("port");
					String message = ip + "," + port + "," + flag;
					map.put(set.getString("jobName"), message);
				} else {
					map.put(set.getString("jobName"), flag);
				}
				
			}
			return map;
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}  finally {
			if(set != null) {
				try {
					set.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}
	
	/**
	 * get data from database
	 * @param sql ȡ���sql
	 * @return list of map
	 */
	public List<Map<String, Object>> getDataFromDB(String sql) {
		List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
		DBUtil dbUtil = new DBUtil();
		Connection connection = dbUtil.getConnection();
		PreparedStatement statement = null;
		ResultSet set = null;
		try {
			statement = connection.prepareStatement(sql);
			set = statement.executeQuery();
			while(set.next()) {
				Map<String, Object> map = new HashMap<String, Object>(20);
				ResultSetMetaData data = set.getMetaData();
				int count = data.getColumnCount();
				for(int i=1; i<=count; i++) {
					String key = data.getColumnName(i);
					Object value = set.getObject(key);
					map.put(key, value);
				}
				lists.add(map);
			}
		
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if(set != null) {
				try {
					set.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(statement != null) {
				try {
					statement.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			if(connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return lists;
	} 
}

