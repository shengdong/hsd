package com.stsing.util;



import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * Created by k1160 on 2016/2/18.
 */
public class XMLTransform {
	/**
	 *  declare xmlTemplateFolder
	 */
    private static String xmlTemplateFolder;
    
    /**
     *  declare xmlResultFolder
     */
    private static String xmlResultFolder;
    
    /**
     *  declare logger
     */
    private static Logger logger = Logger.getLogger(XMLTransform.class);

    /**
     * 初始化xmlTemplateFolder和xmlResultFolder
     */
    static {
        xmlTemplateFolder = SocketConfigUtil.getValue("xmlTemplateFolder");
        xmlResultFolder = SocketConfigUtil.getValue("xmlResultFolder");
    }

    /**
     * convert specified format xml to sql String , ten quote supported
     *
     * @param filePath 文件路径
     * @param xsltName 模板的名名称
     * @return 要入库的数据
     */
    public static LinkedList<String> XMLtransql(String filePath, String xsltName) {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(new File(xmlTemplateFolder + xsltName));
        Transformer transformer;
        try {
            transformer = factory.newTransformer(xslt);
            Source text = new StreamSource(new File(filePath));

            String outFilePath = xmlResultFolder + UUID.randomUUID() + ".xml";
            transformer.transform(text, new StreamResult(new File(outFilePath)));

            return resultTempsql(outFilePath);

        } catch (TransformerConfigurationException e) {
        	logger.error(e.getMessage(), e);
            return null;
        } catch (TransformerException e) {
        	logger.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * convert specified format xml to sql String , ten quote supported
     *
     * @param resultTempPath 转换后文件地址
     * @return 要入库的数据信息
     */
	@SuppressWarnings("unchecked")
	private static LinkedList<String> resultTempsql(String resultTempPath){
        Document doc;
        LinkedList<String> sqls = new LinkedList<String>();
        try {
            doc = new SAXReader().read(new File(resultTempPath));
            Element root = doc.getRootElement();
            Iterator<Element> it = root.elementIterator();
            while (it.hasNext()) {
                Element sql = (Element) it.next();
                sqls.add(sql.getTextTrim());
            }
            return sqls;

        } catch (DocumentException e) {
        	logger.error(e.getMessage());
            return null;
        }
    }
}
