package com.stsing.util;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 
 * @author k1193
 *
 */
public class XmlConfigUtil {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(XmlConfigUtil.class);

	/**
	 * declare result map of XML
	 */
	private static Map<String, String> resultMapOfXml = null;

	/**
	 * declare file path
	 */
//	private static final String FILEPATHXML = "../conf/Config.xml";
	
	private static final String FILEPATHXML = "src/conf/Config.xml";

	
	/**
	 * @param name key
	 * @return declare get value of List
	 */
	public static Map<String, String> getValueOfList(String name) {
		if (resultMapOfXml == null) {
			initXmlMap();
		}
		Map<String, String> result = new HashMap<String, String>();
		for(String key : resultMapOfXml.keySet()) {
			if(key.startsWith(name)) {
				result.put(key.split("-")[1], resultMapOfXml.get(key));
			}
		}
		return result;
	}
	
	/**
	 * get value by key
	 * @param key ���ҵ�key
	 * @return ���ҵ�value
	 */
	public static String getValue(String key) {
		if(resultMapOfXml == null) {
			initXmlMap();
		}
		
		return resultMapOfXml.get(key);
		
	}

	/**
	 * inti xml map
	 */
	@SuppressWarnings("unchecked")
	private static void initXmlMap() {
		try {
			resultMapOfXml = new HashMap<String, String>();
			SAXReader reader = new SAXReader();
			File file = new File(FILEPATHXML);
			Document document = reader.read(file);
			Element root = document.getRootElement();
			List<Element> childElement = root.elements();
			for (Element child : childElement) {
				String value = child.attributeValue("name");
				resultMapOfXml.put(value, child.getTextTrim());
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
