package com.stsing.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.model.Quertz;

/**
 * 
 * @author s.he
 *
 */
public class QuertzConfigUtil {
	/**
	 *  declare logger
	 */
	private static final Logger logger = Logger.getLogger(QuertzConfigUtil.class);
	

	private static final String FILEPATH = "src/conf/Config.ini";
	
	/**
	 *  declare file path
	 */
//	private static final String FILEPATH = "../conf/Config.ini";
	
	/**
	 *  declare configMap
	 */
	private static Map<String, String> configMap = null;
	
	/**
	 * declare configuration
	 */
	private static HierarchicalINIConfiguration configuration = null;
	
	
	/**
	 * get value
	 * @param key ���ҵ�key
	 * @return ���ҵ�value
	 */
	public static String getValue(String key) {
		if(configMap == null) {
			initMap();
		}
		return configMap.get(key);
	}
	
	/**
	 * get value of int
	 * @param key ���ҵ�key
	 * @return ����value
	 */
	public static int getIntValue(String key){
		try {
			int reslut = Integer.parseInt(getValue(key));
			return reslut;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return 0;
		}
	}
	
	/**
	 * inti map
	 */
	private static void initMap() {
		configMap = new HashMap<String, String>();
		Properties props = new Properties();
		 FileInputStream inputFile;
		 try {
			 inputFile = new FileInputStream(FILEPATH);
			 props.load(inputFile);
			 Enumeration<Object> keys =  props.keys();
			 while(keys.hasMoreElements()) {
				 String key = (String) keys.nextElement();
				 String value = (String) props.get(key);
				 value = new String(value.getBytes("iso-8859-1"), "gbk");
				 configMap.put(key, value);
			 }
			 
			 
		 } catch (Exception e) {
			 logger.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @param prefix ǰ׺
	 * @return sql�б�
	 */
	public static List<String> getSQLList(String prefix) {
		List<String> result = new ArrayList<String>();
		if(configMap == null) {
			initMap();
		}
		
		for(String key: configMap.keySet()) {
			if(key.startsWith(prefix)) {
				result.add(configMap.get(key));
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @return ���ص�quertz����Ϣ
	 */
	public static List<Quertz> getListMapOfJobFromDB() {
		DBUtil dbUtil = new DBUtil();
		List<Quertz> result = dbUtil.getJObOfListMapFromDB();
		return result;
	}
	
	/**
	 * 
	 * @param sections  �ڵ����
	 * @param key �ڵ������key
	 * @return GET VALUE BY SECTIONS AND KEY
	 */
	public static String getValue(String sections, String key) {
		try {
			if(configuration == null) {
				configuration = new HierarchicalINIConfiguration(FILEPATH);
			}
			Set<String> setOfSections = configuration.getSections();
			Iterator<String> sectionNames = setOfSections.iterator();
			while(sectionNames.hasNext()) {
				String seactionName = sectionNames.next();
				if(StringUtils.equals(sections, seactionName)) {
					return configuration.getSection(seactionName).getString(key);
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}
	
	/**
	 * @param sections �ڵ����
	 * @return get map from sections
	 */
	public static Map<String, String> getMapBySections(String sections) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			if(configuration == null) {
				configuration = new HierarchicalINIConfiguration(FILEPATH);
			}
			SubnodeConfiguration subnodeConfiguration = configuration.getSection(sections);
			Iterator<String> inIterator = subnodeConfiguration.getKeys();
			String key = "";
			while(inIterator.hasNext()) {
				key = inIterator.next().trim();
				result.put(key, subnodeConfiguration.getString(key));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	public static void main(String[] args) {
		Map<String, String> map = getMapBySections("lcfile2");
		for(String key : map.keySet()) {
			System.out.println(key + "; " + map.get(key));
		}
	} 
}
