package com.stsing.client;


import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.FixedReceiveBufferSizePredictorFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.string.StringEncoder;

import com.stsing.util.SocketConfigUtil;


/**
 * Netty框架
 * @author k1193
 *
 */
public class SocketServer {
	/**
	 *  declare logger
	 */
	private static final Logger logger = Logger.getLogger(SocketServer.class);
	
	/**
	 *  declare ip key
	 */
	private static final String IP = "ip";
	
	/**
	 *  declare port key
	 */
	private static final String PORT = "port";
	
	
	private ServerBootstrap bootstrap;
	

	/**
	 * begin to run 
	 */
	public void run() {
		bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(
				Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			@Override
			public ChannelPipeline getPipeline() throws Exception {
				ChannelPipeline pipeline = Channels.pipeline();
				pipeline.addLast("decoder",  new StringEncoder());
				pipeline.addLast("encoder", new StringEncoder());
				pipeline.addLast("handler", new ServerHanderDispatcher());
				return pipeline;
			}
		});
		
		bootstrap.setOption("child.tcpNoDelay", true);
		bootstrap.setOption("child.keepAlive", true);
		bootstrap.setOption("receiveBufferSizePredictorFactory", 
				new FixedReceiveBufferSizePredictorFactory(65535));
		
		try {
			String ip = SocketConfigUtil.getValue(IP);
			int port = SocketConfigUtil.getValueOfInt(PORT);
			bootstrap.bind(new InetSocketAddress(ip, port));
			logger.info("ip: " + ip + " port : " + port);
		} catch (Exception e) { 
			logger.error(e.getMessage(), e);
		}
	}
	
	
	public void stop() {
		try {
			bootstrap.shutdown();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
