package com.stsing.client;

import org.apache.log4j.Logger;

/**
 * 
 * @author k1193
 *
 */
public class Main {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(Main.class);
	
	private  BootStrap strap = null;
	
//	private SocketServer server = null;
	
	public Main() {
		strap = BootStrap.getInstance();
//		server = new SocketServer();
	}

	/**
	 * run code
	 */
	public void run() {
		logger.info("begin to execute bootstrap ");
		strap.execute();
//		server.run();
	}
	
	public void stop() {
		strap.stop();
//		server.stop();
	}
}
