package com.stsing.provide.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.util.DBUtil;

public class ProvideClFile extends ProvideAbstract {
	private static final Logger logger = Logger.getLogger(ProvideClFile.class);
	
	private String section;
	
	private DBUtil dbutil = null;
	
	
	public ProvideClFile(String section) {
		this.section = section;
	}
	
	@Override
	public String getFileSplit() {
		return "|";
	}

	@Override
	public String getFileType() {
		return ".txt";
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public List<String> getDataFromOracle(String sql) {
		List<String> result = new ArrayList<>(2000);
		int number = Integer.parseInt(sql.split("@")[1]);
		String sqlString = sql.split("@")[0];
		logger.info("sql hsd : " + sqlString);
		dbutil = DBUtil.getIntance();
		Connection connection = dbutil.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		StringBuilder builder = new StringBuilder();
		try {
			statement = connection.prepareStatement(sqlString);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				for (int i = 1; i <= number; i++) {
					String field = resultSet.getString(i);
					if(StringUtils.isBlank(field)) {
						builder.append(getFileSplit());
					} else {
						builder.append(field + getFileSplit());
					}
				}
				result.add(builder.toString());
				builder.delete(0, builder.toString().length());
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			try {
				if (statement != null) {
					statement.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			try {
				if (resultSet != null) {
					resultSet.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		ProvideAbstract provide = new ProvideClFile("lcfile5");
		provide.beginToCreateFile();
	}
}
