package com.stsing.provide.test;

import java.util.List;
import java.util.Map;

public interface ProvideTemplate {

	public List<String> getDataFromOracle(String sql);
	
	public boolean createFileToPath(List<String> dataSet, String titleData);
	
	public String getFileName();
	
	public String getFilePath();
	
	public String getFileSplit();
	
	public String getFileType();
	
	public boolean createAllFile(List<String> sqls, Map<String, String> titleDatas);
	
	public boolean createOKFile();
	
	public String getSection();
}
