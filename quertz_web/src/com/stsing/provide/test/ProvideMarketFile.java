package com.stsing.provide.test;

import org.apache.log4j.Logger;

public class ProvideMarketFile extends ProvideAbstract {
	private static final Logger logger = Logger.getLogger(ProvideMarketFile.class);
	
	private String section;
	
	
	public ProvideMarketFile(String section) {
		this.section = section;
	}

	@Override
	public String getFileSplit() {
		return ",";
	}

	@Override
	public String getFileType() {
		return ".csv";
	}

	@Override
	public String getSection() {
		return section;
	}

	public static void main(String[] args) {
		logger.info("begin to provide marketFile");
		
		ProvideAbstract provideAbstract = new ProvideMarketFile("marketfile");
		
		provideAbstract.beginToCreateFile();
	}
}
