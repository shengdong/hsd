package com.stsing.provide.job;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.stsing.model.Info;
import com.stsing.util.ErrorDBHandler;

public class ProvideSummit implements ProvideJob {

	@Override
	public List<Info> provideToMessage(String jobName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getTrigger().getJobKey().getName();
		List<Info> result = provideToMessage(jobName);
		ErrorDBHandler.insertError(result);
	}

}
