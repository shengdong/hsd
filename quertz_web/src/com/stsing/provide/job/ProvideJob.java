package com.stsing.provide.job;

import java.util.List;

import org.quartz.Job;

import com.stsing.model.Info;

/**
 * 
 * @author k1193
 *
 */
public interface ProvideJob extends Job {
	
	/**
	 * provide to downStram
	 * @param jobName 任务名称
	 * @return 供数的信息
	 */
	public List<Info> provideToMessage(String jobName);

}
