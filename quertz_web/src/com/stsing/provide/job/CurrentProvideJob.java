package com.stsing.provide.job;

import java.io.File;

import org.apache.log4j.Logger;

import com.stsing.util.QuertzConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class CurrentProvideJob {
	
	/**
	 * declare
	 */
	private static final Logger logger = Logger.getLogger(CurrentProvideJob.class);
	
	
	/**
	 * 
	 * @param jobName ��������
	 */
	public static void provideData(String jobName) {
		logger.info("to providing current data");
        String path = QuertzConfigUtil.getValue("filepath");
		logger.info("begin to get data from oracle marketfile");
		String section = QuertzConfigUtil.getValue(jobName);
		String filePath = path + File.separator + section;
		String split = "|";
		String fileType = ".txt";
		logger.info("hsd : " + filePath);
		ProvideDBHandler.provideMsgFromOracle(section, filePath, split, fileType);
	}
}
