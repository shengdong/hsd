package com.stsing.provide.job;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.stsing.model.Info;
import com.stsing.util.DBUtil;
import com.stsing.util.QuertzConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class ProvideDBHandler {
	/**
	 *  declare logger
	 */
	private static final Logger logger = Logger.getLogger(ProvideDBHandler.class);
	

	
	/**
	 * ���⹩���
	 * @param sections �ڵ����
	 * @param path �ļ�·��
	 * @param split �ָ���
	 * @param fileType �ļ�����
	 * @return ���ع���Ľ��
	 */
	public static List<Info> provideMsgFromOracle(String sections, String path, String split, String fileType) {
		List<Info> result = new ArrayList<Info>();
		try {
			Map<String, String> map = QuertzConfigUtil.getMapBySections(sections);
			logger.info("begin to get message from dataBase");
			List<String> lists = getListFromMapOfSql(map);
			DBUtil dbUtil = new DBUtil();
			String filePath =path+ File.separator + getToday();
			makeDir(filePath);
			for (String sql : lists) {
				String trueSql = sql.split("@")[0].trim();
				int length = Integer.parseInt(sql.split("@")[1].trim());
				saveToFile(dbUtil.getMessageOfListFromDB(trueSql, length, split), trueSql, filePath, fileType, sections);
			}
			logger.info("all save to file success !!!!!!!!!!, begin to mkdir OK file");
			File fileOK = new File(filePath + File.separator + QuertzConfigUtil.getValue("fileFlag") + "_" + getToday() + ".OK");
			if(!fileOK.exists()) {
		    	logger.info(fileOK);
				fileOK.createNewFile();
				
			}
			Info info = new Info();
			info.setObject(sections);
			info.setSuccess(true);
			result.add(info);
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Info info = new Info();
			info.setObject(sections);
			info.setSuccess(true);
			result.add(info);
			return result;
		}
	}
	
	/**
	 * ���⹩���
	 * @param map map���
	 * @param path �ļ�·��
	 * @return ��������Ϣ
	 */
	public static List<Info> provideMsgFromOracle(Map<String, String> map, String path){
		List<Info> result = new ArrayList<Info>();
		try {
			logger.info("begin to get message from dataBase");
			DBUtil dbUtil = new DBUtil();
			String trueSql = "";
			String filePath = path + File.separator + getToday();
			makeDir(filePath);
			int length = 0;
			for (String key : map.keySet()) {
	            String value = map.get(key);
	            String fileName = key.trim() + "_" + getToday();
				trueSql = value.split("@")[0].trim();
				length = Integer.parseInt(value.split("@")[1].trim());
				saveToFileOFXMl(dbUtil.getMessageOfListFromDB(trueSql, length, ","), fileName, filePath);
			}
			logger.info("all save to file success !!!!!!!!!!, begin to mkdir OK file");
			File fileOK = new File(filePath + File.separator + QuertzConfigUtil.getValue("fileFlag") + "_" + getToday() + ".OK");
			if(!fileOK.exists()) {
				fileOK.createNewFile();
			}
			Info info = new Info();
			info.setObject("CMDS");
			info.setSuccess(true);
			result.add(info);
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Info info = new Info();
			info.setObject("CMDS");
			info.setSuccess(false);
			result.add(info);
			return result;
			
		}
			
	}
	
	
	
	/**
	 * get SQL from map
	 * @param map �õ�map����Ϣ
	 * @return ����sql�б�
	 */
	private static List<String> getListFromMapOfSql(Map<String, String> map) {
		List<String> result = new ArrayList<String>();
		
		for(String key: map.keySet()) {
			if(key.startsWith("sql")) {
				result.add(map.get(key));
			}
		}
		return result;
		
	}
	

	/**
	 * save to file
	 * @param list �ļ����
	 * @param trueSql select sql���
	 * @param filePath �ļ�·��
	 * @param fileType �ļ�����
	 * @param sections �ڵ����
	 * @throws IOException  ϵͳ���쳣
	 */
	private static void saveToFile(List<String> list, String trueSql, String filePath, String fileType, String sections) throws IOException {
		FileOutputStream fileOutputStream = null;
		BufferedWriter writer = null;
		try {
			String tableName = trueSql.split("from")[1].trim().split("where")[0].trim();
			String fileName = getXmlName(tableName);
			String firstLine = QuertzConfigUtil.getValue(sections, tableName);
			if(fileName.startsWith("WIND")) {
				fileName = "NBCB_" + fileName;
			}
			fileName = fileName.replaceAll("DSS", "NBCB");
			String xmlFlePath = filePath + File.separator + fileName + fileType;
			logger.info("save file : " + xmlFlePath);
			fileOutputStream = new FileOutputStream(xmlFlePath);
			writer = new BufferedWriter(
					new OutputStreamWriter(fileOutputStream));
			writer.write(firstLine);
			writer.newLine();
			for (String line : list) {
				writer.write(line);
				writer.newLine();
			}
			logger.info("save message success!!!");

		} finally {
			try {
				if(writer != null) {
					writer.close();
				}
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * Save to file of XML
	 * @param list �������
	 * @param fileName �ļ����
	 * @param filePath �ļ�·��
	 */
	private static void saveToFileOFXMl(List<String> list, String fileName, String filePath) {
		FileOutputStream fileOutputStream = null;
		BufferedWriter writer = null;
		try {
			String xmlFlePath = filePath + File.separator + fileName + ".csv";
			logger.info("save file : " + xmlFlePath);
			fileOutputStream = new FileOutputStream(xmlFlePath);
			writer = new BufferedWriter(
					new OutputStreamWriter(fileOutputStream));
			for (String line : list) {
				writer.write(line);
				writer.newLine();
			}
			logger.info("save message success!!!");

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if(writer != null) {
					writer.close();
				}
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * get XML name
	 * @param tableName ������
	 * @return �ļ������
	 */
	private static String getXmlName(String tableName) {
		return tableName + "_" + getToday();
	}
	
	/**
	 * @return get today by String 
	 */
	private static String getToday() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(new Date());
	}
	
	/**
	 * if today DIR is not exists, create DIR
	 * @param filedate �ļ����
	 */
	private static void makeDir(String filedate) {
		File file = new File(filedate);
		if(!file.exists()) {
			logger.info("begin to mkdir today path");
			file.mkdir();
		} else {
			logger.info("exists dir!!!");
		}
	}
}
