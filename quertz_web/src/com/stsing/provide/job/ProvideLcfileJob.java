package com.stsing.provide.job;

import java.io.File;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.stsing.model.Info;
import com.stsing.util.DBUtil;
import com.stsing.util.ErrorDBHandler;
import com.stsing.util.QuertzConfigUtil;
import common.Logger;

/**
 * 
 * @author k1193
 *
 */
public class ProvideLcfileJob implements ProvideJob{
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(ProvideLcfileJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getTrigger().getJobKey().getName();
		List<Info> result = provideToMessage(jobName);
		ErrorDBHandler.insertError(result);
		
	}

	@Override
	public List<Info> provideToMessage(String jobName) {
		String flag = new DBUtil().querySet("select jobname, flag from quertz t where ip is null", false).get(jobName);
		String path = QuertzConfigUtil.getValue("filepath");
		logger.info("begin to get data from oracle marketfile");
		String section = flag;
		String filePath = path + File.separator + section;
		String split = "|";
		String fileType = ".txt";
		return ProvideDBHandler.provideMsgFromOracle(section, filePath, split, fileType);
	}
	
	public static void main(String[] args) throws Exception {
		new ProvideLcfileJob().execute(null);
	}
}
