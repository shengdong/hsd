package com.stsing.provide.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.stsing.jedis.JedisClusterUtil;
import com.stsing.jedis.JedisSingleUtil;
import com.stsing.jedis.JedisUtil;
import com.stsing.util.ConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class ProvideSHJob implements Job {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(ProvideSHJob.class);
	
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			JedisUtil jedis = null;
			String flag = ConfigUtil.getString("redis.flag");
			
			if(StringUtils.equals(flag, JedisUtil.CLUSTER)) {
				jedis = new JedisClusterUtil();
			} else if(StringUtils.equals(flag, JedisUtil.JEDIS)) {
				jedis = new JedisSingleUtil();
			}
			
			if(jedis == null || !jedis.ping()) {
				logger.error("redis is shut down , please to checkout redis");
				return;
			}
			
	        List<Map<String, String>> lists = new ArrayList<Map<String, String>>();
	        Set<String> sets = jedis.smembers("SZSE");
	        
	        lists = jedis.piplined(sets);
	        if(jedis != null) {
	        	jedis.close();
	        }
	       SHSDB.getInstance().toDB(lists);
	        
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
}
