package com.stsing.provide.job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.stsing.util.DBUtil;
import com.stsing.util.GsonUtil;
import com.stsing.util.SocketConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class SHSDB {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(SHSDB.class);

	/**
	 * declare shsdb
	 */
	private static SHSDB shsdb = null;

	/**
	 * 
	 * @return instance of SHSDB
	 */
	public static SHSDB getInstance() {
		if (shsdb == null) {
			shsdb = new SHSDB();
		}
		return shsdb;
	}

	/**
	 * 
	 * @param dataLists �������
	 */
	@SuppressWarnings("unchecked")
	public void toDB(List<Map<String, String>> dataLists) {
		List<Map<String, String>> reslutLists = new ArrayList<Map<String, String>>(
				5000);

		Map<String, String> mp = SocketConfigUtil.getshortNameMap();
		for (int j = 0; j < dataLists.size(); j++) {
			Map<String, String> result = new HashMap<String, String>(20);
			Map<String, String> m = dataLists.get(j);
			for (String k : m.keySet()) {
				if (StringUtils.equals("securityId", k)) {
					result.put("SECID", m.get(k));
					String value = mp.get(m.get(k));
					result.put("SHORTNAME", value);
				}
				if (StringUtils.equals("prevClosePx", k)) {
					result.put("LAST_CLOSING_PRICE", getFlaotData(m.get(k), 4));
				}
				if (StringUtils.equals("numTrades", k)) {
					result.put("DEAL_COUNT", m.get(k));
				}
				if (StringUtils.equals("totalVolumeTrade", k)) {
					result.put("DEAL_QUANTITY", getFlaotData(m.get(k), 2));
				}
				if (StringUtils.equals("totalValueTrade", k)) {
					result.put("DEAL_AMOUNT",getFlaotData(m.get(k), 4));
				}
				if (StringUtils.equals("NoMDEntries", k)) {
					String dataString = m.get(k);
					List<String> lists = GsonUtil.getInstance().fromJson(
							dataString, List.class);
					for (String s : lists) {
						String[] datas = s.split(",");
						datas[1] = getFlaotData(datas[1], 6);
						datas[2] = getFlaotData(datas[2], 2);
						if (StringUtils.equals(datas[0], "2")) {
							result.put("LATEST_DEAL_PRICE", datas[1]);
						}
						if (StringUtils.equals(datas[0], "4")) {
							result.put("OPENING_PRICE", datas[1]);
						}
						if (StringUtils.equals(datas[0], "7")) {
							result.put("HIGHEST_PRICE", datas[1]);
						}
						if (StringUtils.equals(datas[0], "8")) {
							result.put("LOWEST_PRICE", datas[1]);
						}
						if (StringUtils.equals(datas[0], "x1")) {
							result.put("PRICE_FLOAT1", datas[1]);
						}
						if (StringUtils.equals(datas[0], "x2")) {
							result.put("PRICE_FLOAT2", datas[1]);
						}
						if (StringUtils.equals(datas[0], "x5")) {
							result.put("EARNINGS_MULTIPLE1", datas[1]);
						}
						if (StringUtils.equals(datas[0], "x6")) {
							result.put("EARNINGS_MULTIPLE2", datas[1]);
						}
						if (StringUtils.equals(datas[0], "xg")) {
							result.put("POSITION", datas[1]);
						}
						// sale in
						if (StringUtils.equals(datas[0], "0")) {
							if (!StringUtils.equals(datas[3], "0")) {
								result.put("BUY_PRICE" + datas[3], datas[1]);
								result.put("BUY_QUANTITY" + datas[3], datas[2]);
							}
						}
						// sale out
						if (StringUtils.equals(datas[0], "1")) {
							if (!StringUtils.equals(datas[3], "0")) {
								result.put("SOLD_PRICE" + datas[3], datas[1]);
								result.put("SOLD_QUANTITY" + datas[3], datas[2]);
							}
						}
					}
				}
			}
			reslutLists.add(result);

		}
		insertOrUpdateDB(reslutLists);
	}

	/**
	 * insert into DB
	 * 
	 * @param reslutLists Ҫ�������
	 * @return �Ƿ�ɹ����
	 */
	private boolean insertIntoDB(List<Map<String, String>> reslutLists) {
		DBUtil dbUtil = new DBUtil();
		Connection connection = dbUtil.getConnection();
		PreparedStatement statement = null;
		try {
			connection.setAutoCommit(false);
			String insertSql = insertSql();
			statement = connection.prepareStatement(insertSql);
			for (Map<String, String> map : reslutLists) {
				statement.setString(1, map.get("SECID"));
				statement.setString(2, map.get("SHORTNAME"));
				statement.setString(3, map.get("LAST_CLOSING_PRICE"));
				statement.setString(4, map.get("OPENING_PRICE"));
				statement.setString(5, map.get("LATEST_DEAL_PRICE"));
				statement.setString(6, map.get("DEAL_QUANTITY"));
				statement.setString(7, map.get("DEAL_AMOUNT"));
				statement.setString(8, map.get("DEAL_COUNT"));
				statement.setString(9, map.get("HIGHEST_PRICE"));
				statement.setString(10, map.get("LOWEST_PRICE"));
				statement.setString(11, map.get("EARNINGS_MULTIPLE1"));
				statement.setString(12, map.get("EARNINGS_MULTIPLE2"));
				statement.setString(13, map.get("PRICE_FLOAT1"));
				statement.setString(14, map.get("PRICE_FLOAT2"));
				statement.setString(15, map.get("POSITION"));
				statement.setString(16, map.get("SOLD_PRICE5"));
				statement.setString(17, map.get("SOLD_QUANTITY5"));
				statement.setString(18, map.get("SOLD_PRICE4"));
				statement.setString(19, map.get("SOLE_QUANTITY4"));
				statement.setString(20, map.get("SOLD_PRICE3"));
				statement.setString(21, map.get("SOLD_QUANTITY3"));
				statement.setString(22, map.get("SOLD_PRICE2"));
				statement.setString(23, map.get("SOLD_QUANTITY2"));
				statement.setString(24, map.get("SOLD_PRICE1"));
				statement.setString(25, map.get("SOLD_QUANTITY1"));
				statement.setString(26, map.get("BUY_PRICE1"));
				statement.setString(27, map.get("BUY_QUANTITY1"));
				statement.setString(28, map.get("BUY_PRICE2"));
				statement.setString(29, map.get("BUY_QUANTITY2"));
				statement.setString(30, map.get("BUY_PRICE3"));
				statement.setString(31, map.get("BUY_QUANTITY3"));
				statement.setString(32, map.get("BUY_PRICE4"));
				statement.setString(33, map.get("BUY_QUANTITY4"));
				statement.setString(34, map.get("BUY_PRICE5"));
				statement.setString(35, map.get("BUY_QUANTITY5"));
				statement.addBatch();
			}
			statement.executeBatch();
			connection.commit();

			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			try {
				connection.rollback();
				return false;
			} catch (SQLException e1) {
				logger.info(e1.getMessage(), e1);
				return false;
			}
		} finally {
			if (statement != null) {
				try {
					dbUtil.close(statement);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}

				if (connection != null) {
					try {
						dbUtil.close(connection);
					} catch (SQLException e) {
						logger.error(e.getMessage(), e);
					}
				}

			}
		}

	}

	/**
	 * 
	 * @return get insert SQL
	 */
	private String insertSql() {
		return "insert into CDMSZ_SJSHQ(SECID, SHORTNAME, LAST_CLOSING_PRICE, "
				+ "OPENING_PRICE, LATEST_DEAL_PRICE, DEAL_QUANTITY, DEAL_AMOUNT, "
				+ "DEAL_COUNT, HIGHEST_PRICE, LOWEST_PRICE, EARNINGS_MULTIPLE1, "
				+ "EARNINGS_MULTIPLE2, PRICE_FLOAT1, PRICE_FLOAT2, POSITION,"
				+ " SOLD_PRICE5, SOLD_QUANTITY5, SOLE_PIRCE4, SOLE_QUANTITY4, "
				+ "SOLE_PRICE3, SOLD_QUANTITY3, SOLD_PRICE2, SOLD_QUANTITY2,"
				+ " SOLD_PRICE1, SOLD_QUANTITY1, BUY_PRICE1, BUY_QUANTITY1, BUY_PRICE2,"
				+ " BUY_QUANTITY2, BUY_PRICE3, BUY_QUANTITY3, BUY_PRICE4, BUY_QUANTITY4, "
				+ "BUY_PRICE5, BUY_QUANTITY5, POSTDATE) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate)";
	}

	/**
	 * 
	 * @param data ���
	 * @param index ����
	 * @return ������λ������
	 */
	private static String getFlaotData(String data, int index) {
		if(StringUtils.isBlank(data) || data == null) {
			return "";
		}
		double num = 1.0;
		for(int i=0; i<index; i++) {
			num = num * 10;
		}
		double d = Double.parseDouble(data) ;
		double t = d / num;
		return t + "";
	}
	
	/**
	 * if secid is exist, update it, or insert
	 * @param reslutLists Ҫ�������
	 * @return �Ƿ�ɹ����
	 */
	private boolean insertOrUpdateDB(List<Map<String, String>> reslutLists) {
		logger.info(reslutLists.size());
		List<Map<String, String>> insertLists = new LinkedList<Map<String,String>>();
		List<Map<String, String>> updateLists = new LinkedList<Map<String,String>>();
		
		List<String> lists = SocketConfigUtil.getSJHQID();
		
		for(Map<String, String> map : reslutLists) {
			String secid = map.get("SECID");
			if(lists.contains(secid)) {
				updateLists.add(map);
			} else {
				insertLists.add(map);
			}
		}
		logger.error("�²�������" + insertLists.size());
		logger.error("���µ����" + updateLists.size());
		return insertIntoDB(insertLists) && updateIntoDB(updateLists);
	}
	
	/**
	 * update database if data is exists
	 * @param reslutLists ���µ����
	 * @return �Ƿ�ɹ�����
	 */
	private boolean updateIntoDB(List<Map<String, String>> reslutLists) {
		DBUtil dbUtil = new DBUtil();
		Connection connection = dbUtil.getConnection();
		PreparedStatement statement = null;
		try {
			connection.setAutoCommit(false);

			String updateSql = updateSql();
			statement = connection.prepareStatement(updateSql);
			for (Map<String, String> map : reslutLists) {
				statement.setString(1, map.get("SHORTNAME"));
				statement.setString(2, map.get("LAST_CLOSING_PRICE"));
				statement.setString(3, map.get("OPENING_PRICE"));
				statement.setString(4, map.get("LATEST_DEAL_PRICE"));
				statement.setString(5, map.get("DEAL_QUANTITY"));
				statement.setString(6, map.get("DEAL_AMOUNT"));
				statement.setString(7, map.get("DEAL_COUNT"));
				statement.setString(8, map.get("HIGHEST_PRICE"));
				statement.setString(9, map.get("LOWEST_PRICE"));
				statement.setString(10, map.get("EARNINGS_MULTIPLE1"));
				statement.setString(11, map.get("EARNINGS_MULTIPLE2"));
				statement.setString(12, map.get("PRICE_FLOAT1"));
				statement.setString(13, map.get("PRICE_FLOAT2"));
				statement.setString(14, map.get("POSITION"));
				statement.setString(15, map.get("SOLD_PRICE5"));
				statement.setString(16, map.get("SOLD_QUANTITY5"));
				statement.setString(17, map.get("SOLD_PRICE4"));
				statement.setString(18, map.get("SOLE_QUANTITY4"));
				statement.setString(19, map.get("SOLD_PRICE3"));
				statement.setString(20, map.get("SOLD_QUANTITY3"));
				statement.setString(21, map.get("SOLD_PRICE2"));
				statement.setString(22, map.get("SOLD_QUANTITY2"));
				statement.setString(23, map.get("SOLD_PRICE1"));
				statement.setString(24, map.get("SOLD_QUANTITY1"));
				statement.setString(25, map.get("BUY_PRICE1"));
				statement.setString(26, map.get("BUY_QUANTITY1"));
				statement.setString(27, map.get("BUY_PRICE2"));
				statement.setString(28, map.get("BUY_QUANTITY2"));
				statement.setString(29, map.get("BUY_PRICE3"));
				statement.setString(30, map.get("BUY_QUANTITY3"));
				statement.setString(31, map.get("BUY_PRICE4"));
				statement.setString(32, map.get("BUY_QUANTITY4"));
				statement.setString(33, map.get("BUY_PRICE5"));
				statement.setString(34, map.get("BUY_QUANTITY5"));
				statement.setString(35, map.get("SECID"));
				statement.addBatch();
			}
			statement.executeBatch();
			connection.commit();

			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			try {
				connection.rollback();
				return false;
			} catch (SQLException e1) {
				logger.info(e1.getMessage(), e1);
				return false;
			}
		} finally {
			if (statement != null) {
				try {
					dbUtil.close(statement);
				} catch (SQLException e) {
					logger.error(e.getMessage(), e);
				}

				if (connection != null) {
					try {
						dbUtil.close(connection);
					} catch (SQLException e) {
						logger.error(e.getMessage(), e);
					}
				}

			}
		}

	}
	
	/**
	 * 
	 * @return ���ظ��µ�sql���
	 */
	private String updateSql() {
		return "update CDMSZ_SJSHQ set SHORTNAME=?, LAST_CLOSING_PRICE=?, "
				+ "OPENING_PRICE=?, LATEST_DEAL_PRICE=?, DEAL_QUANTITY=?, DEAL_AMOUNT=?, "
				+ "DEAL_COUNT=?, HIGHEST_PRICE=?, LOWEST_PRICE=?, EARNINGS_MULTIPLE1=?, "
				+ "EARNINGS_MULTIPLE2=?, PRICE_FLOAT1=?, PRICE_FLOAT2=?, POSITION=?,"
				+ " SOLD_PRICE5=?, SOLD_QUANTITY5=?, SOLE_PIRCE4=?, SOLE_QUANTITY4=?, "
				+ "SOLE_PRICE3=?, SOLD_QUANTITY3=?, SOLD_PRICE2=?, SOLD_QUANTITY2=?,"
				+ " SOLD_PRICE1=?, SOLD_QUANTITY1=?, BUY_PRICE1=?, BUY_QUANTITY1=?, BUY_PRICE2=?,"
				+ " BUY_QUANTITY2=?, BUY_PRICE3=?, BUY_QUANTITY3=?, BUY_PRICE4=?, BUY_QUANTITY4=?, "
				+ "BUY_PRICE5=?, BUY_QUANTITY5=?, POSTDATE=sysdate  where SECID=?";
	}
}
