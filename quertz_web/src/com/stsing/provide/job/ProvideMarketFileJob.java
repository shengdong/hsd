package com.stsing.provide.job;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.stsing.model.Info;
import com.stsing.util.DBUtil;
import com.stsing.util.ErrorDBHandler;
import com.stsing.util.QuertzConfigUtil;
import com.stsing.util.XmlConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class ProvideMarketFileJob implements ProvideJob {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(ProvideMarketFileJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getTrigger().getJobKey().getName();
		List<Info> result = provideToMessage(jobName);
		
		ErrorDBHandler.insertError(result);
		
	}
	
	@Override
	public List<Info> provideToMessage(String jobName) {
		String flag = new DBUtil().querySet("select jobname, flag from quertz t where ip is null", false).get(jobName);
		logger.info("begin to get data from oracle marketfile");
		String path = QuertzConfigUtil.getValue("filepath");
		String section = flag;
		String filePath = path + File.separator + section ;
		String split = ",";
		String fileType = ".csv";
		List<Info> resList = ProvideDBHandler.provideMsgFromOracle(section, filePath, split, fileType);
		
		logger.info("path : " + filePath);
		Map<String, String> map = XmlConfigUtil.getValueOfList("cmds");
		resList.addAll(ProvideDBHandler.provideMsgFromOracle(map, filePath));
		return resList;
	}
	
	public static void main(String[] args) {
		try {
			new ProvideMarketFileJob().execute(null);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
