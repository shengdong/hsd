package com.stsing.model;

/**
 * 
 * @author k1193
 *
 */
public class Quertz {

	/**
	 *  declare className
	 */
	private String className;
	
	/**
	 * declare jobName
	 */
	private String jobName;
	
	/**
	 * declare ttriggerName
	 */
	private String triggerName;
	
	/**
	 * declare trigger
	 */
	private String triggerTime;

	
	/**
	 * @return the way to get className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className 类名称
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the way to getJobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 *@param jobName 任务名称
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the way to getTriggerName
	 */
	public String getTriggerName() {
		return triggerName;
	}

	/**
	 * @param triggerName 触发器的名称
	 */
	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	/**
	 * @return the way to  getTriggerTime
	 */
	public String getTriggerTime() {
		return triggerTime;
	}

	/**
	 * 
	 *@param triggerTime 触发器的时间
	 */
	public void setTriggerTime(String triggerTime) {
		this.triggerTime = triggerTime;
	}
	
	
}
