package com.stsing.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.stsing.model.Info;
import com.stsing.provide.job.CurrentProvideJob;
import com.stsing.provide.job.ProvideJob;
import com.stsing.send.job.SendJob;
import com.stsing.util.DBUtil;
import com.stsing.util.ErrorDBHandler;
import com.stsing.util.SocketConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class ReceivedHandler {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(ReceivedHandler.class);

	
	/**
	 *  send to Client message
	 * @param jobName �������
	 * @return ���ص�ִ�гɹ�ʧ����Ϣ
	 */
	public static String sendToClient(String jobName) {
		Gson gson = new Gson();
		Info info = new Info();
		List<Info> result = new ArrayList<Info>();
	    if(StringUtils.isBlank(jobName)) {
	    	logger.error("job name is error: " + jobName);
	    	info.setSuccess(false);
            info.setErrorMsg("job name is error " + jobName);
            info.setObject(jobName);
            result.add(info);
            ErrorDBHandler.insertError(result);
            return gson.toJson(result);
	     }
	    
	    logger.error("jobName : " + jobName);
	    
		String flag = new DBUtil().querySet("select jobname, flag from quertz", false).get(jobName);
		if(StringUtils.contains(flag, "ZZZZZZZZZZ0000000000STSING")) {
			result = new SendJob().sendToServerJob(jobName);
			 if(("sendlcfile3".equals(jobName) || "sendlcfile4".equals(jobName))) {
				 	logger.info("begin to provide data to lcfile3 or lcfile4");
			    	CurrentProvideJob.provideData(jobName);
			    }
		 } else {
			 String className = SocketConfigUtil.getValue(jobName);
			 try {
				 Class<?> job =  Class.forName(className);
				 ProvideJob provideJob = (ProvideJob) job.newInstance();
				 result = provideJob.provideToMessage(jobName);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		 }
		 ErrorDBHandler.insertError(result);
		return gson.toJson(result);
	}
}
