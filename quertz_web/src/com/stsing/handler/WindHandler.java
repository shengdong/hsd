package com.stsing.handler;

import org.apache.log4j.Logger;

/**
 * 
 * @author k1193
 *
 */
public class WindHandler extends MessageHandler {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(WindHandler.class);
	

	@Override
	public boolean saveToDB(String path) {
		logger.info("wind path : " + path);
		return  DBHandler.saveToDB(path);
	}
}
