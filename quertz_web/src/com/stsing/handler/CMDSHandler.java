package com.stsing.handler;

import java.util.List;

import org.apache.log4j.Logger;

import com.stsing.util.XMLTransform;
import com.stsing.util.XmlConfigUtil;

/**
 * 
 * @author k1193
 *
 */
public class CMDSHandler extends MessageHandler {
	/**
	 * declare logger
	 */
	private static final Logger logger = Logger.getLogger(CMDSHandler.class);

	
	@Override
	public boolean saveToDB(String path) {
		logger.info("begin to insert into CMDS");
		String insertSql = XmlConfigUtil.getValue("insertCMDS");
		
		List<String> lists = XMLTransform.XMLtransql(path, "xmlToSql.xslt");
		System.out.println(lists);
		if(lists == null) {
			return true;
		}
		for(String sql : lists) {
			System.out.println(sql);
		}
		DBHandler.saveToDBFromMessage(lists, insertSql);
		return true;
	}
	
	public static void main(String[] args) {
		new CMDSHandler().saveToDB("C://Users//k1193//Desktop//何胜东//Desktop//行情平台//CMDS//20161010//CNYVND=CFHBC");
	}
}