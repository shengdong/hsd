package com.stsing.handler;

import common.Logger;

/**
 * 
 * @author k1193
 *
 */
public class DBFHandler extends MessageHandler {
	/**
	 * declare logger
	 */
	private final static Logger logger = Logger.getLogger(DBFHandler.class);

	
	@Override
	public boolean saveToDB(String path) {
		logger.info("dbf path: " + path);
		return  DBHandler.saveToDB(path);
	}
}
