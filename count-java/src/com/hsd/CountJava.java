package com.hsd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.lang.StringUtils;

public class CountJava {

	public static void main(String[] args) {
		String path = "/home/hsd/workspace/count-java";
//	
/**
 * 
 */
// 		
//		if(args != null && args.length >1) {
//			 path = args[0];
//		} else {
//			System.out.println(" path is null, please enter workpath");
//		}
		
		System.out.println("java line : " + new CountJava().getJavaCount(path));
	}
	
	public int getJavaCount(String path) {
		File file = new File(path);
		int amount = 0;
		
		if(file.isFile()) {
			return count(path);
		} else if(file.isDirectory()) {
			File[] files = file.listFiles();
			for(File f : files) {
				amount = amount + getJavaCount(f.getAbsolutePath());
			}
		}
		return amount;
	}
	
	
	private int count(String path) {
		File file = new File(path);
		String fileName = file.getName();
		if(!fileName.endsWith(".java")) {
			return 0;
		}
		
		
		InputStream inputStream = null;
		BufferedReader reader = null;
		int result = 0;
		
		try {
			inputStream = new FileInputStream(path);
			reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
			
			String line = "";
			while((line = reader.readLine()) != null) {
				if(StringUtils.isNotBlank(line)) {
					if(isJavaCode(line)) {
						result ++;
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(inputStream != null) {
					inputStream.close();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				if(reader != null) {
					reader.close();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	private boolean isJavaCode(String line) {
		if(line.startsWith("import")) {
			return false;
		}

		if(line.startsWith("//")) {
			return false;
		}
		
		if(line.startsWith("package")) {
			return false;
		}
		
		if(line.startsWith("/**") || line.trim().startsWith("*")) {
			return false;
		}
		return true;
	}
}
