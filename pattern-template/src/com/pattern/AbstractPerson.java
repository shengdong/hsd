package com.pattern;

public abstract class AbstractPerson {
	
	public void prepareGotoSchool() {
		dressUp();
		eatBreakfast();
		takeThings();
	}
	
	
	abstract void dressUp();
	
	abstract void eatBreakfast();
	
	abstract void takeThings();
}
