package com.pattern;

public class Teacher extends AbstractPerson {

	@Override
	void dressUp() {
		System.out.println("wear work clothes");
	}

	@Override
	void eatBreakfast() {
		System.out.println("do breakfast, take care of the children to eat breakfast");
	}

	@Override
	void takeThings() {
		System.out.println("take last night to prepare test paper");
	}

}
