package com.pattern;

public class Client {

	public static void main(String[] args) {
		
		Student student = new Student();
		student.prepareGotoSchool();
		
		System.out.println();
		System.out.println();
		System.out.println();
		Teacher teacher = new Teacher();
		teacher.prepareGotoSchool();
	}

}
