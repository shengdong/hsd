package com.pattern;

public class Student extends AbstractPerson {

	@Override
	void dressUp() {
		System.out.println("wear school uniforms");
	}

	@Override
	void eatBreakfast() {
		System.out.println("eat Mon's breakfast");
	}

	@Override
	void takeThings() {
		System.out.println("back schoolbag, bring my homework and red scarf");
	}

}
